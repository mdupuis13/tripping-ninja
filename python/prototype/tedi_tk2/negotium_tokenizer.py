# -*- coding: utf-8 -*-
#
#       negotium_tokenizer.py
#       
#       Copyright 2013  <martin@martindupuis.info>
#       
def tokenize(fildata):
#    print('IN  --> negotium_tokenizer.tokenize') 
    for fline in fildata:
        #test print('fline: %s' % fline)
        #remove cr+lf 
        #remove leading + trailing spaces
        #  and split into segments as defined by Negotium
        yield(fline.strip("\n").strip(" ").split("|"))
    
    
#    print('OUT <-- negotium_tokenizer.tokenize') 
    
    
