#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       main.py
#       
#       Copyright 2013  <martin@martindupuis.info>
#       
from datetime import datetime
import logging

import os
from pprint import pprint
import sys

import settings
logging.basicConfig(filename = settings.logfile, filemode = 'w', level = logging.INFO)
log = logging.getLogger('Main')

if __name__ == '__main__':
#    pprint(sys.argv[1:])
    
    for filename in sys.argv[1:]:
#        print("filename: %s" % (filename))
        msg = "Processing file: %s" % (filename)
    
        try:
            with open(filename, 'r') as fil:
                fildata = fil.readlines()
                print('fildata: %s' % (fildata))

                if fildata == []:
                    raise Exception('File is empty')
                    
                firstline = fildata[0][:-1]             
                
#                print('%s \r\n%s' % (firstline, firstline[3:6]))
                
                if '|' in firstline:
                    if str(firstline[3:6]) == '850':
                        import negotium_tokenizer as tokenizer
                        import negotium_parser as parser
                        parser.init()
                        import negotium_exporter as exporter
                        
                    else:
                        raise Exception('This is not a valid Negotium EDI PO(850) file.')
                else:
                    raise Exception('This is not a valid Negotium EDI file.')
                
                for tdata in tokenizer.tokenize(fildata):
                    pprint("tdata: %s" % (tdata))
                    parser.parse(tdata)
                
                exporter.export2csv(settings.outfile, parser.outfile)

            msg += "\tSUCCESS"
            print(msg)
            log.info(msg)
            
                #print('----- outfile -----')
                #pprint(parser.outfile)
                
        except Exception as ex:
            msg += "\tERROR\r\nAn exception occured:\r\n%s\r\n" % (ex)
            print(msg)
            log.exception(msg)
