# -*- coding: utf-8 -*-
'''
Test module for:    settings -> unittest

creation: mdupuis 2013-07-23
'''
import negotium_tokenizer as tok
import unittest
from pprint import pprint

class testNegTokenizer(unittest.TestCase):
    """ Class doc """

    def setUp(self):
        fil = open(name='./sample_files/test_edidoc.ff', mode='r')
        self.testfiledata = fil.readlines()

    def tearDown(self):
        pass

    def testNegTokenizer_validfile(self):
        rv = [['ID','850','PLASTIVAL','RON000']]
        rv.extend([['HEAD','','','20101005','7537073','00','NE',' ',' ','']])
        #pprint(rv)
        
        #pprint("testfiledata: %s" % (self.testfiledata))

        for tdata in tok.tokenize(self.testfiledata):
            #pprint("tdata: %s" % (tdata))
            
            if tdata[0] == 'ID':
                self.assertEqual(tdata, rv[0])
            elif tdata[0] == 'HEAD':
                self.assertEqual(tdata, rv[1])
            else:
                print 'no data'



#class testSettings_auto(unittest.TestCase): pass
#
# Run this code if you don't want to have to define tests manually,
# BUT the tests will run in alpha. order
#def suite():
#    suite = unittest.TestSuite()
#    suite.addTest(unittest.makeSuite(testEdidoc))
#    return suite

if __name__ == '__main__':
    suiteOr = unittest.TestSuite()

    suiteOr.addTest(testNegTokenizer('testNegTokenizer_validfile'))
    #suiteOr.addTest(testNegParser('testNegParser_backupdir'))
    #suiteOr.addTest(testNegParser('testNegParser_outfile'))

    unittest.TextTestRunner(verbosity=2).run(suiteOr)
