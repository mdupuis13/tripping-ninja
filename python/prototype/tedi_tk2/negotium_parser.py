# -*- coding: utf-8 -*-
#
#       negotium_parser.py
#       
#       Copyright 2013  <martin@martindupuis.info>
#       
from pprint import pprint

btaddr = []
staddr = []
items = {}
item = {}
itempos = 0
outfile = {}

segmentsize = {"ID":           3,
               "HEAD":         7,
               "CUR":          1,
               "REF":          1,
               "MSG":          1,
               "PER":          3,
               "ADDR_TYPE":    4,
               "ADDR":         2,
               "LOCATION":     2,
               "TERMS_GEN":   10,
               "DATE":         3,
               "FOB":          3,
               "ITEM":        12,
               "BO":           2,
               "PRICE":        2,
               "DESC":         1,
               "PHYS":         1,
               "TAX":          3,
               "TERMS_ITEM":  10,
               "ALLOW_ITEM":   6,
               "REF_ITEM":     2,
               "TOTAL":        2,
               "TOTAL_TAX":    3,
               "CARRIER":      8,
               "ALLOW_GEN":    8,
               "PACK":         4,
               "ST_CONTACT":   1,
               "ITEM_POS":     1,
               "LWADDR":       1
              }

'''
createSegment

creates a list with a fixed number of items
from another list

extra items are inserted as empty strings
'''
def createSegment(tokitem, nbsegments):
#    print("createSegment(%s, %s)" % (tokitem, nbsegments))
    _seg = []
    
    # force a certain nbsegments number of elements.
    #   so we add empty elements at the end if len(source) < nbsegments
    for idx in  range(0, nbsegments):
        try:
            _seg.append(tokitem[idx].strip(" "))
            
        except TypeError:       #not a "listable" type, so just append it
            _seg.append(tokitem)
        except IndexError as ixerr:
            _seg.append("extra")
            
    return _seg

def __initItem():
    global item
    global itempos
    
    item = {"ITEM":    [],
            "BO":      [],
            "PRICE":   [],
            "DESC":    [],
            "PHYS":    [],
            "GTAX":    [],
            "PTAX":    [],
            "HTAX":    [],
            "TERMS_ITEM":  [],
            "ALLOW_ITEM":  [],
            "REF_ITEM":    [],
            "ITEM_POS":   []
            }
    itempos = 0


def init():
    global btaddr
    global staddr
    global items
    global outfile
    

#    print('negotium_parser.init')

    btaddr = []
    staddr = []
    items = []
    __initItem()

    outfile = {"ID":    [],
               "HEAD":  [],
               "CUR":   [],
               "REF":   [],
               "MSG":   [],
               "PER":   [],
               "BTADDR":    [],
               "STADDR":    [],
               "LOCATION":  [],
               "TERMS_GEN": [],
               "STDATE":    [],
               "DRDATE":    [],
               "SODATE":    [],
               "FOB":       [],
               "ITEMS":     [],
               "TOTAL":     [],
               "GTOTAL_TAX": [],
               "PTOTAL_TAX": [],
               "HTOTAL_TAX": [],
               "CARRIER":    [],
               "ALLOW_GEN":  [],
               "PACK":       [],
               "ST_CONTACT": [],
               "LWADDR":     []
              }
              
def __addItem2Dict():
    global item
    global outfile
    
    if len(item["ITEM"]) > 0:
        outfile["ITEMS"].append(item)
        __initItem()
    
def parse(tokenized):
    global btaddr
    global staddr
    global item
    global items
    global itempos
    global outfile
    global segmentsize
    
#    print('IN  --> negotium_parser.parse') 
#    print('segment: %s' % (tokenized[0]))
            
    if tokenized[0] == 'HEAD':
        head = tokenized[1:4]
        head.append(';'.join(tokenized[4:7]))
        head.extend(tokenized[7:])
        
        outfile["HEAD"] = createSegment(head, segmentsize[tokenized[0]])    
            
    elif tokenized[0] == 'REF':
        outfile["REF"] = createSegment([';'.join(tokenized[1:4])], segmentsize[tokenized[0]])
        
    elif tokenized[0] == 'ADDR_TYPE':
        adrtpseg = createSegment(tokenized[1:], segmentsize[tokenized[0]])
        if tokenized[1] =='BT':
            btaddr = adrtpseg
        elif tokenized[1] =='ST':
            staddr = adrtpseg
            
    elif tokenized[0] == 'ADDR':
        adrseg = createSegment(tokenized[1:], segmentsize[tokenized[0]])
        if len(btaddr) > 0:
            btaddr.extend(adrseg)
        elif len(staddr) > 0:
            staddr.extend(adrseg)
            
    elif tokenized[0] == 'LOCATION':
        locseg = createSegment(tokenized[1:], segmentsize[tokenized[0]])
        if len(btaddr) > 0:
            btaddr.extend(locseg)
            outfile["BTADDR"] = btaddr

        elif len(staddr) > 0:
            staddr.extend(locseg)
            outfile["STADDR"] = staddr
        
        #print('BTADDR: %s' % (btaddr))
        #print('STADDR: %s' % (staddr))
            
    elif tokenized[0] == 'DATE':
        outfile[tokenized[1] + 'DATE'] = createSegment(tokenized[1:], segmentsize[tokenized[0]])
 
    elif tokenized[0] == "ITEM":
        #print("item['ITEM']: %s" % (item))
        __addItem2Dict()
            
        itempos += 1
        item[tokenized[0]] = createSegment(tokenized[1:], segmentsize[tokenized[0]])
        item["ITEM_POS"] = createSegment(itempos, segmentsize["ITEM_POS"])

    elif tokenized[0] in ["BO", "PRICE", "DESC", "TERMS_ITEM", "ALLOW_ITEM", "REF_ITEM"]:
        item[tokenized[0]] = createSegment(tokenized[1:], segmentsize[tokenized[0]])
                
    elif tokenized[0] == "PHYS":
        item[tokenized[0]] = ';'.join(tokenized[1:4]), segmentsize[tokenized[0]]
                
    elif tokenized[0] == "TAX":
#        print(item["ITEM"])
        
        if len(item["ITEM"]) > 0:
            # get tax for item
#            print("Tax into item")
            pass
        else:
            # get general tax
#            print("Tax OUT of item")
            pass
            
    else:
        __addItem2Dict()

        outfile[tokenized[0]] = createSegment(tokenized[1:], segmentsize[tokenized[0]])
        
#    print('OUT <-- negotium_parser.parse') 
