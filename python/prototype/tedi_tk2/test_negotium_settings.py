# -*- coding: utf-8 -*-
'''
Test module for:    settings -> unittest

creation: mdupuis 2013-07-23
'''
import settings
import unittest

class testSettings(unittest.TestCase):
    """ Class doc """

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testSettings_logfile(self):
        self.assertEqual(settings.logfile, './tedi_tk.log')
        
    def testSettings_backupdir(self):
        self.assertEqual(settings.backupdir, '../archive/')
        
    def testSettings_outfile(self):
        self.assertEqual(settings.outfile, './nego.csv')
            
#class testSettings_auto(unittest.TestCase): pass
#
# Run this code if you don't want to have to define tests manually,
# BUT the tests will run in alpha. order
#def suite():
#    suite = unittest.TestSuite()
#    suite.addTest(unittest.makeSuite(testEdidoc))
#    return suite

if __name__ == '__main__':
    suiteOr = unittest.TestSuite()

    suiteOr.addTest(testSettings('testSettings_logfile'))
    suiteOr.addTest(testSettings('testSettings_backupdir'))
    suiteOr.addTest(testSettings('testSettings_outfile'))


    unittest.TextTestRunner(verbosity=2).run(suiteOr)
