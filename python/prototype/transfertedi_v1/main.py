#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#       main.py
#       
#       Copyright 2011  <martin@martindupuis.info>
#       
from datetime import datetime
import logging
from negotium_edidoc import negotiuminit
import os
import sys
import settings

logging.basicConfig(filename = settings.logfile, filemode = 'w', level = logging.DEBUG)
log = logging.getLogger('Main')

# add current path to sys.path for easy reference
cmd_folder = os.path.dirname(os.path.abspath(__file__))
if cmd_folder not in sys.path:
        sys.path.insert(0, cmd_folder)
        
curdt = datetime.now()


def main():
    files = []
    docsdir = settings.docsdir

    # read INput folder for files to process
    for fname in os.listdir(docsdir):
        filepath = os.path.join(docsdir, fname)
        if os.path.isfile(filepath):
            files.append(filepath)

    for infile in files:
        msg = 'Processing: %s' % (infile)
        log.info(msg)
        print(msg)
        archivefile(infile, settings.archdir)
        
        try:
            oedi = getEdiProcessor(infile)
            oedi.processFile()

        except AttributeError as ex:
            pass
            #print('processOther(%s)' % infile)

        except Exception as ex:
            log.exception('Exception: %s' % ex)
    #return 0

    #TODO: check for presence of output file, if it exists, rename the
    #      existing file and create a new empty output file.

def getEdiProcessor(filename):
    try:
        oedi = negotiuminit.negotiumInit(filename)
        
        if oedi.isFileHandled():
            return oedi
        else:
            return None
    except:
        log.debug('getEdiProcessor -> raise')
        raise
    
from shutil import copy2

def archivefile(source, dest):
    destfilename = os.path.join(dest, getarchivefilename(source))
        
    log.debug('ARCHIVE TO: %s' % destfilename)
    #copy2(source, destfilename)
   
def getarchivefilename(source):
    #remove path and extension from path
    destfilename = os.path.splitext(os.path.basename(source))[0]
    destfilename += curdt.strftime('_%Y%m%d_%H%M')
    # add original file extension
    destfilename += os.path.splitext(source)[1]

    return destfilename
    
if __name__ == '__main__':
    main()
