#!/usr/bin/env python3
# -*- coding: utf-8 -*-
''' edidoc.py

This module contains classes that define an EDI doc
coming from the Negotium Pegdata software.

'''
import logging
#from pprint import pprint

# for file manipulations
import os
# regular expressions
import re

log = logging.getLogger('Edidoc')

class ediDocError(Exception): 
    def __init__(self, value):
        self.value = value
        log.error('ediDocError occured')
        
    def __str__(self):
        return str('ediDocError: %s' % self.value)
        
class ediSegmentError(ediDocError): pass
class ediElementValidationError(ediDocError): pass

class ediSegment():
    '''
        Segment of information from a Negotium ff file
    '''    
    def __init__(self, doctype, data, spec):
        '''
        the constructor fills some class variable and validates the segment
        against the specification
        '''
        self.subSegments = []
        self.Type = doctype
        self.ID = data[0]
        log.debug('ID: %s   speclen:%s' % (self.ID, len(spec)-1) )
        self.data = data[1:]
        self.spec = spec
        self._validate()
            
    def __repr__(self):
        return 'doctype : %s   ID : %s' % (self.Type, self.ID)
        
    def _validate(self):
        '''
        Validate the element against the spec
        '''
        log.debug('ediSegment.Validate()')
        
        # spec element #0 contains segment requirements. DO NOT test this right now...
 
        try:
            # if we can index like a dictionary, then we have sub-elements
            #   process them and return one ediSegment per sub-
            log.debug('self.spec[1][%s] = %s' % (self.ID, self.spec[1]))
            x = self.spec[1][self.ID]
            log.debug('create sub-elements')
            
            for data in self.data:
                spec = self.spec[1][data[0]]
                log.debug('datanew: %s\nspecnew: %s' % (data, spec))
                self.subSegments.append(ediSegment(self.Type, data, spec))
                
        except TypeError as TEx:
            # we are in presence of a top-level element, process it directly
            data = self.data
            spec = self.spec
            self._validateElement(data, spec[1:])

        except Exception as ex:
            log.exception('ediSegment._validate')
            
    def _validateElement(self, element, spec):
        '''
        element:    data in a list
        spec:       validation specification in a list
                    [requirement:datatype:minlen:maxlen:allowed_IDs]
                    [M|O|Cx:AN|DT|ID|NB|Rx:0:9999:ID1,ID2,ID3...]
                    M=Mandatory, O=Optional, C=Conditional on element at position x
                    AN=Alphanumeric, DT=date, ID=ID_from_allowed_IDs,
                    NB=integer, Rx=Real number with x decimal
        '''
        log.debug('ediSegment.ValidateElement(element=%s, spec=%s' %(element, spec))
                
        for segidx in range(len(element)):
            try:
                #TODO: trim spaces
                segdata = element[segidx]
                segspec = str(spec[segidx]).split(':')
#                print('-- segdata: %s\n-- segspec: %s' % (segdata,  segspec))
#                print('segidx: %s' % segidx)

                # check if the element is required or not
                # if the check passes, continue
                if self._checkElementRequirement(segdata, segspec[0]):
                    # verify that the segment has the correct length
                    self._checkElementLength(segdata, segspec[2:4])
                
                    # check the element type
                    self._checkElementType(segdata, segspec)
                    
            except ediElementValidationError:
                raise
                
            except IndexError as ie:
                log.error('IndexError: %s\r\nsegidx: %s' % (ie.args, segidx))
                
                
            except ValueError as ve:
                log.error('ValueError: %s' % ve.args)
                raise
                    
            except NotImplementedError as NIErr:
                log.error(NIErr.args)
                    
            except AttributeError as ex:
                # 'list' has no attribute 'split'
                # DEV here I assume that if I cannot split segdata, it means
                # segdata is a list containing other sub-segments
#                print('Exception: %s' % ex.args)
#                print('\tsegdata: %s   Type: %s' % (segdata, self.Type))

                subspec = spec[segidx]
                subseg = ediSegment(self.Type, segdata, subspec)
            except Exception as ex:
                log.exception('')
                raise
                
    def _checkElementRequirement(self, elemdata, elemspec):
        '''
        Checks for the "required" validation field
        
        returns true if validation can continue
        returns false if validation should not continue because of an
          optional or conditional field
        raises an error if validation should stop
        
                
        #TODO: conditional field validation is not complete
        '''
        #DEV: it seems that in the Negotium documents mandatory fields can be empty !!
        #     so we check that first and return False as soon as the data is empty

        log.debug('ediSegment._checkElementRequirement(elemdata=%s, elemspec=%s)' % (elemdata, elemspec))

        if str(elemdata).strip() == '':
            return False
            
        # check field requirements
        if elemspec == 'Cx':       # conditionnal field
            #TODO: validate correctly
           return True
                
        elif elemspec == 'M':   # mandatory field
            if elemdata == '':
                raise ediElementValidationError('Field is mandatory but no data found')
            else:
                return True
                
        elif elemspec == 'O':   # optional field
            if elemdata == '':
                return False
            else:
                return True
        
    def _checkElementLength(self, elemdata, elemlenspec):
        log.debug('ediSegment._checkElementLength(elemdata=%s, elemlenspec=%s)' % (elemdata, elemlenspec))
        if len(str(elemdata)) != 0:
            if len(str(elemdata)) < int(elemlenspec[0]) or len(str(elemdata)) > int(elemlenspec[1]):
                raise ediElementValidationError('%s: "%s" length is not conform to spec (%s to %s)' % (self.ID, elemdata, elemlenspec[0], elemlenspec[1]))

    def _checkElementType(self, elemdata, elemspec):
        log.debug('ediSegment._checkElementType(elemdata=%s, elemspec=%s)' % (elemdata, elemspec))
#        print('elemdata: %s' % elemdata)
        try:
            elemdata = elemdata.strip()
        except:
            pass
            
#        print('%s parsing %s' % (elemdata, elemspec))
        if len(str(elemdata)) != 0:
            if elemspec[1] == 'ID':
                checklist = str(elemspec[4]).split(',')
                log.debug(checklist)
                
                if elemdata in checklist:
                    pass
                else:
                    raise ediElementValidationError('%s: ID "%s" not in allowed list "%s"' % (self.ID, elemdata, checklist))

            elif elemspec[1] =='AN':
                # accepts every character possible
                pass

            elif elemspec[1] == 'DT':
                import datetime
                try:
                    value = datetime.date(int(elemdata[0:4]), int(elemdata[4:6]), int(elemdata[6:8]))
                except ValueError as vex:
                    raise ediElementValidationError('Segment DT is not a valid date') from vex
                except Exception as ex:
                    raise ediElementValidationError('Segment DT is not a valid date') from ex
                
            elif elemspec[1] == 'NB':
                try:
                    value = 1 + int(elemdata)
                except ValueError as vex:
                    raise ediElementValidationError('Segment NB is not a number') from vex
                except Exception as ex:
                    raise ediElementValidationError('Segment NB is not a number') from ex
                
            elif elemspec[1] == 'Rx':
                smin = elemspec[2]
                smax = elemspec[3]
                sdec = elemspec[4]
                swhole = str(int(smax) - int(sdec))
#                print('smin: %s, smax: %s, sdec: %s, swhole: %s' % (smin,smax,sdec,swhole))
#                print(elemdata)

                #ex.: Rx:1:4:2 must match 0004 or 4.40 
                spattern = r'(^\-?(\d{' + smin + ',' + smax + '})|(\d{' + smin + ',' + swhole + '}\[.]\d{' + smin + ',' + sdec + '}$))'
                log.debug(spattern)
                rx = re.compile(spattern, re.VERBOSE)
                result = rx.search(elemdata)
                log.debug('regexp: %s\nResult: %s' % (rx.pattern, result))
                
                if result is None and elemdata.strip(' ') != '':
                    raise ediElementValidationError('%s: "%s" does not conform to "%s"' % (self.ID, elemdata, rx.pattern))

            else:
                raise ediElementValidationError('"%s" is not a valid segment type' % elemspec[1])
        
        
class ediDoc():
    '''
    Base class of an EDIdocument
    Provides functions for:
        - reading the file into a document
        - writing the document to disk

    '''

    def __init__(self):
        self.segments = []

    def readFile(self, path, filename):
        '''
        input
            filename: a complete path to a text file with an extension
            present in the extlist
        '''
        log.debug('ediDoc.readFile(path=%s, filename=%s)' % (path, filename))
        if filename == '':
            raise IOError('No file name given')

        self.client = filename.split('-')[0].lower()
        self.filename = filename
        log.debug('%s: %s' % (self.client, self.filename))
        
        with open(file=os.path.join(path, filename), mode='r') as edifile:

            for line in edifile:
                # remove ending linefeed from the line read
                # and split into a list on the "|" character
                self.segments.append(list(line[:-1].split('|')))

        if self.segments == []:
            raise ValueError('Empty edidoc')

        if self.segments[0][0] != 'ID':
            raise TypeError('Le fichier n''est pas un fichier EDI reconnu.')


    def writeFile(self, path, filename):
        log.debug('ediDoc.writeFile(path=%s, filename=%s)' % (path, filename))
        if self.segments == []:
            raise ValueError('fileitems is empty')

        f = open(file=path + filename, mode='w')

        for segment in self.segments:
            f.write('|'.join('%s' % elem for elem in segment))
            f.write('\r\n')

        f.close()


class edi850(ediDoc):
    '''
    This class defines a purchase order (850 type) document

    The different elements are separated here to be processed later
    '''
    #DEV:  ITEM->ALLOW_ITEM->ID4 changed to AN because ID list very long
    #TODO: ID lists must be loaded from database
    #TODO: change ID4 to use list from database
    docSpec = {'ID':['M', 'M:ID:3:3:850', 'M:AN:1:15', 'M:AN:1:15'], 
               'HEAD': ['M', 'O:DT:8:8', 'O:AN:1:22', 'M:DT:8:8', 'M:AN:1:22', 
                        #'M:ID:2:2:00,OO,CO', 'M:ID:2:2:SA,DR,NS,NN,RO,NE,FF', 
                        'M:ID:2:2:00,OO,CO', 'M:ID:2:2:SA,DR,NS,NN,RO,NE,FF,OS', 
                        'O:AN:1:22', 'O:AN:1:12', 'O:AN:1:22'],
               'CUR': ['O', 'M:ID:3:3:CAD,USD'],
               #'REF': ['M', 'M:ID:2:3:GST,PST,HST,BOL,ATH,CM,SN,APO,RSN,DP,IT,MR', 
               'REF': ['M', 'M:ID:2:3:GST,PST,HST,BOL,ATH,CM,SN,APO,RSN,DP,IT,MR,ZZ', 
                       'M:AN:1:30', 'O:AN:1:20'],
                       #'M:AN:1:30', 'O:AN:1:10'],
               'MSG': ['O', 'O:AN:2:130'],
               #'PER': ['O', 'M:ID:2:2:SR,BY,AG', 'M:AN:1:35', 'M:AN:1:80'],
               'PER': ['O', 'M:ID:2:2:SR,BY,AG,OC,DC,BD', 'M:AN:1:35', 'M:AN:1:80'],
               'ADDR': ['M', 
                        #{'ADDR_TYPE': ['M', 'M:ID:2:2:BT,ST,BY,VN,FD,SF,BS', 'M:AN:1:80', 'M:AN:2:2', 'M:AN:2:20'],
                        {'ADDR_TYPE': ['M', 'M:ID:2:2:BT,ST,BY,VN,FD,SF,BS,LW', 'M:AN:1:80', 'M:AN:2:2', 'M:AN:2:20'],
                         'ADDR': ['M', 'M:AN:1:55', 'M:AN:1:55'],
                         #'LOCATION': ['M', 'M:AN:2:30', 'M:AN:2:2', 'M:AN:3:15', 'M:ID:2:3:CA,US,  ']
                         'LOCATION': ['M', 'M:AN:2:30', 'M:AN:2:2', 'M:AN:3:15', 'M:ID:2:3:CA,US,USA,  ']
                        }
                       ],
               'TERMS_GEN': ['M', 'M:ID:2:2:RT,ST', 'M:ID:2:2:DD,ID,  ', 'O:Rx:1:6:3', 'Cx:DT:8:8',
                             'Cx:AN:1:3', 'Cx:DT:8:8', 'O:AN:1:3', 'O:Rx:1:10:2', 'O:AN:1:30', 'O:AN:1:2'],
               'DATE': ['M', 'M:ID:2:2:DL,ST,CA,DR,SO,PK', 'M:DT:8:8', 'O:NB:4:8'],
               #'FOB': ['M', 'M:ID:2:2:PP,CL,DF,PU', 'M:ID:2:2:CR,DE,ZZ', 'O:AN:1:80'],
               'FOB': ['M', 'M:ID:2:2:PP,CL,DF,PU', 'M:ID:2:2:CR,DE,ZZ,DC', 'O:AN:1:80'],
               'ITEM': ['M', 
                        {'ITEM': ['M', 'O:AN:1:20', 'M:Rx:1:10:3', 'M:ID:2:2:BP,FC,01,BG,BO,BX,BD,Z3,CO,CY,DZ,DR,EA,FT,GR,GS,H4,KG,KT,LF,LM,LY,LT,ML,PK,PL,PC,QT,SF,TH,TM,TS,TI,UN,YD,IN',
                                  'M:Rx:0:17:4', 'M:ID:2:2:VN', 'M:AN:1:48', 'O:ID:2:2:UP,EN,UK,UA', 'O:AN:1:48',
                                  'O:ID:2:2:SK', 'O:AN:1:48', 'O:NB:1:3', 'O:NB:1:3'],
                         'BO': ['O', 'M:Rx:1:10:3', 'O:Rx:1:10:3'],
                         'PRICE': ['O', 'O:ID:3:3:UCP', 'M:Rx:1:17:4'],
                         'DESC': ['M', 'M:AN:1:80'],
                         'PHYS': ['O', 'M:NB:1:6', 'M:Rx:1:8:3', 'M:ID:2:2:BP,FC,01,BG,BO,BX,BD,Z3,CO,CY,DZ,DR,EA,FT,GR,GS,H4,KG,KT,LF,LM,LY,LT,ML,PK,PL,PC,QT,SF,TH,TM,TS,TI,UN,YD,IN'],
                         'TAX': ['O', 
                                 {'TAX': ['O', 'M:ID:2:3:GST,PST,HST', 'M:Rx:1:15:2', 'O:Rx:1:10:2']}
                                ],
                         'TERMS_ITEM': ['O', 'M:ID:2:2:RT,ST', 'M:ID:2:2:DD,ID', 'O:Rx:1:6:3', 'Cx:DT:8:8',
                                       'Cx:AN:1:3', 'C:DT:8:8', 'O:AN:1:3', 'O:Rx:1:10:2', 'O:AN:1:30', 'O:AN:1:2'],
                         #TODO: Item2 is ID, get list from database
                         'ALLOW_ITEM': ['O', 'M:ID:1:1:A,C', 'M:AN:4:4', 'Cx:Rx:1:15:2', 'Cx:Rx:1:6:3', 'Cx:Rx:1:6:3', 'O:AN:1:80'],
                         'REF_ITEM': ['O', 'O:ID:2:3:PG', 'O:AN:1:30']
                        }
                       ],
               'TOTAL': ['M', 'M:ID:2:2:TT', 'M:Rx:1:15:2','Cx:Rx:1:15:2'],
               'TOTAL_TAX': ['Cx', 'M:ID:2:3:GST,PST,HST', 'M:Rx:1:15:2', 'O:Rx:1:10:2'], 
               'CARRIER': ['O', 'O:ID:1:2:AI,CC,PP,PS,PK,ZZ', 'O:AN:2:4', 'O:AN:1:35', 'M:ID:2:3:CN', 
                                'M:AN:1:30', 'O:ID:2:3:AV,BO,BK,CL,DD,DI,DP,EX,ED,ZZ,PR,CP,PD,SE,CC,CM,CS,CE,LM,SI,BP,SC,SA,SK,SL,SH,SG,SF,SD,SI,SS,ST,OR,UN,UB', 
                                '0:AN:2:15', 'Cx:ID:2:2:01,02,03,04'],
               #TODO: Item2 is ID, get list
               'ALLOW_GEN': ['O', 'M:ID:1:1:A,C', 'M:AN:4:4', 'C:Rx:1:15:2', 'Cx:Rx:1:6:3', 'Cx:Rx:1:6:3', 'O:AN:1:80'], 
               'PACK': ['Cx', 'M:NB:1:10', 'M:ID:2:2:PK,CA,PL', 'M:Rx:1:10:2', 'M:ID:2:2:BP,FC,01,BG,BO,BX,BD,Z3,CO,CY,DZ,DR,EA,FT,GR,GS,H4,KG,KT,LF,LM,LY,LT,ML,PK,PL,PC,QT,SF,TH,TM,TS,TI,UN,YD,IN'] 
              }
    
    docType = '850'
    
    def __init__(self):
        #init super class
        ediDoc.__init__(self)
        #dict that will contain elements
        self.elements = {}

    def readFile(self, path, filename):
        log.debug('edi850.ReadFile(path=%s, filename=%s)' % (path, filename))
        ediDoc.readFile(self, path, filename)

#        print(self.segments)
        
        if self.segments[0][1] != self.docType:
            raise ValueError('The document is not a purchase order (850) document.')

        self.PO = self.segments[1][4]
        
        self._readSegments()
        
    def _readSegments(self):
        #TODO: find a way to create the ADDR, ITEM and TAX elements with their sub-elements
        #      looping with index might help ??

        for icount in range(len(self.segments)): 
            elem = []
            
#            print(self.segments[icount][0])
            if self.segments[icount][0] == 'ADDR_TYPE':
                elem = ['ADDR']
                addrelem = self.segments[icount:icount+3]
                elem.extend(addrelem)

            # these items have been cared of in the previous 'if'
            elif self.segments[icount][0] in ['ADDR', 'LOCATION']:
                continue
                
            elif self.segments[icount][0] == 'ITEM':
                logg.debug('-- self.segments[icount] == %s' % self.segments[icount])
                elem = ['ITEM']
                elem.append(self.segments[icount])
                
                for iItem in range(icount + 1, len(self.segments)):
                    logg.debug('---- self.segments[iItem] == %s' % self.segments[iItem])
                    
                    if self.segments[iItem][0] in ['BO', 'PRICE', 'DESC', 'PHYS', 'TERMS_ITEM', 'ALLOW_ITEM']:
                        elem.append(self.segments[iItem])
                        
                    elif self.segments[iItem][0] == 'TAX':
                        taxelem = []
                        for iTax in range(iItem + 1, len(self.segments)):
                            if self.segments[iTax][0] == 'TAX':
                                taxelem.append(self.segments[iTax])
                            else:
                                break

                        elem.append(taxelem)

                    else:
                        #new item
                        break

            elif self.segments[icount][0] in ['BO', 'PRICE', 'DESC', 'PHYS', 'TERMS_ITEM', 'ALLOW_ITEM']:
               #'TOTAL': ['M', ], 
               #'TOTAL_TAX': ['C', ], 
               #'CARRIER': ['O', ], 
               #'ALLOW_GEN': ['O', ], 
               #'PACK': ['C', ] 
                continue
                
            #elif self.segments[icount][0] in ['TOTAL', 'TOTAL_TAX', 'CARRIER', 'ALLOW_GEN', 'PACK']:
                #pass
            else:
                elem = self.segments[icount]

            log.debug(elem)
            if elem[0] != '':
                spec = self.docSpec[elem[0]]
                try:
                    oseg = ediSegment(self.docType, elem, spec)
                    log.debug(oseg)
                    
                except Exception as ex:
#                    print('_readSegment: %s' % ex)
                    log.exception('_readSegment: %s' % ex)

class edi850_Rona(edi850): pass
class edi850_HomeDepot(edi850): pass
class edi850_Lowes(edi850): pass
class edi850_Menards(edi850): pass
