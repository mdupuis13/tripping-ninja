#!/usr/bin/env python3
# -*- coding: utf-8 -*-
''' negotiuminit.py

This module is the interface to processing edi documents
coming from the Negotium Pegdata software.

This interface defines some generic functions and variables used by the
main.py program.

'''
import edidoc
import logging
#from pprint import pprint
import os

import settings

log = logging.getLogger('NegotiumInit')

class negotiumEdiError(edidoc.ediDocError): 
    def __init__(self, value):
        edidoc.ediDocError.__init__(self,value)
        #self.value = value
        log.debug('NEGOTIUMEdiError occured')
        
    def __str__(self):
        return str('negotiumEdiError: %s' % self.value)

class negotiumInit():
    _clients = {'RONA': 'RON000',
                'HDC':  'HDC000',
                'MENARDS': 'MEN000|MEN001|MEN002',
                'LOWES': 'LOW000'
               }


    def _init(self):
        self.extlist = ['.ff']       # list of extensions handled by Negotium
        self.filepath = ''
        self.filename = ''
        

    def __init__(self, filename):
        self._init()

        if filename == None:
            raise AttributeError('filename parameter is empty')

        self.filename = filename
    

    def isFileHandled(self):
        log.debug('isFileHandled')
        infileext = os.path.splitext(self.filename)[1]
        
        if infileext in self.extlist:
            return True
        else:
            #raise TypeError('File extension is not one of those: %s' % self.extlist)
            return False

    def processFile(self):
        log.debug('negotiuminit.processFile(%s)' % self.filename)
        
        try:
            fil = open(self.filename, 'r')
            firstline = fil.readline()

            if firstline == '':
                raise negotiumEdiError('File is empty')
                return None
                
            if '|' not in firstline:
                raise negotiumEdiError('This is not a valid Negotium EDI file.')
                return
            
            firstline = firstline[:-1].split('|')
            filespec = os.path.split(self.filename)

            oedi = self._selectDoctype(firstline)
            oedi.readFile(filespec[0], filespec[1])
            
#            pprint(oedi.segments)
        except Exception as ex:
            log.exception('%s :File not read\n%s' % (self.filename, ex.message))
            raise
            
            
    def _selectDoctype(self, line):
        log.debug('negotiuminit._selectDoctype: line=%s' % line)
        if line[1] == '850':
            return self._select850Client(line[3])
        else:
            print('doctype not recognized: %s' % line[1])
        
        
    def _select850Client(self, client):
        client = client.strip()
        log.debug('negotiuminit._select850Client: client=%s' % client)
        
        if client in self._clients['HDC'].split('|'):
            return edidoc.edi850_HomeDepot()

        elif client in self._clients['LOWES'].split('|'):
            return edidoc.edi850_Lowes()

        elif client in self._clients['MENARDS'].split('|'):
            return edidoc.edi850_Menards()

        elif client in self._clients['RONA'].split('|'):
            return edidoc.edi850_Rona()

        else:
            print('client not recognized: %s' % client)
