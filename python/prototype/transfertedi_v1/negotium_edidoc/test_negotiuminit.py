#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Test module for:    negotiuminit -> unittest

creation: mdupuis 2011-08-17
'''

import negotiuminit
import os
import unittest


class testInit(unittest.TestCase):
    
    def setUp(self):
        self.testpath='./testdocs/'

        
    def tearDown(self):
        pass

    def testInit_checkFileExt(self):
        testfile='test_edidoc.txt'
        self.oinit = negotiuminit.negotiumInit(filename=os.path.join(self.testpath, testfile))
        self.assertEqual(len(self.oinit.extlist), 1)
            
            
    def testInit_isFileHandled(self):
        '''
        This tests a file extension accepted
        '''
        testfile='test_edidoc.txt'
        self.oinit = negotiuminit.negotiumInit(filename=os.path.join(self.testpath, testfile))
        self.assertFalse(self.oinit.isFileHandled())

        testfile='test_edidoc.ff'
        self.oinit = negotiuminit.negotiumInit(filename=os.path.join(self.testpath, testfile))
        self.assertTrue(self.oinit.isFileHandled())


    def testInit_processFile_TypeError(self):
        from negotiuminit import negotiumEdiError
        
        testfile='test_not_edidoc.ff'
        self.oinit = negotiuminit.negotiumInit(filename=os.path.join(self.testpath, testfile))
        self.assertRaises(negotiumEdiError, self.oinit.processFile)
        
    def testInit_processFile(self):
        testfile='test_edidoc.ff'
        filename=os.path.join(self.testpath, testfile)
        self.oinit = negotiuminit.negotiumInit(filename)

        if self.oinit.isFileHandled():
            self.assertEqual(self.oinit.processFile(), None)

        
        
if __name__ == '__main__':
    suiteOr = unittest.TestSuite()

    suiteOr.addTest(testInit('testInit_checkFileExt'))
    suiteOr.addTest(testInit('testInit_isFileHandled'))
    #suiteOr.addTest(testInit('testInit_processFile_TypeError'))
    suiteOr.addTest(testInit('testInit_processFile'))
    unittest.TextTestRunner(verbosity=2).run(suiteOr)
