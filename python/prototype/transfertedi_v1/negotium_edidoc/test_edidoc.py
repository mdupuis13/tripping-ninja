# -*- coding: utf-8 -*-
'''
Test module for:    edidoc -> unittest

creation: mdupuis 2011-06-22
'''
import edidoc
import unittest
import os

class testSegment(unittest.TestCase):
    def setUp(self):
        self.oseg = None

    def tearDown(self):
        self.oseg = None

    def testSegment_CreateSegment(self):
        '''
        tests the creation of a segment
        '''
        self.oseg = edidoc.ediSegment('850', 
                                      ['ID', '850', 'PLASTIVAL', 'RON000'], 
                                      ['M', 'M:ID:3:3:850', 'M:AN:1:15', 'M:AN:1:15'])

        self.assertEqual(self.oseg.Type, '850')
        self.assertEqual(self.oseg.ID, 'ID')
        self.assertEqual(self.oseg.data, ['850', 'PLASTIVAL', 'RON000'])

    def testSegment_ValidSegment(self):
        '''
        tests the validation of segments
        '''
        print('')
        print('--- Tests segments of type ID, AN')
        self.oseg = edidoc.ediSegment('850', 
                                      ['ID', '850', 'PLASTIVAL', 'RON000'],
                                      ['M', 'M:ID:3:3:850,855', 'M:AN:1:15', 'M:AN:1:15'])

        print('--- Tests segments of type DT')
        self.oseg = edidoc.ediSegment('850', 
                                      ['ID', '20111104'],
                                      ['M', 'M:DT:8:8'])      

        print('--- Tests segments of type NB')
        self.oseg = edidoc.ediSegment('850', 
                                      ['ID', 123],
                                      ['M', 'M:NB:1:10'])

        print('--- Tests sub-segments')
        self.oseg = edidoc.ediSegment('850', 
                                      ['ITEM', ['ITEM', '123', 'ITEM1', 'Item1_Elem3'], ['DESC', '234', 'DESC1', 'DESC1_Elem3']],
                                      ['M', {'ITEM': ['M', 'M:ID:3:3:123', 'M:AN:1:15', 'M:AN:1:15'], 
                                             'DESC': ['M', 'M:ID:3:3:234', 'M:AN:1:15', 'M:AN:1:15']}
                                      ])

        print('--- Tests segments of type Rx')
        self.oseg = edidoc.ediSegment('850', 
                                      ['ID', '123.45'], 
                                      ['M', 'M:Rx:1:10:2'])
        self.oseg = edidoc.ediSegment('850', 
                                      ['ID', '123.45'], 
                                      ['M', 'M:Rx:1:10:3'])
        self.oseg = edidoc.ediSegment('850', 
                                      ['ID', '123.454'], 
                                      ['M', 'M:Rx:1:7:3'])

    def testSegment_InvalidSegment(self):
        print('')

        print('--- Tests segment ID')
        self.assertRaises(edidoc.ediElementValidationError, edidoc.ediSegment, 
                          '850', 
                          ['ID', '850', 'PLASTIVAL', 'RON000'],
                          ['M', 'M:ID:3:3:840', 'M:AN:1:15', 'M:AN:1:15'])
        
        print('--- Tests segment''s element datatype')
        self.assertRaises(edidoc.ediElementValidationError, edidoc.ediSegment, 
                          '850', 
                          ['ID', '850', 'PLASTIVAL', 'RON000'],
                          ['M', 'M:AA:3:3:850', 'M:AN:1:15', 'M:AN:1:15'])
        
        print('--- Tests segments length')
        self.assertRaises(edidoc.ediElementValidationError, edidoc.ediSegment, 
                          '850', 
                          ['ID', '850', 'PLASTIVAL', 'RON000'],
                          ['M', 'M:ID:2:2:850', 'M:AN:1:15', 'M:AN:1:15'])
        
        print('--- Tests segments of type ID')
        self.assertRaises(edidoc.ediElementValidationError, edidoc.ediSegment, 
                          '850', 
                          ['ID', '850'],
                          ['M', 'M:ID:3:3:840'])
        
        print('--- Tests segments of type AN')
        self.assertRaises(edidoc.ediElementValidationError, edidoc.ediSegment, 
                          '850', 
                          ['ID', 'RON000'],
                          ['M', 'M:AN:10:15'])
        
        print('--- Tests segments of type DT')
        self.assertRaises(edidoc.ediElementValidationError, edidoc.ediSegment, 
                          '850', 
                          ['ID', '20111504'],
                          ['M', 'M:DT:8:8'])
        
        print('--- Tests segments of type NB')
        self.assertRaises(edidoc.ediElementValidationError, edidoc.ediSegment, 
                          '850', 
                          ['ID', 'a123'],
                          ['M', 'M:NB:1:10'])
        print('--- Tests segments of type NB')
        self.assertRaises(edidoc.ediElementValidationError, edidoc.ediSegment, 
                          '850', 
                          ['ID', 123],
                          ['M', 'M:NB:5:10'])
        print('--- Tests segments of type NB')
        self.assertRaises(edidoc.ediElementValidationError, edidoc.ediSegment, 
                          '850', 
                          ['ID', 12345678901],
                          ['M', 'M:NB:1:10'])
        
        print('--- Tests segments of type Rx')
        self.assertRaises(edidoc.ediElementValidationError, edidoc.ediSegment, 
                          '850', 
                          ['ID', 'a123'],
                          ['M', 'M:Rx:1:10:2'])
        self.assertRaises(edidoc.ediElementValidationError, edidoc.ediSegment, 
                          '850', 
                          ['ID', 123.45],
                          ['M', 'M:Rx:1:5:3'])
        self.assertRaises(edidoc.ediElementValidationError, edidoc.ediSegment, 
                          '850', 
                          ['ID', 12345123.454],
                          ['M', 'M:Rx:1:10:3'])
        
        print('--- Tests sub-segments - too many segments')
        self.assertRaises(KeyError, edidoc.ediSegment, '850', 
                          ['ITEM', ['ITEM', '123', 'ITEM1', 'Item1_Elem3'], ['DESC', '234', 'DESC1', 'DESC1_Elem3']],
                          ['M', {'ITEM': ['M', 'M:ID:3:3:123', 'M:AN:1:15', 'M:AN:1:15']}])
        print('--- Tests sub-segments - missing segment')
        print('--- Code missing for this part. ---')
        #self.assertRaises(ValueError, edidoc.ediSegment, '850', 
                          #['ITEM', ['DESC', '234', 'DESC1', 'DESC1_Elem3']],
                          #['M', {'ITEM': ['M', 'M:ID:3:3:123', 'M:AN:1:15', 'M:AN:1:15'], 
                                 #'DESC': ['M', 'M:ID:3:3:234', 'M:AN:1:15', 'M:AN:1:15']}])



class testEdidoc(unittest.TestCase):
    def setUp(self):
        self.testfilepath='./testdocs/'
        self.oedidoc = edidoc.ediDoc()

    def tearDown(self):
        self.oedidoc == None

    def testEdidoc_ReadFile(self):
        '''
        This tests a normal, valiv edi file
        '''
        filename='test_edidoc.ff'
        self.assertEqual(self.oedidoc.readFile(self.testfilepath, filename), None)

    def testEdidoc_ReadFileNotEdidoc(self):
        '''
        This tests a file with correct extension, but does not start
        with the right element.
        '''
        filename='test_not_edidoc.ff'
        self.assertRaises(TypeError, self.oedidoc.readFile, self.testfilepath, filename)

    def testEdidoc_ReadFileEmpty(self):
        '''
        This tests an empty file, but with valid extension
        '''
        filename='test_empty_edidoc.ff'
        self.assertRaises(ValueError, self.oedidoc.readFile, self.testfilepath, filename)

    def testEdidoc_ReadNoFile(self):
        '''
        This tests when no filename is given.
        '''
        filename = ''
        self.assertRaises(IOError, self.oedidoc.readFile, self.testfilepath, filename)

    def testEdidoc_WriteFileEmptyFileitems(self):
        '''
        This tests ValueError raised if fileitems is empty
        '''
        filename = 'test_write_file.ff'
        self.assertRaises(ValueError, self.oedidoc.writeFile, self.testfilepath, filename)

    def testEdidoc_WriteFile(self):
        '''
        This tests output to a file
        '''
        filename = 'test_write_file.ff'

        # delete file if it exists
        if filename in os.listdir(self.testfilepath):
            os.remove(self.testfilepath + filename)

        # read a valid edifile
        self.oedidoc.readFile(self.testfilepath,'test_edidoc.ff')
        # run the function
        self.assertEqual(self.oedidoc.writeFile(self.testfilepath, filename), None)

        # test if the file was created
        if not (filename in os.listdir(self.testfilepath)):
            raise IOError('%s not created !' % (self.testfilepath + filename))

        # test if the file contains the ID segment as first segment
        f = open(file=self.testfilepath + filename, mode='r')
        line = f.read()
        if line == '':
            raise IOError('File is empty')

        # test if the file has the ID element as first element
        if list(line.split('|'))[0] != 'ID':
            raise ValueError('File not an EDI file')

class testEdi850(unittest.TestCase):
    def setUp(self):
        self.testfilepath='./testdocs/'
        self.oedi850 = edidoc.edi850()

    def tearDown(self):
        self.oedi850 == None

    def testEdi850_Is850Doc(self):
        '''
        This tests if the document is a purchase order (type=850)
        '''
        filename='test850_edidoc.ff'

        self.assertEqual(self.oedi850.readFile(self.testfilepath, filename), None)

    def testEdi850_Not850Doc(self):
        '''
        This tests if the document is not a purchase order (type=850)
        '''
        filename='test850_notedidoc.ff'
        self.assertRaises(ValueError, self.oedi850.readFile, self.testfilepath, filename)

    def testEdi850_SavePONumber(self):
        '''
        This tests the proper storing of the PO number
        '''
        filename='test850_edidoc.ff'
        self.oedi850.readFile(self.testfilepath, filename)
        self.assertEqual(self.oedi850.PO,'7537073')

    def testEdi850_readSegments(self):
        '''
        This tests the proper storing of the PO number
        '''
        filename='test850_edidoc.ff'
        self.oedi850.readFile(self.testfilepath, filename)

#class testedi850_RONA(unittest.TestCase): pass
#
# Run this code if you validate850Segmentdon't want to have to define tests manually,
# BUT the tests will run in alpha. order
#def suite():
#    suite = unittest.TestSuite()
#    suite.addTest(unittest.makeSuite(testEdidoc))
#    return suite

if __name__ == '__main__':
    suiteOr = unittest.TestSuite()

    suiteOr.addTest(testSegment('testSegment_CreateSegment'))
    suiteOr.addTest(testSegment('testSegment_ValidSegment'))
    suiteOr.addTest(testSegment('testSegment_InvalidSegment'))

    suiteOr.addTest(testEdidoc('testEdidoc_ReadFile'))
    suiteOr.addTest(testEdidoc('testEdidoc_ReadFileNotEdidoc'))
    suiteOr.addTest(testEdidoc('testEdidoc_ReadFileEmpty'))
    suiteOr.addTest(testEdidoc('testEdidoc_ReadNoFile'))
    suiteOr.addTest(testEdidoc('testEdidoc_WriteFileEmptyFileitems'))
    suiteOr.addTest(testEdidoc('testEdidoc_WriteFile'))

    suiteOr.addTest(testEdi850('testEdi850_Is850Doc'))
    suiteOr.addTest(testEdi850('testEdi850_Not850Doc'))
    suiteOr.addTest(testEdi850('testEdi850_SavePONumber'))
    suiteOr.addTest(testEdi850('testEdi850_readSegments'))

    unittest.TextTestRunner(verbosity=2).run(suiteOr)
