b = {'ID': ['M', AN, DT],
     'ITEM': ['M', {'ITEM': ['M', AN, NB, DT], 
                    'BO': [], 
                    'DESC': ['M', NB, AN], 
                    'TAX': ['O', {TAX: ['O', ]}], 
                    'ALLOW_GEN': []
                   }
             ]
    }

data = [{'ID':       ['850', 'PLASTIVAL', 12345],
         'DATE':     [['DR', 2012-04-06],
                      ['SO', 2012-04-08]],
         'ADDR':     [{'ADDRTYPE': [],
                       'ADDR':     [],
                       'LOCATION': []},
                      {}],
         'PER':      [['MARTIN DUPUIS'],
                      ['CHARLES EDOUARD DUPUIS']],
         'ITEM': [{'ITEM':       [],
                   'DESC':       [['DESC 1'],
                                  ['DESC line 2']],
                   'TAX':        [['TPS', AMT],
                                  ['TVQ', AMT]],
                   'TOTAL':      []
                  },
                  {'ITEM':       [],
                   'DESC':       [[]],
                   'TAX':        [['TPS', AMT],
                                  ['TVQ', AMT]],
                   'TOTAL':      []
                  }
                 ],
         'ALLOWGEN': [['C', 'D540', 123.45],
                      ['A', 'C130',  34.98]]
        },
        {'ID':       ['850', 'PLASTIVAL', 67890],
         'DATE':     [2012-04-07],
         'PER':      [['JOE TANTO'],
                      ['ROCKY BALBOA']],
         'ITEM': [{'ITEM':   [],
                   'DESC':   [[]],
                   'TAX':    [[]],
                   'TOTAL':  []
                  },
                  {'ITEM':   [],
                   'DESC':   [[]],
                   'TAX':    [[]],
                   'TOTAL':  []
                  }]
        }
       ]
