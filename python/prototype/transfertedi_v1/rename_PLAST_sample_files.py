import os.path

def renamefiles(filespath):
    for fname in os.listdir(filespath):
        filepath = os.path.join(filespath, fname)
        if os.path.isfile(filepath):
            if '_' in fname:
                #print(fname.split('_'))
                fnamepre = fname.split('_')[0]
                fnamepost = fname.split('_')[-1].split('-')[-1]
                
                newfname='-'.join([fnamepre, fnamepost])
                newfname=os.path.join(filespath, newfname)
                
                fname=os.path.join(filespath, fname)

                print('%s -> %s' % (fname, newfname))
                os.rename(fname, newfname)
    return 0

if __name__ == "__main__":
    renamefiles("./sample_files/2010/")
    renamefiles("./sample_files/2011/")
    renamefiles("./sample_files/2012/")
