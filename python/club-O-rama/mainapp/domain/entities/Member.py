from BaseEntity import BaseEntity

class Member(BaseEntity):
  def __init__(self):
    super(Member, self).__init__()

def MemberFromData(memberId):
  mbr = Member()
  mbr.ID = memberId

  return mbr
