import uuid

class BaseEntity(object):
  def __init__(self, identity_value = None, internal_id = None):

    # Providea
    if identity_value == None:
      identity_value = uuid.uuid4()

    self.ID = identity_value

    if internal_id != None:
      self.internal_id = internal_id

  def getId(self):
    return self.ID

  def getInternalId(self):
    try:
      retval = self.internal_id
    except AttributeError:
      retval = None

    return retval
