import unittest
import uuid

from entities.Member import MemberFromData

class MemberFromDataTest(unittest.TestCase):
  def test_MemberFromData_gets_ID(self):
    mbr_id = uuid.uuid4()
    mbr = MemberFromData(mbr_id)

    self.assertEquals(mbr.getId(), mbr_id)

