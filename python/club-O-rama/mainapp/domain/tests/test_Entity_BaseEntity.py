import unittest
import uuid

from entities.BaseEntity import BaseEntity

class BaseEntityTests(unittest.TestCase):
  def test_BaseEntity_must_have_GUID_ID(self):
    newid = uuid.uuid4()
    testobj = BaseEntity(newid)

    self.assertEqual(testobj.getId(), newid)

  def test_BaseEntity_gets_UUID_assigned_if_none_provided(self):
    testobj = BaseEntity()

    self.assertNotEqual(testobj.getId(), None)

  def test_BaseEntity_ID_is_GUID(self):
    testobj = BaseEntity(None)

    self.assertTrue(isinstance(testobj.getId(), uuid.UUID))

  def test_BaseEntity_InternalID_is_different_than_ID(self):
    testobj = BaseEntity(None, 2)

    self.assertNotEqual(testobj.getId(), testobj.getInternalId())

  def test_BaseEntity_InternalID_not_defined_must_return_None(self):
    testobj = BaseEntity(None, None)

    self.assertEqual(testobj.getInternalId(), None)
