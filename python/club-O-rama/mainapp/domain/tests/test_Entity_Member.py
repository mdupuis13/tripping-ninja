import unittest
import uuid

from entities.Member import Member

class MemberTest(unittest.TestCase):
  def test_Member_inherited_ID_from_BaseEntity(self):
    mbr = Member()

    self.assertTrue(isinstance(mbr.getId(), uuid.UUID))

