from sqlalchemy.ext.declarative import declared_attr
from custom_datatypes import GUID

class PrimaryKeyMixIn(object):

    id =  Column(GUID, primary_key=True)
