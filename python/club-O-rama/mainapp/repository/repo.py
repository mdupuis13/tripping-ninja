#!/usr/bin/python
# -*- coding: utf-8 -*-
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# cluborama data model
import dbmodel

class dbsession():

    def __init__(self, dbcontext, session):
        if dbcontext != None:
            self._dbcontext = dbcontext

        if session != None:
            self._session = session

    def __init_dbcontext(self, dbfilename):
        return create_engine(string.format('sqlite:///%s', dbfilename))

    def __init_session(self, dbcontext):
        return sessionmaker(bind=_dbcontext)

    def get_current_session(self, dbfilename):
        if self._session != None:
            self._dbcontext = __init_dbcontext(dbfilename)
            self._session = __init_session(self._dbcontext)

        return self._session

    def get_all_members(self):
        if self._session != None:
            return self._session.query(member).all()
        else:
            return ()

    def save_one_member(self, member):
        if self._session != None:
            self._session.add(member)
