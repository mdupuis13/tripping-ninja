#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Club O Rama data definition

Using SQL Alchemy class-based object-relational notation
"""
# imports for table definitions
from sqlalchemy import Table, PrimaryKeyConstraint
from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, Numeric, String
from sqlalchemy.orm import relationship
from custom_datatypes import GUID

from sqlalchemy.ext.declarative import declared_attr, declarative_base

# redefining Base
class Base(object):
    """Base class
    Base class redefined so that ALL tables contain versioning, created_date and modified_date columns
    """
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    # created_dt - automatic date to current date/time
    creat_dt = Column(DateTime)
    # modif_dt - gets the current date/time on modifications only. A NULL means it's an original record.
    modif_dt = Column(DateTime, nullable=True)
    # versioning is kept with a ROWVERSION-type column. The technique for this is taken from
    # SQLAlchemy's own documentation
    version_id =  Column(GUID, unique=True, nullable=False)

    __mapper_args__ = {
        'version_id_col':version_id,
        'version_id_generator':lambda version: uuid.uuid4()
    }

Base = declarative_base(cls=Base)

    # # Table definition - mbr_adr
# # Link between a member and an address
# mbr_adr = Table('mbr_adr', Base.metadata,
#     # mbr_pk - Guid
#     Column('mbr_pk', GUID, ForeignKey('member.mbr_pk'), primary_key=True),
#     Column('adr_pk', GUID, ForeignKey('address.adr_pk'), primary_key=True)
# )

# # Table definition - address
# class address(Base):
#     """address table
#     Contains basic data for an address
#     Has a many-to-many relationship with member
#     """
#     adr_pk = Column(GUID, primary_key=True, autoincrement=False)
#     # adr_type - value linked to param_list.list_short_value
#     adr_type = Column(String, nullable=True)
#     line1 = Column(String, nullable=False)
#     line2 = Column(String, nullable=True)
#     line3 = Column(String, nullable=True)
#     city = Column(String, nullable=True)
#     province = Column(String, nullable=True)
#     postalcode = Column(GUID, nullable=True)
#
#     members = relationship('member',
#                            secondary=mbr_adr,
#                            back_populates="addresses")
#
#     def __repr__(self):
#         return "<address('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')>" % (
#                 self.adr_pk, self.adr_type, self.line1,
#                 self.line2, self.line3, self.city,
#                 self.province, self.postalcode, self.members,
#                 self.creat_dt, self.modif_dt, self.version_id)

# # Table definition - event_subscription
# # List's an event's subscribers (member)
# event_subscription = Table('event_subscription', Base.metadata,
#     # mbr_pk - Guid
#     Column('event_pk', GUID, ForeignKey('event.event_pk'), primary_key=True),
#     Column('mbr_pk', GUID, ForeignKey('member.mbr_pk'), primary_key=True)
# )

# # Table definition - events
# # One line for each event.
# class Event(Base):
#     # event_pk - Guid
#     event_pk = Column(GUID, primary_key=True)
#     event_dt = Column(DateTime)
#     title = Column(String, nullable=True)
#     subscription_limit_dt = Column(DateTime, nullable=True)
#     # links to a file in the "file" table
#     linked_map = Column(GUID, nullable=True)
#     note = Column(String, nullable=True)
#     cancelled = Column(Boolean)
#     need_volonteer = Column(Boolean)
#
#     members = relationship("Member",
#                            secondary=event_subscription,
#                            back_populates="events")
#     # links to a route
#     # route_pk_route = Column(GUID, sa.ForeignKey('route.route_pk'))
#
#     def __repr__(self):
#         return "<event('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', members:%s, '%s', '%s', '%s')>" % (
#                 self.event_pk, self.event_dt, self.title,
#                 self.subscription_limit_dt, self.linked_map, self.note,
#                 self.cancelled, self.need_volonteer, self.members,
#                 self.creat_dt, self.modif_dt, self.version_id)

# # Table definition - login_cred
# # Login credentials for a member. Password must be hashed using current strong technology
# class User(Base):
#     user_pk = Column(GUID, primary_key=True)
#     mbr_pk = Column(GUID, ForeignKey('member.mbr_pk'))
#     # username - could be member's email by default
#     username = Column(String, nullable=False)
#     # password - hashed pwd with most secure hash algo of the moment (bcrypt, sha512_crypt ?)
#     password = Column(String, nullable=False)
#     member = relationship("Member", back_populates="user")
#
#     def __repr__(self):
#         return "<user('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')>" % (
#                 self.user_pk, self.mbr_pk, self.username,
#                 self.password, self.member,
#                 self.creat_dt, self.modif_dt, self.version_id)

# Table definition - member
# List of members of the club
class member(Base):
    """member table
    Contains basic data for a member

    """
    mbr_pk = Column(GUID, primary_key=True)
    member_no = Column(String(15), nullable=True)
    firstname = Column(String, nullable=True)
    lastname = Column(String, nullable=True)
    # speed_group - value linked to param_list.list_short_value
    speed_group = Column(String, nullable=True)
    active = Column(Boolean, nullable=False, default=False)
    is_admin = Column(Boolean, nullable=False, default=False)
    # is_on_bod - 1 = is on board of directors (automatically confers admin rights)
    is_on_bod = Column(Boolean, nullable=False, default=False)

    # addresses = relationship('address', secondary=mbr_adr, back_populates="members")
    # communication_methods = relationship("MemberCommunicationInformation", back_populates="member")
    # events = relationship('Event', secondary=event_subscription, back_populates="members")
    # subscriptions = relationship("MemberSubscription", back_populates="subscription")
    # user = relationship("User", uselist=False, back_populates="member")

    def __repr__(self):
        return "<member('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')>" % (
                self.mbr_pk, self.member_no, self.firstname,
                self.lastname, self.speed_group, self.active,
                self.is_admin, self.is_on_bod,
                self.creat_dt, self.modif_dt, self.version_id)

        # return "<member('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', addresses:%s, communication_methods:%s, events:%s, subscriptions:%s,  user:%s, '%s', '%s', '%s')>" % (
        #         self.mbr_pk, self.member_no, self.firstname,
        #         self.lastname, self.speed_group, self.active,
        #         self.is_admin, self.is_on_bod, self.addresses,
        #         # self.communication_methods, self.events, self.subscriptions,
        #         # self.user,
        #         self.crea

# # Table definition - mbr_comm
# # Ways to contact a member
# class MemberCommunicationInformation(Base):
#     mbrcomminfo_pk = Column(GUID, primary_key=True)
#     mbr_pk = Column(GUID, ForeignKey("member.mbr_pk"))
#     # method - value linked to param_list.list_short_value
#     commmethod = Column(String, nullable=True)
#     value = Column(String, nullable=True)
#     member = relationship("Member", back_populates="communication_methods")
#
#     def __repr__(self):
#         return "<membercommunicationinformation('%s', '%s', '%s', '%s', members:%s, '%s', '%s', '%s')>" % (
#                 self.mbrcomminfo_pk, self.mbr_pk, self.method,
#                 self.value, self.member,
#                 self.creat_dt, self.modif_dt, self.version_id)

# # Table definition - subscription
# class Subscription(Base):
#     subs_pk = Column(GUID, primary_key=True)
#     year = Column(Integer, nullable=True)
#     price = Column(Numeric(8,2), nullable=True)
#     members = relationship("MemberSubscription", back_populates="member")
#
#     def __repr__(self):
#         return "<subscription('%s', '%s', '%s', members:%s, '%s', '%s', '%s')>" % (
#                 self.subs_pk, self.year, self.price,
#                 self.members,
#                 self.creat_dt, self.modif_dt, self.version_id)

# # Table definition - mbr_subscription
# # A member's subscription
# class MemberSubscription(Base):
#     mbr_pk = Column(GUID, ForeignKey('member.mbr_pk'), primary_key=True)
#     subs_pk = Column(GUID, ForeignKey('subscription.subs_pk'), primary_key=True)
#     is_paid = Column(Boolean, default=False)
#     member = relationship("Subscription", back_populates="members")
#     subscription = relationship("Member", back_populates="subscriptions")
#
#     def __repr__(self):
#         return "<membersubscription('%s', '%s', '%s', members:%s, subscriptions:%s, '%s', '%s', '%s')>" % (
#                 self.mbr_pk, self.subs_pk, self.is_paid,
#                 self.member, self.subscription,
#                 self.creat_dt, self.modif_dt, self.version_id)

# # Table definition - route
# # A route has a title and multiple maps, distinguished per distance or other parameters (difficulty or else).
# class route(Base):
#     # route_pk - Guid
#     route_pk = Column(GUID, primary_key=True)
#     route_title = Column(String, nullable=True)
#     # route_master_map_file - Usually points to a PDF file with all the maps inside.
#     route_master_map_file = Column(GUID, sa.ForeignKey('file.file_pk') nullable=True)

# # Table definition - map
# # A maps has a title, a start address/map and one or more map files (PDF and GPX).
# class map(Base):
#     # map_pk - Guid
#     map_pk = Column(GUID, primary_key=True)
#     map_title = Column(String)
#     # map_start - Guid
#     map_start = Column(GUID, sa.ForeignKey('map_start.mapstart_pk'))

# # Table definition - file
# class File(Base):
#     file_pk = Column(GUID, primary_key=True)
#     # this filename will be shown on screen, it could be derived automatically from uploaded file
#     filename = Column(String, unique=True)
#     file_desc = Column(String)
#     # os_filename - filename as seen on the filesystem, relative to a common repository
#     os_filename = Column(String)

# # Table definition - route_map
# class route_map(Base):
#     # routemap_pk - Guid
#     routemap_pk = Column(GUID, primary_key=True)
#     routemap_name = Column(String)
#     # route_pk_route - Guid
#     route_pk_route = Column(GUID, sa.ForeignKey('route.route_pk'))
#     # map_pk_map - Guid
#     map_pk_map = Column(GUID, sa.ForeignKey('map.map_pk'))

# # Table definition - map_file
# # Usually, one PDF file and one GPX file per map.
# class map_file(Base):
#     # mapfile_pk - Guid
#     mapfile_pk = Column(GUID, primary_key=True)
#     # file_pk_file - Guid
#     file_pk_file = Column(GUID, sa.ForeignKey('file.file_pk'))
#     # map_pk_map - Guid
#     map_pk_map = Column(GUID, sa.ForeignKey('map.map_pk'))

# # Table definition - params
# # General parameters for the application
# class Param(Base):
#     param_pk = Column(GUID, primary_key=True)
#     param_name = Column(String, unique=True, nullable=False)
#     # param_value - param_value is 'list' if the parameter is a list. Then, see param_list table.
#     param_value = Column(String)
#
#     def __repr__(self):
#         return "<user('%s', '%s', '%s', '%s', '%s', '%s')>" % (
#                 self.param_pk, self.param_name, self.param_value,
#                 self.creat_dt, self.modif_dt, self.version_id)

# # Table definition - param_list
# class ParamList(Base):
#     # paramlist_pk - Guid
#     paramlist_pk = Column(GUID, primary_key=True)
#     param_pk = Column(GUID, ForeignKey('params.param_pk'))
#     list_short_value = Column(String(6), nullable=False)
#     list_value = Column(String)
#
#     def __repr__(self):
#         return "<user('%s', '%s', '%s', '%s', '%s', '%s', '%s')>" % (
#                 self.paramlist_pk, self.param_pk, self.list_short_value,
#                 self.list_value,
#                 self.creat_dt, self.modif_dt, self.version_id)
