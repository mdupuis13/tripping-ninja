import os
import sys
sys.path.insert(0, os.path.abspath('..'))

import unittest
# from dbmodel import Param, ParamList

@unittest.skip("Param class not defined")
class param_test(unittest.TestCase):
    def test_param_creation_returns_a_Param_with_no_primarykey(self):
        param = Param()
        self.assertIsNone(param.param_pk)


@unittest.skip("ParamList class not defined")
class paramlist_test(unittest.TestCase):
    def test_paramlist_creation_returns_a_ParamList_with_no_primarykey(self):
        plist = ParamList()
        self.assertIsNone(plist.paramlist_pk)


if __name__ == "__main__":
    unittest.main()
