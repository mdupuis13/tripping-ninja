import os
import sys
sys.path.insert(0, os.path.abspath('..'))

import unittest
from dbmodel import member

class member_test(unittest.TestCase):
    def test_new_member_creation_returns_a_member_with_no_primarykey(self):
        mbr = member()
        self.assertIsNone(mbr.mbr_pk)

    def test_member_creation_with_info_returns_an_initialised_member(self):
        expected = "<member('None', '123456', 'Martin', 'Dupuis', '20-25', 'False', 'False', 'False', 'None', 'None', 'None')>"
        mbr = member(member_no='123456', firstname='Martin', lastname='Dupuis',
                     speed_group='20-25', active=False, is_admin=False, is_on_bod = False)
        self.assertEqual(expected, str(mbr))

    @unittest.skip("address class not defined")
    def test_member_creation_with_address_contains_the_address(self):
        expected = "500 rue Lafleur"
        adr = address(line1="500 rue Lafleur", city="Montreal")
        mbr = member(member_no='123456', firstname='Martin', lastname='Dupuis',
                     speed_group='20-25', active=False, is_admin=False, is_on_bod = False, addresses=[adr])

        self.assertEqual(expected, mbr.addresses[0].line1)

if __name__ == "__main__":
    unittest.main()
