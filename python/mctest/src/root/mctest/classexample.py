'''
Created on 2011-02-25

@author: mdupuis
'''
__metaclass__ = type #Make sure we get new style classes
class Forme():
    def __init__(self):
        print 'init of Forme'
        
    def descr(self):
        print 'Je suis un', self.form
        
    def __del__(self):
        print 'J\'etais un', self.form
        
class Geometrique(Forme):
    def __init__(self, value):
        self.form = value + '.'
        # calling superclass constructor
        super(Geometrique, self).__init__()
        
    def __del__(self):
        print '    Geometrique __del__'
        super(Geometrique, self).__del__()
        
if __name__ == '__main__':
    orond = Geometrique('rond')
    ocarre = Geometrique('carre')
    orond.descr()
    ocarre.descr()