'''
Created on 2011-02-25

@author: mdupuis
'''
def faulty():
    raise Exception('something wong !')

def ignoreException():
    faulty()
    
def handleException():
    try:
        faulty()
    except:
        print 'exception handled'
        
def loopException():
    while True:
        try:
            x = input('First number:')
            y = input('Second number:')
            x *= 1.0
            value = x/y
            print 'x/y is', value
        except:
            print 'invalid input...'
        else:
            break
    
if __name__ == '__main__':
    #handleException()
    #ignoreException()
    loopException()