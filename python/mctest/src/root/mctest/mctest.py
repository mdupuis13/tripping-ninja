'''
Created on 2011-02-22

@author: md
'''
from mailsnake import MailSnake
import mcAPIkeys

global f

def initMS(apikey):
    return MailSnake(apikey)

def getLists(objms):
    #get all the lists defined
    mclists = objms.lists()
    
    #iterate through the lists and extract information
    for info,value in mclists.iteritems():
        if type(value) is list:
            
            for valuedir in value:
                #for x in valuedir:
                    #pre = "%s: %s:" % (valuedir['name'], x)
                    #if x == 'stats':
                        #print(pre)
                        #printListStats(len(pre), valuedir[x])
                    #else:
                        #print("%s %s" % (pre, valuedir[x]))
                    
                    
                listid = valuedir.get('id')
                
                getMembers(objms, valuedir['name'], listid)
        else:
            print("%s: %s" % (info, value))


def getMembers(objms, listname, listid):
    global f
    mcmb = ms.listMembers(id=listid, status='subscribed', limit=10000)
    print("%s: Total abonnes: %s" % (listname, mcmb['total']))

    mcmbemail = mcmb['data']
    
    for member in mcmbemail:
        #print(u"%s: Email: %s" % (listname, member['email']))
        
        try:
            f.write(u"%s: Email: %s\r\n" % (listname, str(member['email']).decode("utf8")))
        except UnicodeEncodeError:
            pass
            
    f.flush()
    
def printListStats(nbemptyspaces, stats):
    pre = (' ' * nbemptyspaces) + ' '
    for stat in stats:
        print("%s %s: %s" % (pre,stat, stats[stat]))
    
if __name__ == '__main__':
    global f
    
    from random import choice
    #choose which key to use for the connection to MailChimp API
    mcapikey = choice(mcAPIkeys.mcapikeys)
    
    ms = initMS(mcapikey)
    #print('Key choosen:', mcapikey)
    
    f = open("mctestfile.txt", "w")
    f.write('Key choosen: %s\r\n' % (mcapikey))

    getLists(ms)
    
