from datetime import datetime
from datetime import timedelta
from enum import Enum

from pprint import pprint

from .event import Event
from .timeseries import TimeSeries, MovingAverage, NotEnoughDataException

class StockSignal(Enum):
  buy = 1
  neutral = 0
  sell = -1

class Stock:
  """  """
  DATE_FORMAT = "%Y%m%d"

  def __init__(self, symbol):
    self.symbol = symbol
    self.history = TimeSeries()
    self.updated = Event()


  @property
  def price(self):
    """ property price(self)
    Returns the lastest price available ALWAYS
    """
    return self.get_price()

  @price.setter
  def price(self, value):
    """property price(self, value)
    Adds/update the price to the history with the current date
    """
    self.update(datetime.today(), value)


  def get_price(self, timestamp=None):
    """ get_price(self, timestamp=None)
    Returns the lastest price if no 'date' given
    Returns the price for the date received in param or the first OLDER date with a price available
    """
    if timestamp == None:
      try:
        return self.history[-1].closing_price

      except IndexError:
        return None

    else:
      retval = [hist.closing_price for hist in self.history if hist.timestamp.date() == timestamp.date()]

      if retval != None and len(retval) > 0:
        return retval[0]

      else:
        retval = [hist.closing_price for hist in reversed(self.history) if hist.timestamp.date() <= timestamp.date()]

        if retval != None and len(retval) > 0:
            return retval[0]
        else:
          raise KeyError


  def update(self, timestamp, price):
    """update(self, timestamp, price):
    Updates the history with the information received
    If key already exists, update with the price provided
    """
    if price < 0:
      raise ValueError("Price should not be negative")

    self.history.update(timestamp, price)
    self.updated.fire(self)

  def is_increasing_trend(self):
    """is_increasing_trend(self)
    An increasing trend is detected if the last three prices are increasing
    """
    return self.history[-3].closing_price < self.history[-2].closing_price < self.history[-1].closing_price


  def is_decreasing_trend(self):
    """is_decreasing_trend(self)
    A decreasing trend is detected if the last three prices are decreasing
    """
    return self.history[-3].closing_price > self.history[-2].closing_price > self.history[-1].closing_price


  def get_crossover_signal(self, on_date, short_avg_nb_days, long_avg_nb_days):
    prev_date = on_date - timedelta(1)

    long_term_ma = MovingAverage(self.history, long_avg_nb_days)
    short_term_ma = MovingAverage(self.history, short_avg_nb_days)

    try:
      if self._is_crossover_below_to_above(on_date, short_term_ma, long_term_ma):
        return StockSignal.buy

      if self._is_crossover_below_to_above(on_date, long_term_ma, short_term_ma):
        return StockSignal.sell

    except NotEnoughDataException:
      return StockSignal.neutral

    return StockSignal.neutral


  def _is_crossover_below_to_above(self, on_date, ma, reference_ma):
    prev_date = on_date - timedelta(1)
    return (ma.value_on(prev_date) < reference_ma.value_on(prev_date)
            and ma.value_on(on_date) > reference_ma.value_on(on_date))
