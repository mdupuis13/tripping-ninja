import json

import re # regular expressions

import urllib.parse
import urllib.request

class YahooReader:
  def __init__(self, source):
    """Receives source to use in param.
    'source' could be url to Yahoo Query Language sourced to store://datatables.org/alltableswithkeys
     or dictionnary with the same structure (see yahootestdata.py)
    """
    if source == None:
      raise ValueError("'source' cannot be None")
    
    self.rawdata = {}
    
    #use regex to detect url
    if type(source) is str:
      yqlpattern = re.compile('http[s]://query.yahooapis.com/v1/public/yql(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+')
      
      if yqlpattern.match(source): #yql.match():
        self.rawdata = self._get_data_from_yql(source)
      else:
        raise ValueError("'source' is not a Yahoo Query Language source")
        
    elif type(source) is dict:
      self.rawdata = source


  def _get_data_from_yql(self, url):
    try:
      with urllib.request.urlopen(url) as response:
        url_resp = response.read()
    
      return json.loads(url_resp.decode(encoding='UTF-8'))
    
    except urllib.error.URLError as urlerr:
      return None
      
