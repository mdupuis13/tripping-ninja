from datetime import datetime
import unittest

from pprint import pprint

# the class we are currently testing
from ..stock import Stock
from ..stock import StockSignal


class StockTest(unittest.TestCase):
  def setUp(self):
    self._stock = Stock("GLD")


  def given_a_series_of_prices(self, prices):
    timestamps = [datetime(2014, 2, 10), datetime(2014, 2, 11), datetime(2014, 2, 12), datetime(2014, 2, 13),
                  datetime(2014, 2, 14), datetime(2014, 2, 15), datetime(2014, 2, 16), datetime(2014, 2, 17),
                  datetime(2014, 2, 18), datetime(2014, 2, 19), datetime(2014, 2, 20), datetime(2014, 2, 21),
                  datetime(2014, 2, 22), datetime(2014, 2, 23), datetime(2014, 2, 24), datetime(2014, 2, 25),
                 ]

    for timestamp, price in zip(timestamps, prices):
      self._stock.update(timestamp, price)

  def test_price_of_a_new_stock_class_should_be_None(self):
    """The price of new stock should be None
    """
    self.assertIsNone(self._stock.price)


  def test_hist_of_a_new_stock_class_should_be_Empty_history(self):
    """The history of new stock should be an empty dict
    """
    self.assertEqual(0, len(self._stock.history))


  def test_update_stockprice_property(self):
    """An update should set the price on the stock object
       The price should be set for the current day
    """
    self._stock.price = 10
    self.assertEqual(self._stock.get_price(datetime.today()), 10)


  def test_update_stockprice_for_given_date(self):
    """An update should set the price on the stock object for the date given
       We will be using the `datetime` module for the timestamp
    """
    self.given_a_series_of_prices([10])
    self.assertEqual(self._stock.price, 10)


  def test_negative_price_should_throw_ValueError(self):
    """We should not accept a negative price
    """
    with self.assertRaises(ValueError):
      self._stock.update(datetime(2014, 2, 13), -1)


  def test_stock_price_should_give_the_latest_price(self):
    """Price property should always give the recent price
    """
    self.given_a_series_of_prices([10, 8.4])
    self.assertAlmostEqual(8.4, self._stock.price, delta=0.0001)


  def test_get_price_for_non_existing_day(self):
    """GETting the price for a day that has no history should
    return the price of the first day available prior to the given day
    """
    self._stock.update(datetime(2014, 2, 11), price=11)
    self._stock.update(datetime(2014, 2, 15), price=10)

    self.assertEqual(11, self._stock.get_price(datetime(2014, 2, 13)))


  def test_get_price_for_non_existing_day_no_price_before(self):
    """GETting the price for a day that has no history before should return None
    """
    self.given_a_series_of_prices([10, 11])

    with self.assertRaises(KeyError):
      self._stock.get_price(datetime(2014, 2, 9))


  def test_get_price_for_existing_day(self):
    """GETting the price for a specific day should return the correct price
    """
    self.given_a_series_of_prices([10, 11, 12])
    self.assertAlmostEqual(self._stock.get_price(datetime(2014, 2, 11)), 11, delta=0.0001)



class StockTrendTest(unittest.TestCase):
  def setUp(self):
    self._stock = Stock("GLD")


  def given_a_series_of_prices(self, prices):
    timestamps = [datetime(2014, 2, 10), datetime(2014, 2, 11), datetime(2014, 2, 12), datetime(2014, 2, 13),
                  datetime(2014, 2, 14), datetime(2014, 2, 15), datetime(2014, 2, 16), datetime(2014, 2, 17),
                  datetime(2014, 2, 18), datetime(2014, 2, 19), datetime(2014, 2, 20), datetime(2014, 2, 21),
                  datetime(2014, 2, 22), datetime(2014, 2, 23), datetime(2014, 2, 24), datetime(2014, 2, 25),
                 ]

    for timestamp, price in zip(timestamps, prices):
      self._stock.update(timestamp, price)


  def test_increasing_trend_is_true_if_price_increase_for_3_updates(self):
    """Three increasing prices is a upward trend
    """
    self.given_a_series_of_prices([8, 10, 12])

    self.assertTrue(self._stock.is_increasing_trend())


  def test_increasing_trend_is_false_if_price_decreases(self):
    """Not an upward trend if a price is less than the two others
    """
    self.given_a_series_of_prices([8, 12, 10])

    self.assertFalse(self._stock.is_increasing_trend())


  def test_increasing_trend_is_false_if_price_equal(self):
    """Not an upward trend if two prices are equal
    """
    self.given_a_series_of_prices([8, 10, 10])

    self.assertFalse(self._stock.is_increasing_trend())


  def test_decreasing_trend_is_true_if_price_decrease_for_3_updates(self):
    """Three decreasing prices is a downward trend
    """
    self.given_a_series_of_prices([12, 10, 8])

    self.assertTrue(self._stock.is_decreasing_trend())


  def test_decreasing_trend_is_false_if_price_increases(self):
    """Not an upward trend if a price is less than the two others
    """
    self.given_a_series_of_prices([12, 8, 10])

    self.assertFalse(self._stock.is_decreasing_trend())


  def test_decreasing_trend_is_false_if_price_equal(self):
    """Not an upward trend if two prices are equal
    """
    self.given_a_series_of_prices([10, 10, 8])

    self.assertFalse(self._stock.is_increasing_trend())


  def test_crossoversignal_to_sell(self):
    """ Testing a sell signal based on DMAC (dual crossover moving average)
     DO NOT CHANGE THE PRICES BELOW !!!
   """
    self.given_a_series_of_prices([23, 23, 23, 23,
                                   23, 20, 23, 23,
                                   23, 23, 23, 23,
                                   22, 23, 24, 24])
    result = self._stock.get_crossover_signal(datetime(2014, 2, 23), 3, 8)
    self.assertEqual(StockSignal.sell, result)


  def test_crossoversignal_to_buy(self):
    """ Testing a buy signal based on DMAC (dual crossover moving average)
    DO NOT CHANGE THE PRICES BELOW !!!
    """
    self.given_a_series_of_prices([35,   55,  133,  125,
                                   133,   74, 43, 26,
                                   26, 25 , 24, 45,
                                   35, 53, 30, 38])
    result = self._stock.get_crossover_signal(datetime(2014, 2, 23), 3, 8)
    self.assertEqual(StockSignal.buy, result)














