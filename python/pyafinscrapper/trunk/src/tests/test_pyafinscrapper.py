import unittest

from ..pyafinscrapper import YahooReader
from ..yahootestdata import *

from pprint import pprint

class PyafinscrapperTest(unittest.TestCase):

  def test_class_creation_source_cannot_be_none(self):
    with self.assertRaises(ValueError):
      self.yaread = YahooReader(None)


  def test_data_source_is_dict(self):
    yaread = YahooReader(yahootestdata)
    self.assertIsNotNone(yaread.rawdata)

  #~def test_data_source_is_not_yql(self):
    #~with self.assertRaises(ValueError):
      #~self.yaread = YahooReader("https://google.com")
#~
#~
  #~def test_data_from_yql_is_dict(self):
    #~testurl = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.historicaldata%20where%20symbol%20%3D%20%22{ticker}%22%20and%20startDate%20%3D%20%22{startdate}%22%20and%20endDate%20%3D%20%22{enddate}%22&format={resultformat}&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback="
    #~testurl = testurl.format_map({'ticker':       "HR-UN.TO",
                                  #~'startdate':    "2015-01-01",
                                  #~'enddate':      "2015-06-30",
                                  #~'resultformat': "json"})
    #~self.yaread = YahooReader(testurl)
#~
    #~self.assertTrue("query" in self.yaread.rawdata.keys())

  #~def test_web_not_accessible(self):
    #~testurl = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.historicaldata%20where%20symbol%20%3D%20%22HR-UN.TO%22%20and%20startDate%20%3D%20%222013-06-30%22%20and%20endDate%20%3D%20%222014-07-28%22&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback="
    #~self.yaread = YahooReader(testurl)
#~
    #~self.assertEqual(None, self.yaread.rawdata)
