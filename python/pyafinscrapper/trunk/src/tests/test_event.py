import unittest
from unittest import mock
from pprint import pprint

# the class we are currently testing
from ..event import Event
from ..stock import Stock

class test_event(unittest.TestCase):
  def test_a_listener_is_notified_when_an_event_is_raised(self):
    listener = mock.MagicMock()
    event = Event()
    event.connect(listener)
    event.fire()
    self.assertTrue(listener.called)


  def test_a_listener_is_passed_right_parameters(self):
    listener = mock.MagicMock()
    event = Event()
    event.connect(listener)
    event.fire(5, shape="square")
    listener.assert_has_calls([mock.call(5, shape="square")])


