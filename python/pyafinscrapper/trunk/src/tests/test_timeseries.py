import unittest

from pprint import pprint
import collections

from datetime import datetime
from datetime import timedelta


# the class we are currently testing
from ..timeseries import NotEnoughDataException

class test_NotEnoughDataException(unittest.TestCase):
  def test_not_enough_data(self):
    nedexcep = NotEnoughDataException


# the class we are currently testing
from ..timeseries import TimeSeries

class test_TimeSeries(unittest.TestCase):
  def setUp(self):
    self._timeseries = TimeSeries()

  def test_update_stockprice(self):
    self._timeseries.update(datetime.now(), 10)

  def test_retrieve_stockprice(self):
    ts = datetime.now()
    self._timeseries.update(ts, 10)
    self.assertEqual(10, self._timeseries[0].closing_price)
    self.assertEqual(ts, self._timeseries[0].timestamp)

  def test_items_are_in_chronological_order(self):

    # inserting items in reversed order
    for idx in reversed(range(4)):
      ts = datetime(2015,1,14) - timedelta(idx)
      self._timeseries.update(ts, int(ts.strftime("%d")))

    # trying to read in chronological order
    self.assertEqual(11, self._timeseries[0].closing_price)
    self.assertEqual(12, self._timeseries[1].closing_price)
    self.assertEqual(13, self._timeseries[2].closing_price)
    self.assertEqual(14, self._timeseries[3].closing_price)

  def test_len(self):
    self._timeseries.update(datetime.now(), 12)
    self.assertEqual(1, len(self._timeseries))

  def test_get_closing_price_list(self):
    Update = collections.namedtuple("Update", ["timestamp", "closing_price"])

    expected_result = [Update(datetime(2015, 1, 12), 12),
                       Update(datetime(2015, 1, 13), 13)]

    for idx in range(4):
      ts = datetime(2015,1,14) - timedelta(idx)
      self._timeseries.update(ts, int(ts.strftime("%d")))

    #~pprint("expected: {}".format(expected_result))
    #~pprint(self._timeseries.get_closing_price_list(datetime(2015,1,14),2))

    self.assertEqual(expected_result, self._timeseries.get_closing_price_list(datetime(2015, 1, 13), 2))


# the class we are currently testing
from ..timeseries import MovingAverage

class test_MovingAverage(unittest.TestCase):
  def setUp(self):
    self._movingaverage = MovingAverage(None, None)

  def test_init(self):
    self._movingaverage = MovingAverage(1,2)

    self.assertEqual(1, self._movingaverage.series)
    self.assertEqual(2, self._movingaverage.timespan)
