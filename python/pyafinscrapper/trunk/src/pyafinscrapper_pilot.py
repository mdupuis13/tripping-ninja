#!/usr/bin/python3
import json
import pprint
import string
import urllib.request

YQL_BASE_QRY  = '''select+*+from+yahoo.finance.historicaldata+where+symbol+%3D+"{0}"+and+startDate+%3D+"{1}"+and+endDate+%3D+"{2}"&format={3}&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback='''

BASE_URL = "https://query.yahooapis.com/v1/public/yql?"
#yql_query = "select wind from weather.forecast where woeid=2460286" + "&format=json"
yql_query = YQL_BASE_QRY.format("HR-UN.TO", "2014-08-01", "2015-07-31", "json")
yql_url = BASE_URL + 'q=' + yql_query #+ "&format=json"

#pprint.pprint(yql_url)

with urllib.request.urlopen(yql_url) as f:
  data = f.read().decode('utf-8')

#pprint.pprint(data)
data = json.loads(data)

#pprint.pprint(data['query']['results']['quote'])

print("date;price")
for hist in data['query']['results']['quote']:
  print("{0};{1}".format(hist['Date'], hist['Close']))
