#!/bin/python

def funca(param1, param2):
  return param1 * param2
  
  
vara = funca("varA", 1)
varb = funca("varB", 2)

print(vara)
print(varb)

varc = lambda prnt, x: prnt * x
print(varc("varC", 3))
