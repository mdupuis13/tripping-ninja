class APerson():
    def __init__(self, name):
        self.Name = name

    def getGender(self):
        raise NotImplementedError("Subclass must implement abstracct method")
    
class Man(APerson):
    def getGender(self):
        return "male"
        
class Woman(APerson):
    def getGender(self):
        return "female"
        
if __name__ == "__main__":
    persons = [Man("Martin"),
               Woman("Geneviève")]
    
    for person in persons:
        print("%s is a %s" % (person.Name, person.getGender()))
        
#comment faire pour que prsn appelle Man.GetGender en pensant à martin?
#est-ce que python connait la notion de classe abstraite ?
