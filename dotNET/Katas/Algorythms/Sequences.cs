﻿using System;

namespace Kata.Algorythms
{
    public class Sequences
    {
        public static double fibonacci(int number)
        {
            if (number < 0)
            {
                throw new ArgumentOutOfRangeException("number", "negative numbers not supported yet");
            }

            if (number == 0 || number == 1)
            {
                return number;
            }

            return fibonacci(number - 1) + fibonacci(number - 2);
        }

        public static double factorial(int number)
        {
            if (number < 0)
            {
                throw new ArgumentOutOfRangeException("number", "number cannot be negative");
            }

            if (number == 0)
            {
                return 1;
            }

            return factorial(number - 1) * number;
        }
    }
}
