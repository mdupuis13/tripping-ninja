﻿using System;

using Xunit;
using FluentAssertions;

namespace Kata.Algorythms.Tests
{
    public class SequencesTest
    {
        [Theory]
        [InlineData(0, 0)]
        [InlineData(1, 1)]
        [InlineData(2, 1)]
        [InlineData(3, 2)]
        [InlineData(10, 55)]
        [InlineData(14, 377)]
        public void FibonacciPositiveNumber_Test(int value, double expected)
        {
            Sequences.fibonacci(value).Should().Be(expected);
        }

        [Fact]
        public void FibonacciNegativeNumber_Test()
        {
            System.Action callingWithANegativeValue = () => Sequences.fibonacci(-1);

            callingWithANegativeValue.ShouldThrow<ArgumentOutOfRangeException>("Negative values not supported.");
        }

        [Theory]
        [InlineData(0, 1)]
        [InlineData(1, 1)]
        [InlineData(2, 2)]
        [InlineData(3, 6)]
        [InlineData(10, 3628800)]
        [InlineData(14, 87178291200)]
        public void FactorielPositiveNumber_Test(int value, double expected)
        {
            Sequences.factorial(value).Should().Be(expected);
        }

        [Fact]
        public void FactorielNegativeNumber_Test()
        {
            System.Action callingWithANegativeValue = () => Sequences.factorial(-1);

            callingWithANegativeValue.ShouldThrow<ArgumentOutOfRangeException>("Negative values not supported.");
        }
    }
}
