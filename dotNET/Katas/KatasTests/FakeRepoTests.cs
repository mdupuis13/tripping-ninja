﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kata.FakeRepo.Entities;

using Xunit;
using FluentAssertions;
using Moq;

namespace Kata.FakeRepo.Tests
{
    public class When_CustomerRepo_Tests
    {
        [Fact]
        public void Then_Calling_getDBName_Test()
        {
            var fakedbCtx = new Mock<IDatabaseContext>();
            fakedbCtx.Setup(ctx => ctx.getDBName()).Returns("MaBD");

            ICustomerRepository custrepo = new CustomerRepository(fakedbCtx.Object);
            custrepo.getDBContext().getDBName().Should().Be("MaBD");
        }

        [Fact]
        public void Then_getting_one_customers()
        {
            // Arrange
            var fakeDbCtx = new Mock<IDatabaseContext>();
            fakeDbCtx.Setup(ctx => ctx.fetch()).Returns(new List<Object>()
                    { new Customer(1, "John Doe"),
                      new Customer(2, "Jane Doe")}
            );

            // Act
            CustomerRepository custrepo = new CustomerRepository(fakeDbCtx.Object);
            Customer cust = custrepo.getOneCustomer(2);

            //Assert
            fakeDbCtx.Verify(m => m.fetch());
            cust.getFullname().Should().Be("Jane Doe");
        }
    }
}