﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.FakeRepo
{
    public interface IDatabaseContext
    {
        string getDBName();
        void save();
        void update();
        List<object> fetch();
    }

    public class DatabaseContext: IDatabaseContext
    {
        public string getDBName()
        {
            return "dbName";
        }

        public void save() { }

        public void update() { }

        public List<object> fetch()
        {
            return new List<object>();
        }
    }
}
