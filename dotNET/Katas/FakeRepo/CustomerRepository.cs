﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kata.FakeRepo.Entities;

namespace Kata.FakeRepo
{
    public interface ICustomerRepository
    {
        IDatabaseContext getDBContext();
    }

    public class CustomerRepository: ICustomerRepository
    {
        private IDatabaseContext _dbContext;

        public CustomerRepository(IDatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IDatabaseContext getDBContext()
        {
            return _dbContext;
        }

        public Customer getOneCustomer(int CustomerID)
        {
            List<Object> objs = _dbContext.fetch();
            List<Customer> custs = new List<Customer>();
            
            foreach (Object obj in objs){
                custs.Add((Customer)obj);
            }

            Customer cust = custs.Where(x => x.getInternalID() == CustomerID).FirstOrDefault();

            return cust;
        }
    }
}
