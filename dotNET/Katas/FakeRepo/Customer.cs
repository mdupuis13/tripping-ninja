﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.FakeRepo.Entities
{
    public class Customer
    {
        private string _name;
        private int _Id;

        public Customer(int ID, string Fullname)
        {
            _Id = ID;
            _name = Fullname;
        }

        public int getInternalID()
        {
            return _Id;
        }

        public string getFullname()
        {
            return _name;
        }
    }
}
