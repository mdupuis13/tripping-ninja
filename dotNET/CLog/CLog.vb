Imports System.IO

Public Class Clog
    Private m_strmLogFile As StreamWriter
    Private m_intOptions As Integer = 0

    Public Enum OptionsEnum As Integer
        optIncludeNothing = 0
        optIncludeDate = 1
        optIncludeTime = 2
    End Enum

    Public WriteOnly Property Options() As OptionsEnum
        Set(ByVal Value As OptionsEnum)
            m_intOptions = Value
        End Set
    End Property

    Public Function Open(ByVal LogFileName As String, _
                         Optional ByVal AppendMode As Boolean = True) As Boolean
        Try
            m_strmLogFile = New StreamWriter(LogFileName, AppendMode, Text.Encoding.UTF8)
            Return True

        Catch ex As Exception
            MsgBox(ex.Message, , ex.Source)
            Return False
        End Try
    End Function

    Public Sub Close()
        'm_strmLogFile.Write(ControlChars.CrLf)
        m_strmLogFile.Close()

        m_strmLogFile = Nothing
    End Sub

    Public Sub Write(ByVal Message As String)
        CheckForOptions(Message)

        m_strmLogFile.Write(Message)
        m_strmLogFile.Flush()
    End Sub

    Public Sub WriteLn(ByVal Message As String)
        CheckForOptions(Message)

        m_strmLogFile.WriteLine(Message)
        m_strmLogFile.Flush()
    End Sub

    Private Sub CheckForOptions(ByRef Message As String)
        Dim strMess As String = ""

        If (m_intOptions Or OptionsEnum.optIncludeNothing) = OptionsEnum.optIncludeNothing Then
            strMess = ""
        Else
            If (m_intOptions And OptionsEnum.optIncludeDate) = OptionsEnum.optIncludeDate Then
                strMess += Now.ToShortDateString & " "
            End If

            If (m_intOptions And OptionsEnum.optIncludeTime) = OptionsEnum.optIncludeTime Then
                strMess += Now.ToShortTimeString & " "
            End If
        End If

        Message = strMess + Message
    End Sub

    Protected Overrides Sub Finalize()
        If Not m_strmLogFile Is Nothing Then
            Me.Close()
        End If

        MyBase.Finalize()
    End Sub
End Class
