Set fso = CreateObject("Scripting.FileSystemObject")

Call CreateFolder("C:\Program Files\Logic-MD")
Call CreateFolder("C:\Program Files\Logic-MD\QSF")

fso.CopyFile "QueryStringFormatter.exe", "C:\Program Files\Logic-MD\QSF"
Call CreateShortcut



'--------------------------
Sub CreateFolder(Path)
	If not fso.folderexists(Path) Then
		fso.CreateFolder Path
	End If
End Sub

Sub CreateShortcut()
	' Create Shortcut
	Set Shell = CreateObject("WScript.Shell")
	ProgramsPath = Shell.SpecialFolders("AllUsersPrograms") 
	Call CreateFolder(ProgramsPath & "\Logic-MD")
	Call CreateFolder(ProgramsPath & "\Logic-MD\QSF")
	
	QSFPath = ProgramsPath & "\Logic-MD\QSF"
	
	Set link = Shell.CreateShortcut(QSFPath & "\QueryStringFormatter.lnk")
	link.Description = "SQL <-> VB Query String Formatter"
	link.IconLocation = "QueryStringFormatter.exe,1"
	link.TargetPath = "C:\Program Files\Logic-MD\QSF\QueryStringFormatter.exe"
	link.WindowStyle = 1
	link.WorkingDirectory = "C:\Program Files\Logic-MD\QSF"
	link.Save
End Sub