Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("VB <-> SQL QueryStringFormatter")> 
<Assembly: AssemblyDescription("Formatte les requ�tes SQL en format VB et vice versa")> 
<Assembly: AssemblyCompany("Les logiciels KMSoft Inc.")> 
<Assembly: AssemblyProduct("QueryStringFormatter")> 
<Assembly: AssemblyCopyright("2003 KMSoft Inc.")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("0CF53B57-A061-47CA-8BC1-2B1756B0D406")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.0.2.*")> 
