Public Class frmMain
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtSource As System.Windows.Forms.TextBox
    Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
    Friend WithEvents cmdTransform As System.Windows.Forms.Button
    Friend WithEvents chkAddCRLF As System.Windows.Forms.CheckBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents optToVBNET As System.Windows.Forms.RadioButton
    Friend WithEvents optToSQL As System.Windows.Forms.RadioButton
    Friend WithEvents optToVB As System.Windows.Forms.RadioButton
    Friend WithEvents txtTransformed As System.Windows.Forms.TextBox
    Friend WithEvents optCSharp As System.Windows.Forms.RadioButton
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.txtSource = New System.Windows.Forms.TextBox
        Me.Splitter1 = New System.Windows.Forms.Splitter
        Me.cmdTransform = New System.Windows.Forms.Button
        Me.chkAddCRLF = New System.Windows.Forms.CheckBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.optCSharp = New System.Windows.Forms.RadioButton
        Me.optToVBNET = New System.Windows.Forms.RadioButton
        Me.optToSQL = New System.Windows.Forms.RadioButton
        Me.optToVB = New System.Windows.Forms.RadioButton
        Me.txtTransformed = New System.Windows.Forms.TextBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtSource
        '
        Me.txtSource.AcceptsReturn = True
        Me.txtSource.AcceptsTab = True
        Me.txtSource.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtSource.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSource.Location = New System.Drawing.Point(0, 0)
        Me.txtSource.Multiline = True
        Me.txtSource.Name = "txtSource"
        Me.txtSource.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSource.Size = New System.Drawing.Size(488, 192)
        Me.txtSource.TabIndex = 0
        '
        'Splitter1
        '
        Me.Splitter1.Cursor = System.Windows.Forms.Cursors.HSplit
        Me.Splitter1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Splitter1.Location = New System.Drawing.Point(0, 192)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(488, 4)
        Me.Splitter1.TabIndex = 8
        Me.Splitter1.TabStop = False
        '
        'cmdTransform
        '
        Me.cmdTransform.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdTransform.Location = New System.Drawing.Point(392, 16)
        Me.cmdTransform.Name = "cmdTransform"
        Me.cmdTransform.Size = New System.Drawing.Size(92, 25)
        Me.cmdTransform.TabIndex = 11
        Me.cmdTransform.Text = "Transformer"
        '
        'chkAddCRLF
        '
        Me.chkAddCRLF.Location = New System.Drawing.Point(288, 16)
        Me.chkAddCRLF.Name = "chkAddCRLF"
        Me.chkAddCRLF.Size = New System.Drawing.Size(92, 24)
        Me.chkAddCRLF.TabIndex = 10
        Me.chkAddCRLF.Text = "Ajouter CRLF"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.optCSharp)
        Me.Panel1.Controls.Add(Me.optToVBNET)
        Me.Panel1.Controls.Add(Me.optToSQL)
        Me.Panel1.Controls.Add(Me.optToVB)
        Me.Panel1.Location = New System.Drawing.Point(8, 8)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(252, 40)
        Me.Panel1.TabIndex = 9
        '
        'optCSharp
        '
        Me.optCSharp.AutoSize = True
        Me.optCSharp.Location = New System.Drawing.Point(132, 23)
        Me.optCSharp.Name = "optCSharp"
        Me.optCSharp.Size = New System.Drawing.Size(39, 17)
        Me.optCSharp.TabIndex = 7
        Me.optCSharp.TabStop = True
        Me.optCSharp.Text = "C#"
        Me.optCSharp.UseVisualStyleBackColor = True
        '
        'optToVBNET
        '
        Me.optToVBNET.Location = New System.Drawing.Point(132, 4)
        Me.optToVBNET.Name = "optToVBNET"
        Me.optToVBNET.Size = New System.Drawing.Size(112, 16)
        Me.optToVBNET.TabIndex = 6
        Me.optToVBNET.Text = "Visual Basic.NET"
        '
        'optToSQL
        '
        Me.optToSQL.Location = New System.Drawing.Point(8, 24)
        Me.optToSQL.Name = "optToSQL"
        Me.optToSQL.Size = New System.Drawing.Size(84, 16)
        Me.optToSQL.TabIndex = 5
        Me.optToSQL.Text = "SQL"
        '
        'optToVB
        '
        Me.optToVB.Checked = True
        Me.optToVB.Location = New System.Drawing.Point(8, 4)
        Me.optToVB.Name = "optToVB"
        Me.optToVB.Size = New System.Drawing.Size(84, 16)
        Me.optToVB.TabIndex = 5
        Me.optToVB.TabStop = True
        Me.optToVB.Text = "Visual Basic"
        '
        'txtTransformed
        '
        Me.txtTransformed.AcceptsReturn = True
        Me.txtTransformed.AcceptsTab = True
        Me.txtTransformed.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTransformed.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTransformed.Location = New System.Drawing.Point(0, 56)
        Me.txtTransformed.Multiline = True
        Me.txtTransformed.Name = "txtTransformed"
        Me.txtTransformed.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtTransformed.Size = New System.Drawing.Size(488, 188)
        Me.txtTransformed.TabIndex = 3
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.cmdTransform)
        Me.Panel2.Controls.Add(Me.chkAddCRLF)
        Me.Panel2.Controls.Add(Me.txtTransformed)
        Me.Panel2.Controls.Add(Me.Panel1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 196)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(488, 246)
        Me.Panel2.TabIndex = 9
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(488, 442)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Splitter1)
        Me.Controls.Add(Me.txtSource)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(496, 476)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Logic-MD - Query String Formatter v1.2"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub cmdTransform_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTransform.Click
        Dim str As String
        Dim int As Integer
        Dim intLenToRemove As Integer

        txtTransformed.Text = ""

        If optToVB.Checked Then
            For int = 0 To txtSource.Lines.Length - 1
                If int = txtSource.Lines.Length - 1 Then
                    str = txtSource.Lines.GetValue(int)

                    If str <> "" Then
                        str = Replace(str, """", """""")

                        txtTransformed.Text += """" & str & """"
                    Else
                        intLenToRemove = IIf(chkAddCRLF.CheckState = CheckState.Checked, 16, 7)
                        txtTransformed.Text = Mid(txtTransformed.Text, 1, Len(txtTransformed.Text) - intLenToRemove)
                        txtTransformed.Text += """"
                    End If
                Else
                    str = txtSource.Lines.GetValue(int)
                    str = Replace(str, """", """""")
                    txtTransformed.Text += """" & str & " "" " & IIf(chkAddCRLF.CheckState = CheckState.Checked, "& vbCrLf ", "") & "& _" & ControlChars.CrLf
                End If

            Next
        ElseIf optToVBNET.Checked Then
            For int = 0 To txtSource.Lines.Length - 1
                If int = txtSource.Lines.Length - 1 Then
                    str = txtSource.Lines.GetValue(int)

                    If str <> "" Then
                        str = Replace(str, """", """""")
                        txtTransformed.Text += """" & str & """"
                    Else
                        intLenToRemove = IIf(chkAddCRLF.CheckState = CheckState.Checked, 27, 7)
                        txtTransformed.Text = Mid(txtTransformed.Text, 1, Len(txtTransformed.Text) - intLenToRemove)
                        txtTransformed.Text += """"
                    End If
                Else
                    str = txtSource.Lines.GetValue(int)
                    str = Replace(str, """", """""")
                    txtTransformed.Text += """" & str & " "" " & IIf(chkAddCRLF.CheckState = CheckState.Checked, "& ControlChars.CrLf ", "") & "& _" & ControlChars.CrLf
                End If
            Next
        ElseIf optToSQL.Checked Then
            For Each str In txtSource.Lines
                txtTransformed.Text += Replace(str, " & ControlChars.CrLf", "")
                txtTransformed.Text = Replace(txtTransformed.Text, " & vbCrLf", "")
                txtTransformed.Text = Replace(txtTransformed.Text, " _", "")
                txtTransformed.Text = Replace(txtTransformed.Text, " &", "")
                txtTransformed.Text = Replace(txtTransformed.Text, " """, "")
                txtTransformed.Text = Replace(txtTransformed.Text, " \r\n +", "")
                txtTransformed.Text = Replace(txtTransformed.Text, " +", "")
                txtTransformed.Text = Replace(txtTransformed.Text, """", "") & ControlChars.CrLf
            Next
            txtTransformed.Text = Mid(txtTransformed.Text, 1, Len(txtTransformed.Text) - 2)
        ElseIf optCSharp.Checked Then
            For int = 0 To txtSource.Lines.Length - 1
                If int = txtSource.Lines.Length - 1 Then
                    str = txtSource.Lines.GetValue(int)

                    If str <> "" Then
                        str = Replace(str, """", """""")
                        txtTransformed.Text += """" & str & """"
                    Else
                        intLenToRemove = IIf(chkAddCRLF.CheckState = CheckState.Checked, 11, 6)
                        txtTransformed.Text = Mid(txtTransformed.Text, 1, Len(txtTransformed.Text) - intLenToRemove)
                        txtTransformed.Text += """"
                    End If
                Else
                    str = txtSource.Lines.GetValue(int)
                    str = Replace(str, """", """""")
                    txtTransformed.Text += """" & str & IIf(chkAddCRLF.CheckState = CheckState.Checked, " \r\n", "") & " "" +" & ControlChars.CrLf
                End If
            Next

        End If

        Trim(txtTransformed.Text)
        Clipboard.SetDataObject(txtTransformed.Text)
    End Sub

    Private Sub optToVB_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optToVB.CheckedChanged
        chkAddCRLF.Enabled = True
    End Sub

    Private Sub optToSQL_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optToSQL.CheckedChanged
        chkAddCRLF.Enabled = False
    End Sub

    Private Sub optToVBNET_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optToVBNET.CheckedChanged
        chkAddCRLF.Enabled = True
    End Sub

    Private Sub optCSharp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optCSharp.CheckedChanged
        chkAddCRLF.Enabled = True
    End Sub
End Class
