Public Class cSchedule
    Private mEnabled As Boolean
    Private mTime As DateTime
    Private mSearchPath As String
    Private mSound1 As String
    Private mSound2 As String

    Private mPlayed As Boolean

    Public Property Played() As Boolean
        Get
            Return mPlayed
        End Get
        Set(ByVal Value As Boolean)
            mPlayed = Value
        End Set
    End Property

    Public Property Enabled() As Boolean
        Get
            Return mEnabled
        End Get
        Set(ByVal Value As Boolean)
            mEnabled = Value
        End Set
    End Property

    Public Property Time() As DateTime
        Get
            Return mTime
            'test
        End Get
        Set(ByVal Value As DateTime)
            mTime = Value
        End Set
    End Property

    Public Property Sound1() As String
        Get
            Return mSound1
        End Get
        Set(ByVal Value As String)
            mSound1 = Value
        End Set
    End Property

    Public Property Sound2() As String
        Get
            Return mSound2
        End Get
        Set(ByVal Value As String)
            mSound2 = Value
        End Set
    End Property

    Public Property SearchPath() As String
        Get
            Return mSearchPath
        End Get
        Set(ByVal value As String)
            mSearchPath = value
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return Time.ToShortDateString
    End Function

End Class
