Imports System.Data

Public Class cSettings
    Private mDBSettings As DataSet
    Private mSchedules As Collection

    Public Sub New(ByVal SettingsPath As String)
        Try
            mDBSettings = New DataSet("KlingelnSettings")
            mDBSettings.ReadXml(SettingsPath)

            LoadSchedules()
        Catch E As Exception
            Throw E
        End Try
    End Sub

#Region "Do not ring days"
    Public Property Sunday() As Boolean
        Get
            Return mDBSettings.Tables("tblSetting").Rows(0).Item("Sunday")
        End Get
        Set(ByVal Value As Boolean)
            mDBSettings.Tables("tblSetting").Rows(0).Item("Sunday") = Value
            mDBSettings.Tables("tblSetting").AcceptChanges()
        End Set
    End Property

    Public Property Monday() As Boolean
        Get
            Return mDBSettings.Tables("tblSetting").Rows(0).Item("Monday")
        End Get
        Set(ByVal Value As Boolean)
            mDBSettings.Tables("tblSetting").Rows(0).Item("Monday") = Value
            mDBSettings.Tables("tblSetting").AcceptChanges()
        End Set
    End Property

    Public Property Tuesday() As Boolean
        Get
            Return mDBSettings.Tables("tblSetting").Rows(0).Item("Tuesday")
        End Get
        Set(ByVal Value As Boolean)
            mDBSettings.Tables("tblSetting").Rows(0).Item("Tuesday") = Value
            mDBSettings.Tables("tblSetting").AcceptChanges()
        End Set
    End Property

    Public Property Wednesday() As Boolean
        Get
            Return mDBSettings.Tables("tblSetting").Rows(0).Item("Wednesday")
        End Get
        Set(ByVal Value As Boolean)
            mDBSettings.Tables("tblSetting").Rows(0).Item("Wednesday") = Value
            mDBSettings.Tables("tblSetting").AcceptChanges()
        End Set
    End Property

    Public Property Thursday() As Boolean
        Get
            Return mDBSettings.Tables("tblSetting").Rows(0).Item("Thursday")
        End Get
        Set(ByVal Value As Boolean)
            mDBSettings.Tables("tblSetting").Rows(0).Item("Thursday") = Value
            mDBSettings.Tables("tblSetting").AcceptChanges()
        End Set
    End Property

    Public Property Friday() As Boolean
        Get
            Return mDBSettings.Tables("tblSetting").Rows(0).Item("Friday")
        End Get
        Set(ByVal Value As Boolean)
            mDBSettings.Tables("tblSetting").Rows(0).Item("Friday") = Value
            mDBSettings.Tables("tblSetting").AcceptChanges()
        End Set
    End Property

    Public Property Saturday() As Boolean
        Get
            Return mDBSettings.Tables("tblSetting").Rows(0).Item("Saturday")
        End Get
        Set(ByVal Value As Boolean)
            mDBSettings.Tables("tblSetting").Rows(0).Item("Saturday") = Value
            mDBSettings.Tables("tblSetting").AcceptChanges()
        End Set
    End Property
#End Region

#Region "Off days - jours f�ri�s"
    Public Sub ClearOffDays()
        mDBSettings.Tables("tblOffDay").Rows.Clear()
    End Sub

    Public Sub AddOffDay(ByVal Value As DateTime)
        Dim lrow As DataRow
        lrow = mDBSettings.Tables("tblOffDay").NewRow
        lrow!OffDay = Value.ToShortDateString
        mDBSettings.Tables("tblOffDay").Rows.Add(lrow)
        mDBSettings.Tables("tblOffDay").AcceptChanges()
    End Sub

    Public Function GetOffDay(ByVal RowValue As Integer) As String
        Return mDBSettings.Tables("tblOffDay").Rows(RowValue).Item(0)
    End Function

    Public Function CountOffDays() As Integer
        Return mDBSettings.Tables("tblOffDay").Rows.Count
    End Function
#End Region

#Region "Schedule"
    Public Property Schedule() As Collection
        Get
            Return mSchedules
        End Get
        Set(ByVal Value As Collection)
            mSchedules = Value
        End Set
    End Property

    Private Sub LoadSchedules()
        Dim row As DataRow
        Dim oSched As cSchedule
        mSchedules = New Collection

        For Each row In mDBSettings.Tables("tblScheduleItem").Rows
            oSched = New cSchedule
            oSched.Enabled = row!Enabled
            oSched.Time = row!SchedTime
            oSched.Sound1 = row!Sound1
            oSched.Sound2 = row!Sound2
            Try
                oSched.SearchPath = row!SearchPath
            Catch ex As Exception
                oSched.SearchPath = ""
            End Try


            mSchedules.Add(oSched)
        Next
    End Sub

    Private Sub SaveSchedules()
        Dim oRow As DataRow
        mDBSettings.Tables("tblScheduleItem").Clear()

        For Each oSched As cSchedule In mSchedules
            If mDBSettings.Tables("tblScheduleItem").Columns(mDBSettings.Tables("tblScheduleItem").Columns.Count - 1).ToString <> "SearchPath" Then
                mDBSettings.Tables("tblScheduleItem").Columns.Add("SearchPath", System.Type.GetType("System.String"))
            End If

            oRow = mDBSettings.Tables("tblScheduleItem").NewRow
            oRow!Enabled = oSched.Enabled
            oRow!SchedTime = oSched.Time.ToLongTimeString
            oRow!Sound1 = oSched.Sound1
            oRow!Sound2 = oSched.Sound2

            oRow!SearchPath = oSched.SearchPath

            mDBSettings.Tables("tblScheduleItem").Rows.Add(oRow)
            oRow = Nothing
        Next
    End Sub

    Public Function GetScheduleIndex(ByVal ItemTime As String) As Integer
        Dim idx As Integer = -1

        For idx = 1 To mSchedules.Count
            If ItemTime = CType(mSchedules.Item(idx), cSchedule).Time Then
                Return idx
            End If
        Next
    End Function
#End Region

    Public Property StopPlayAfterXSeconds() As Long
        Get
            Return mDBSettings.Tables("tblSetting").Rows(0)!StopPlayAfterXSeconds
        End Get
        Set(ByVal Value As Long)
            mDBSettings.Tables("tblSetting").Rows(0).Item("StopPlayAfterXSeconds") = Value
            mDBSettings.Tables("tblSetting").AcceptChanges()
        End Set
    End Property

    Public Property WAVsFolder() As String
        Get
            Return mDBSettings.Tables("tblSetting").Rows(0)!WAVsPath
        End Get
        Set(ByVal Value As String)
            mDBSettings.Tables("tblSetting").Rows(0).Item("WAVsPath") = Value
            mDBSettings.Tables("tblSetting").AcceptChanges()
        End Set
    End Property

    Public Sub SaveToFile(ByVal Filename As String)
        SaveSchedules()
        mDBSettings.WriteXml(Filename, XmlWriteMode.WriteSchema)
    End Sub

    Public Sub Dispose()
        mDBSettings = Nothing
    End Sub
End Class
