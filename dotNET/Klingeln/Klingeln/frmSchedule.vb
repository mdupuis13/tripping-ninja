Public Class frmSchedule
    Inherits System.Windows.Forms.Form

    Private mSettings As cSettings

#Region " Windows Form Designer generated code "

    'Public Sub New()
    '    MyBase.New()

    '    'This call is required by the Windows Form Designer.
    '    InitializeComponent()

    '    'Add any initialization after the InitializeComponent() call

    'End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkEnable As System.Windows.Forms.CheckBox
    Friend WithEvents cboSoundFile1 As System.Windows.Forms.ComboBox
    Friend WithEvents cboSoundFile2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmdBrowseFolder As System.Windows.Forms.Button
    Friend WithEvents lblWAVsFolder As System.Windows.Forms.Label
    Friend WithEvents oFolderBrowser As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSchedule))
        Me.txtTime = New System.Windows.Forms.DateTimePicker
        Me.Label1 = New System.Windows.Forms.Label
        Me.chkEnable = New System.Windows.Forms.CheckBox
        Me.cboSoundFile1 = New System.Windows.Forms.ComboBox
        Me.cboSoundFile2 = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.cmdBrowseFolder = New System.Windows.Forms.Button
        Me.lblWAVsFolder = New System.Windows.Forms.Label
        Me.oFolderBrowser = New System.Windows.Forms.FolderBrowserDialog
        Me.SuspendLayout()
        '
        'txtTime
        '
        Me.txtTime.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.txtTime.Location = New System.Drawing.Point(160, 10)
        Me.txtTime.Name = "txtTime"
        Me.txtTime.ShowUpDown = True
        Me.txtTime.Size = New System.Drawing.Size(80, 20)
        Me.txtTime.TabIndex = 2
        Me.txtTime.Value = New Date(2005, 10, 4, 12, 23, 0, 0)
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(95, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 20)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Heure:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkEnable
        '
        Me.chkEnable.Checked = True
        Me.chkEnable.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkEnable.Location = New System.Drawing.Point(10, 10)
        Me.chkEnable.Name = "chkEnable"
        Me.chkEnable.Size = New System.Drawing.Size(50, 20)
        Me.chkEnable.TabIndex = 0
        Me.chkEnable.Text = "Actif"
        '
        'cboSoundFile1
        '
        Me.cboSoundFile1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSoundFile1.Location = New System.Drawing.Point(90, 88)
        Me.cboSoundFile1.Name = "cboSoundFile1"
        Me.cboSoundFile1.Size = New System.Drawing.Size(165, 21)
        Me.cboSoundFile1.TabIndex = 4
        '
        'cboSoundFile2
        '
        Me.cboSoundFile2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSoundFile2.Location = New System.Drawing.Point(90, 113)
        Me.cboSoundFile2.Name = "cboSoundFile2"
        Me.cboSoundFile2.Size = New System.Drawing.Size(165, 21)
        Me.cboSoundFile2.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(40, 88)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 20)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Son 1:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(40, 113)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 20)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Son 2:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdOK
        '
        Me.cmdOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.cmdOK.Location = New System.Drawing.Point(55, 143)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 7
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Location = New System.Drawing.Point(145, 143)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 8
        Me.cmdCancel.Text = "&Cancel"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(14, 41)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(175, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Path to music files for this schedule:"
        '
        'cmdBrowseFolder
        '
        Me.cmdBrowseFolder.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdBrowseFolder.Location = New System.Drawing.Point(373, 54)
        Me.cmdBrowseFolder.Name = "cmdBrowseFolder"
        Me.cmdBrowseFolder.Size = New System.Drawing.Size(24, 20)
        Me.cmdBrowseFolder.TabIndex = 12
        Me.cmdBrowseFolder.Text = "..."
        '
        'lblWAVsFolder
        '
        Me.lblWAVsFolder.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWAVsFolder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblWAVsFolder.Location = New System.Drawing.Point(17, 54)
        Me.lblWAVsFolder.Name = "lblWAVsFolder"
        Me.lblWAVsFolder.Size = New System.Drawing.Size(357, 20)
        Me.lblWAVsFolder.TabIndex = 13
        '
        'oFolderBrowser
        '
        Me.oFolderBrowser.Description = "Dossier contenant les fichiers musicaux"
        Me.oFolderBrowser.SelectedPath = "D:\users\mdupuis\My Music"
        Me.oFolderBrowser.ShowNewFolderButton = False
        '
        'frmSchedule
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(409, 178)
        Me.Controls.Add(Me.lblWAVsFolder)
        Me.Controls.Add(Me.cmdBrowseFolder)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cboSoundFile2)
        Me.Controls.Add(Me.cboSoundFile1)
        Me.Controls.Add(Me.chkEnable)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtTime)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmSchedule"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Horaire"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region


    Public Property Time() As String
        Get
            Return txtTime.Value.ToLongTimeString
        End Get
        Set(ByVal Value As String)
            GetSchedules(Value)
        End Set
    End Property

    Public ReadOnly Property Sound1() As cFileItem
        Get
            Return CType(cboSoundFile1.SelectedItem, cFileItem)
        End Get
    End Property

    Public ReadOnly Property Sound2() As cFileItem
        Get
            Return CType(cboSoundFile2.SelectedItem, cFileItem)
        End Get
    End Property

    Public ReadOnly Property Active() As Boolean
        Get
            Return chkEnable.Checked
        End Get
    End Property

    Public ReadOnly Property SearchPath() As String
        Get
            Return lblWAVsFolder.Text
        End Get
    End Property

    Public Sub New(ByVal Settings As cSettings)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        mSettings = Settings
        Dim oFilItm As cFileItem

        oFilItm = New cFileItem
        oFilItm.FileName = ""
        cboSoundFile1.Items.Add(oFilItm)
        cboSoundFile2.Items.Add(oFilItm)
        oFilItm = Nothing

        oFilItm = New cFileItem
        oFilItm.FileName = "Random"
        cboSoundFile1.Items.Add(oFilItm)
        cboSoundFile2.Items.Add(oFilItm)
        oFilItm = Nothing

        LoadFilesFromDirectory(cboSoundFile1, mSettings.WAVsFolder)
        LoadFilesFromDirectory(cboSoundFile2, mSettings.WAVsFolder)
    End Sub

    Private Sub GetSchedules(ByVal Time As String)
        Dim oSched As cSchedule

        Dim idx As Integer = mSettings.GetScheduleIndex(Time)

        If idx > 0 Then
            oSched = mSettings.Schedule.Item(idx)
            chkEnable.Checked = oSched.Enabled
            txtTime.Value = DateTime.Parse(DateValue(DateTime.Now) & " " & oSched.Time)
            lblWAVsFolder.Text = oSched.SearchPath

            If oSched.SearchPath <> "" Then
                LoadFilesFromDirectory(cboSoundFile1, oSched.SearchPath)
                LoadFilesFromDirectory(cboSoundFile2, oSched.SearchPath)
            End If
        End If

        For i As Integer = 0 To cboSoundFile1.Items.Count - 1
            If cboSoundFile1.Items(i).ToString = System.IO.Path.GetFileNameWithoutExtension(oSched.Sound1) Then
                cboSoundFile1.SelectedIndex = i
            End If
        Next

        For i As Integer = 0 To cboSoundFile2.Items.Count - 1
            If cboSoundFile2.Items(i).ToString = System.IO.Path.GetFileNameWithoutExtension(oSched.Sound2) Then
                cboSoundFile2.SelectedIndex = i
            End If
        Next

        oSched = Nothing
    End Sub

    Private Sub LoadFilesFromDirectory(ByVal cbo As ComboBox, ByVal FolderPath As String)
        cbo.Items.Clear()
        Dim sFiles As String() = System.IO.Directory.GetFiles(FolderPath, "*.wav", IO.SearchOption.AllDirectories)
        Dim oFilItm As cFileItem

        oFilItm = New cFileItem
        oFilItm.FileName = ""
        cbo.Items.Add(oFilItm)

        oFilItm = New cFileItem
        oFilItm.FileName = "Random"
        cbo.Items.Add(oFilItm)
        oFilItm = Nothing

        For Each fil As String In sFiles
            oFilItm = New cFileItem
            oFilItm.FileName = fil
            cbo.Items.Add(oFilItm)
            oFilItm = Nothing
        Next

        sFiles = System.IO.Directory.GetFiles(FolderPath, "*.mp3", IO.SearchOption.AllDirectories)
        For Each fil As String In sFiles
            oFilItm = New cFileItem
            oFilItm.FileName = fil
            cbo.Items.Add(oFilItm)
            oFilItm = Nothing
        Next
    End Sub

    Private Sub cmdBrowseFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowseFolder.Click
        If lblWAVsFolder.Text <> "" Then
            oFolderBrowser.SelectedPath = lblWAVsFolder.Text
        End If

        If oFolderBrowser.ShowDialog() = Windows.Forms.DialogResult.OK Then
            If oFolderBrowser.SelectedPath <> "" Then
                lblWAVsFolder.Text = oFolderBrowser.SelectedPath
                mSettings.WAVsFolder = lblWAVsFolder.Text
            End If
        End If

    End Sub
End Class
