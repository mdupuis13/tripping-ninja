Imports System.Configuration

Public Class frmMain
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents oFolderBrowser As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents cmdBrowseWAVFolder As System.Windows.Forms.Button
    Friend WithEvents lblWAVsFolder As System.Windows.Forms.Label
    Friend WithEvents chkNRSaturday As System.Windows.Forms.CheckBox
    Friend WithEvents chkNRSunday As System.Windows.Forms.CheckBox
    Friend WithEvents grpDoNotRing As System.Windows.Forms.GroupBox
    Friend WithEvents chkNRMonday As System.Windows.Forms.CheckBox
    Friend WithEvents chkNRWednesday As System.Windows.Forms.CheckBox
    Friend WithEvents chkNRThursday As System.Windows.Forms.CheckBox
    Friend WithEvents chkNRFriday As System.Windows.Forms.CheckBox
    Friend WithEvents lstOffDays As System.Windows.Forms.ListBox
    Friend WithEvents cmdAddOffDay As System.Windows.Forms.Button
    Friend WithEvents cmdDeleteOffDay As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnulstSchedule As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuSchedAdd As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSchedDelete As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSchedModify As System.Windows.Forms.MenuItem
    Friend WithEvents chkNRTuesday As System.Windows.Forms.CheckBox
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents lstSchedule As System.Windows.Forms.ListView
    Friend WithEvents updnNbSecondsToPlay As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ttipNbSecondsToPlay As System.Windows.Forms.ToolTip
    Friend WithEvents oWMP As AxWMPLib.AxWindowsMediaPlayer
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdRestartService As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.cmdBrowseWAVFolder = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.oFolderBrowser = New System.Windows.Forms.FolderBrowserDialog
        Me.lblWAVsFolder = New System.Windows.Forms.Label
        Me.chkNRSaturday = New System.Windows.Forms.CheckBox
        Me.chkNRSunday = New System.Windows.Forms.CheckBox
        Me.grpDoNotRing = New System.Windows.Forms.GroupBox
        Me.cmdDeleteOffDay = New System.Windows.Forms.Button
        Me.cmdAddOffDay = New System.Windows.Forms.Button
        Me.chkNRFriday = New System.Windows.Forms.CheckBox
        Me.chkNRThursday = New System.Windows.Forms.CheckBox
        Me.chkNRWednesday = New System.Windows.Forms.CheckBox
        Me.chkNRTuesday = New System.Windows.Forms.CheckBox
        Me.chkNRMonday = New System.Windows.Forms.CheckBox
        Me.lstOffDays = New System.Windows.Forms.ListBox
        Me.lstSchedule = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.mnulstSchedule = New System.Windows.Forms.ContextMenu
        Me.mnuSchedAdd = New System.Windows.Forms.MenuItem
        Me.mnuSchedModify = New System.Windows.Forms.MenuItem
        Me.mnuSchedDelete = New System.Windows.Forms.MenuItem
        Me.cmdSave = New System.Windows.Forms.Button
        Me.cmdRestartService = New System.Windows.Forms.Button
        Me.updnNbSecondsToPlay = New System.Windows.Forms.NumericUpDown
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.ttipNbSecondsToPlay = New System.Windows.Forms.ToolTip(Me.components)
        Me.oWMP = New AxWMPLib.AxWindowsMediaPlayer
        Me.ColumnHeader4 = New System.Windows.Forms.ColumnHeader
        Me.grpDoNotRing.SuspendLayout()
        CType(Me.updnNbSecondsToPlay, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.oWMP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdBrowseWAVFolder
        '
        Me.cmdBrowseWAVFolder.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdBrowseWAVFolder.Location = New System.Drawing.Point(600, 10)
        Me.cmdBrowseWAVFolder.Name = "cmdBrowseWAVFolder"
        Me.cmdBrowseWAVFolder.Size = New System.Drawing.Size(24, 20)
        Me.cmdBrowseWAVFolder.TabIndex = 2
        Me.cmdBrowseWAVFolder.Text = "..."
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(27, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(165, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Emplacement des fichiers WAVs:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'oFolderBrowser
        '
        Me.oFolderBrowser.Description = "Dossier contenant les fichiers musicaux"
        Me.oFolderBrowser.SelectedPath = "D:\users\mdupuis\My Music"
        Me.oFolderBrowser.ShowNewFolderButton = False
        '
        'lblWAVsFolder
        '
        Me.lblWAVsFolder.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWAVsFolder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblWAVsFolder.Location = New System.Drawing.Point(200, 10)
        Me.lblWAVsFolder.Name = "lblWAVsFolder"
        Me.lblWAVsFolder.Size = New System.Drawing.Size(400, 20)
        Me.lblWAVsFolder.TabIndex = 1
        '
        'chkNRSaturday
        '
        Me.chkNRSaturday.Location = New System.Drawing.Point(16, 168)
        Me.chkNRSaturday.Name = "chkNRSaturday"
        Me.chkNRSaturday.Size = New System.Drawing.Size(80, 24)
        Me.chkNRSaturday.TabIndex = 6
        Me.chkNRSaturday.Text = "&Samedi"
        '
        'chkNRSunday
        '
        Me.chkNRSunday.Location = New System.Drawing.Point(16, 24)
        Me.chkNRSunday.Name = "chkNRSunday"
        Me.chkNRSunday.Size = New System.Drawing.Size(80, 24)
        Me.chkNRSunday.TabIndex = 0
        Me.chkNRSunday.Text = "&Dimanche"
        '
        'grpDoNotRing
        '
        Me.grpDoNotRing.Controls.Add(Me.cmdDeleteOffDay)
        Me.grpDoNotRing.Controls.Add(Me.cmdAddOffDay)
        Me.grpDoNotRing.Controls.Add(Me.chkNRFriday)
        Me.grpDoNotRing.Controls.Add(Me.chkNRThursday)
        Me.grpDoNotRing.Controls.Add(Me.chkNRWednesday)
        Me.grpDoNotRing.Controls.Add(Me.chkNRTuesday)
        Me.grpDoNotRing.Controls.Add(Me.chkNRMonday)
        Me.grpDoNotRing.Controls.Add(Me.chkNRSaturday)
        Me.grpDoNotRing.Controls.Add(Me.chkNRSunday)
        Me.grpDoNotRing.Controls.Add(Me.lstOffDays)
        Me.grpDoNotRing.Location = New System.Drawing.Point(8, 61)
        Me.grpDoNotRing.Name = "grpDoNotRing"
        Me.grpDoNotRing.Size = New System.Drawing.Size(248, 200)
        Me.grpDoNotRing.TabIndex = 3
        Me.grpDoNotRing.TabStop = False
        Me.grpDoNotRing.Text = "Ne pas sonner..."
        '
        'cmdDeleteOffDay
        '
        Me.cmdDeleteOffDay.Location = New System.Drawing.Point(160, 24)
        Me.cmdDeleteOffDay.Name = "cmdDeleteOffDay"
        Me.cmdDeleteOffDay.Size = New System.Drawing.Size(56, 24)
        Me.cmdDeleteOffDay.TabIndex = 8
        Me.cmdDeleteOffDay.Text = "E&ffacer"
        '
        'cmdAddOffDay
        '
        Me.cmdAddOffDay.Location = New System.Drawing.Point(104, 24)
        Me.cmdAddOffDay.Name = "cmdAddOffDay"
        Me.cmdAddOffDay.Size = New System.Drawing.Size(56, 24)
        Me.cmdAddOffDay.TabIndex = 7
        Me.cmdAddOffDay.Text = "&Ajouter"
        '
        'chkNRFriday
        '
        Me.chkNRFriday.Location = New System.Drawing.Point(16, 144)
        Me.chkNRFriday.Name = "chkNRFriday"
        Me.chkNRFriday.Size = New System.Drawing.Size(80, 24)
        Me.chkNRFriday.TabIndex = 5
        Me.chkNRFriday.Text = "&Vendredi"
        '
        'chkNRThursday
        '
        Me.chkNRThursday.Location = New System.Drawing.Point(16, 120)
        Me.chkNRThursday.Name = "chkNRThursday"
        Me.chkNRThursday.Size = New System.Drawing.Size(80, 24)
        Me.chkNRThursday.TabIndex = 4
        Me.chkNRThursday.Text = "&Jeudi"
        '
        'chkNRWednesday
        '
        Me.chkNRWednesday.Location = New System.Drawing.Point(16, 96)
        Me.chkNRWednesday.Name = "chkNRWednesday"
        Me.chkNRWednesday.Size = New System.Drawing.Size(80, 24)
        Me.chkNRWednesday.TabIndex = 3
        Me.chkNRWednesday.Text = "M&ercredi"
        '
        'chkNRTuesday
        '
        Me.chkNRTuesday.Location = New System.Drawing.Point(16, 72)
        Me.chkNRTuesday.Name = "chkNRTuesday"
        Me.chkNRTuesday.Size = New System.Drawing.Size(80, 24)
        Me.chkNRTuesday.TabIndex = 2
        Me.chkNRTuesday.Text = "&Mardi"
        '
        'chkNRMonday
        '
        Me.chkNRMonday.Location = New System.Drawing.Point(16, 48)
        Me.chkNRMonday.Name = "chkNRMonday"
        Me.chkNRMonday.Size = New System.Drawing.Size(80, 24)
        Me.chkNRMonday.TabIndex = 1
        Me.chkNRMonday.Text = "&Lundi"
        '
        'lstOffDays
        '
        Me.lstOffDays.Location = New System.Drawing.Point(104, 48)
        Me.lstOffDays.Name = "lstOffDays"
        Me.lstOffDays.Size = New System.Drawing.Size(112, 134)
        Me.lstOffDays.Sorted = True
        Me.lstOffDays.TabIndex = 9
        '
        'lstSchedule
        '
        Me.lstSchedule.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstSchedule.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lstSchedule.ContextMenu = Me.mnulstSchedule
        Me.lstSchedule.FullRowSelect = True
        Me.lstSchedule.Location = New System.Drawing.Point(264, 61)
        Me.lstSchedule.MultiSelect = False
        Me.lstSchedule.Name = "lstSchedule"
        Me.lstSchedule.Size = New System.Drawing.Size(360, 305)
        Me.lstSchedule.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lstSchedule.TabIndex = 4
        Me.lstSchedule.UseCompatibleStateImageBehavior = False
        Me.lstSchedule.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Time"
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Cloche 1"
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Cloche 2"
        '
        'mnulstSchedule
        '
        Me.mnulstSchedule.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuSchedAdd, Me.mnuSchedModify, Me.mnuSchedDelete})
        '
        'mnuSchedAdd
        '
        Me.mnuSchedAdd.Index = 0
        Me.mnuSchedAdd.Text = "Ajouter"
        '
        'mnuSchedModify
        '
        Me.mnuSchedModify.Index = 1
        Me.mnuSchedModify.Text = "Modifier"
        '
        'mnuSchedDelete
        '
        Me.mnuSchedDelete.Index = 2
        Me.mnuSchedDelete.Text = "Effacer"
        '
        'cmdSave
        '
        Me.cmdSave.Location = New System.Drawing.Point(8, 266)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(90, 35)
        Me.cmdSave.TabIndex = 5
        Me.cmdSave.Text = "Sauvegarder"
        '
        'cmdRestartService
        '
        Me.cmdRestartService.Location = New System.Drawing.Point(165, 266)
        Me.cmdRestartService.Name = "cmdRestartService"
        Me.cmdRestartService.Size = New System.Drawing.Size(90, 35)
        Me.cmdRestartService.TabIndex = 6
        Me.cmdRestartService.Text = "Arr�ter le service"
        '
        'updnNbSecondsToPlay
        '
        Me.updnNbSecondsToPlay.Location = New System.Drawing.Point(151, 38)
        Me.updnNbSecondsToPlay.Maximum = New Decimal(New Integer() {600, 0, 0, 0})
        Me.updnNbSecondsToPlay.Minimum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.updnNbSecondsToPlay.Name = "updnNbSecondsToPlay"
        Me.updnNbSecondsToPlay.Size = New System.Drawing.Size(41, 20)
        Me.updnNbSecondsToPlay.TabIndex = 8
        Me.ttipNbSecondsToPlay.SetToolTip(Me.updnNbSecondsToPlay, "0 = Ne pas arr�ter")
        Me.updnNbSecondsToPlay.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(27, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(122, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Arr�ter la chanson apr�s"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(197, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "secondes."
        '
        'oWMP
        '
        Me.oWMP.Enabled = True
        Me.oWMP.Location = New System.Drawing.Point(8, 320)
        Me.oWMP.Name = "oWMP"
        Me.oWMP.OcxState = CType(resources.GetObject("oWMP.OcxState"), System.Windows.Forms.AxHost.State)
        Me.oWMP.Size = New System.Drawing.Size(247, 46)
        Me.oWMP.TabIndex = 11
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Chemin vers la musique"
        Me.ColumnHeader4.Width = 160
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(632, 375)
        Me.Controls.Add(Me.oWMP)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.updnNbSecondsToPlay)
        Me.Controls.Add(Me.cmdRestartService)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.lstSchedule)
        Me.Controls.Add(Me.lblWAVsFolder)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmdBrowseWAVFolder)
        Me.Controls.Add(Me.grpDoNotRing)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(640, 380)
        Me.Name = "frmMain"
        Me.Text = "Klingeln - Horaire des cloches"
        Me.grpDoNotRing.ResumeLayout(False)
        CType(Me.updnNbSecondsToPlay, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.oWMP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

#Region "Private variables"
    Private mSettings As cSettings
    Private WithEvents mCalendar As frmCalendar
    Private WithEvents mTimer As Timers.Timer
    Private WithEvents mPlayStopTimer As Timers.Timer
    Private mPlayFileIndex As Long = 0
#End Region

    Private Sub cmdBrowseWAVFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowseWAVFolder.Click
        If lblWAVsFolder.Text <> "" Then
            oFolderBrowser.SelectedPath = lblWAVsFolder.Text
        End If

        If oFolderBrowser.ShowDialog() = Windows.Forms.DialogResult.OK Then
            If oFolderBrowser.SelectedPath <> "" Then
                lblWAVsFolder.Text = oFolderBrowser.SelectedPath

                SaveSettings()
                LoadSettings()
            End If
        End If
    End Sub

    Private Sub cmdAddOffDay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddOffDay.Click
        mCalendar = New frmCalendar
        mCalendar.Show()
    End Sub

    Private Sub cmdDeleteOffDay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDeleteOffDay.Click
        If lstOffDays.SelectedIndex >= 0 Then
            lstOffDays.Items.RemoveAt(lstOffDays.SelectedIndex)

            If lstOffDays.Items.Count > 0 Then
                lstOffDays.SelectedIndex = 0
            End If
        End If
    End Sub

    Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        oWMP.Ctlcontrols.stop()
        'oWMP.Dispose()
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mTimer = New Timers.Timer
        mPlayStopTimer = New Timers.Timer
        LoadSettings()
        mTimer.Interval = 500
        SetServiceTimer()
    End Sub

    Private Sub mnuSchedAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSchedAdd.Click
        Dim ofrmSched As frmSchedule = New frmSchedule(mSettings)

        If ofrmSched.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Dim oSched As cSchedule = New cSchedule

            oSched.Enabled = ofrmSched.Active
            oSched.Time = ofrmSched.Time

            If CType(ofrmSched.Sound1, cFileItem).FileName = "Random" Then
                oSched.Sound1 = "Random"
            Else
                oSched.Sound1 = CType(ofrmSched.Sound1, cFileItem).FilePath
            End If

            If CType(ofrmSched.Sound1, cFileItem).FileName = "Random" Then
                oSched.Sound2 = "Random"
            Else
                oSched.Sound2 = CType(ofrmSched.Sound1, cFileItem).FilePath
            End If

            mSettings.Schedule.Add(oSched)
            ShowScheduleInListView(oSched)
            oSched = Nothing
        End If

        ofrmSched = Nothing
    End Sub

    Private Sub mnuSchedModify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSchedModify.Click
        ModifySchedule()
    End Sub

    Private Sub mnuSchedDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSchedDelete.Click
        mSettings.Schedule.Remove(mSettings.GetScheduleIndex(lstSchedule.SelectedItems(0).Text))
        RemoveScheduleFromListView(lstSchedule.SelectedItems(0))
    End Sub

    Private Sub RemoveScheduleFromListView(ByVal Item As ListViewItem)
        lstSchedule.Items.Remove(Item)
    End Sub

    Private Sub ShowScheduleInListView(ByVal Schedule As cSchedule)
        Dim oSchedItem As ListViewItem = New ListViewItem

        If Schedule.Enabled Then
            oSchedItem.BackColor = Color.LightGreen
        End If

        oSchedItem.Text = Schedule.Time.ToLongTimeString
        oSchedItem.SubItems.Add(System.IO.Path.GetFileNameWithoutExtension(Schedule.Sound1))
        oSchedItem.SubItems.Add(System.IO.Path.GetFileNameWithoutExtension(Schedule.Sound2))
        oSchedItem.SubItems.Add(Schedule.SearchPath)
        lstSchedule.Items.Add(oSchedItem)
        oSchedItem = Nothing
    End Sub

    Private Sub frmMain_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        SaveSettings()
        mTimer.Stop()
        mSettings.Dispose()
    End Sub

    Private Sub mCalendar_DateSelected(ByVal Value As Date) Handles mCalendar.DateSelected
        lstOffDays.Items.Add(Value.ToShortDateString)
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        SaveSettings()
        LoadSettings()
    End Sub

    Private Sub SaveSettings()
        mSettings.Sunday = chkNRSunday.Checked
        mSettings.Monday = chkNRMonday.Checked
        mSettings.Tuesday = chkNRTuesday.Checked
        mSettings.Wednesday = chkNRWednesday.Checked
        mSettings.Thursday = chkNRThursday.Checked
        mSettings.Friday = chkNRFriday.Checked
        mSettings.Saturday = chkNRSaturday.Checked
        mSettings.WAVsFolder = lblWAVsFolder.Text

        mSettings.StopPlayAfterXSeconds = updnNbSecondsToPlay.Value
        mSettings.ClearOffDays()
        For i As Integer = 0 To lstOffDays.Items.Count - 1
            mSettings.AddOffDay(lstOffDays.Items(i))
        Next

        Dim sFilename As String
        Try
            sFilename = System.Configuration.ConfigurationSettings.AppSettings("FilePath")

            If sFilename Is Nothing Then
                sFilename = Application.StartupPath & "\settings.xml"
            End If
        Catch Ex As Exception
            sFilename = Application.StartupPath & "\settings.xml"
        End Try

        mSettings.SaveToFile(sFilename)
    End Sub

    Private Sub LoadSettings()
        Try
            mSettings = New cSettings(Application.StartupPath & "\settings.xml")
        Catch Ex As Exception
            mSettings = New cSettings(System.Configuration.ConfigurationSettings.AppSettings("FilePath"))
        End Try

        lstSchedule.Items.Clear()
        lstOffDays.Items.Clear()

        chkNRSunday.Checked = mSettings.Sunday
        chkNRMonday.Checked = mSettings.Monday
        chkNRTuesday.Checked = mSettings.Tuesday
        chkNRWednesday.Checked = mSettings.Wednesday
        chkNRThursday.Checked = mSettings.Thursday
        chkNRFriday.Checked = mSettings.Friday
        chkNRSaturday.Checked = mSettings.Saturday

        lblWAVsFolder.Text = mSettings.WAVsFolder
        For i As Integer = 0 To mSettings.CountOffDays - 1
            lstOffDays.Items.Add(mSettings.GetOffDay(i))
        Next

        For Each oSched As cSchedule In mSettings.Schedule
            ShowScheduleInListView(oSched)
        Next

        If mSettings.StopPlayAfterXSeconds > 0 Then
            mPlayStopTimer.Interval = mSettings.StopPlayAfterXSeconds * 1000
            updnNbSecondsToPlay.Value = mSettings.StopPlayAfterXSeconds
        End If

        lstSchedule.Sorting = SortOrder.Ascending
        lstSchedule.Sort()
    End Sub

    Private Sub cmdRestartService_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRestartService.Click
        SetServiceTimer()
    End Sub

    Private Sub SetServiceTimer()
        If mTimer.Enabled Then
            cmdRestartService.Text = "D�marrer le service"
            mTimer.Stop()
        Else
            cmdRestartService.Text = "Arr�ter le service"
            mTimer.Start()
        End If
    End Sub

    Private Sub mTimer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles mTimer.Elapsed

        Select Case DateTime.Now.DayOfWeek
            Case DayOfWeek.Sunday
                If mSettings.Sunday Then
                    Exit Sub
                End If
            Case DayOfWeek.Monday
                If mSettings.Monday Then
                    Exit Sub
                End If
            Case DayOfWeek.Tuesday
                If mSettings.Tuesday Then
                    Exit Sub
                End If
            Case DayOfWeek.Wednesday
                If mSettings.Wednesday Then
                    Exit Sub
                End If
            Case DayOfWeek.Thursday
                If mSettings.Thursday Then
                    Exit Sub
                End If
            Case DayOfWeek.Friday
                If mSettings.Friday Then
                    Exit Sub
                End If
            Case DayOfWeek.Saturday
                If mSettings.Saturday Then
                    Exit Sub
                End If
        End Select

        Dim oplst As WMPLib.IWMPPlaylist
        oplst = oWMP.newPlaylist("temp", "")

        For Each oSched As cSchedule In mSettings.Schedule
            If Format(oSched.Time, "hh:mmtt") = Format(DateTime.Now, "hh:mmtt") Then
                mTimer.Enabled = False

                Try
                    If Not oSched.Played And oSched.Enabled Then
                        mPlayStopTimer.Stop()
                        oWMP.Ctlcontrols.stop()

                        Select Case oSched.Sound1
                            Case "Random"
                                AddFileToPlaylist(oSched, oplst)
                            Case ""
                            Case Else
                                AddFileToPlaylist(oSched.Sound1, oplst)
                        End Select

                        Select Case oSched.Sound2
                            Case "Random"
                                AddFileToPlaylist(oSched, oplst)
                            Case ""
                            Case Else
                                AddFileToPlaylist(oSched.Sound2, oplst)
                        End Select

                        oSched.Played = True
                        WMPPlay(oplst)
                        Exit For
                    End If

                Catch Ex As Exception
                    Throw Ex
                End Try

            Else
                oSched.Played = False
            End If
        Next

        mTimer.Enabled = True
    End Sub

    Private Sub WMPPlay(ByVal playlist As WMPLib.IWMPPlaylist)
        oWMP.currentPlaylist = playlist

        oWMP.Ctlcontrols.play()
        mPlayFileIndex = 0
        mPlayStopTimer.Start()
    End Sub

    'Private Sub WMPPlay(ByVal filename As String)
    '    oWMP.launchURL(filename)
    '    mPlayStopTimer.Enabled = True
    'End Sub

    Private Sub AddFileToPlaylist(ByVal filename As String, ByRef playlist As WMPLib.IWMPPlaylist)
        Dim oWMPMedia As WMPLib.IWMPMedia
        oWMPMedia = oWMP.mediaCollection.add(filename)
        'EventLog.WriteEntry("Klingeln", "Playing: " & FileName, EventLogEntryType.Information)

        playlist.appendItem(oWMPMedia)
    End Sub

    'Cette fonction re�oit 2 param�tres. Le premier �tant le fichier voulu,
    ' le 2e est l'autre fichier, qu'on veut �viter de jouer et ainsi ne jamais
    ' jouer le m�me fichier deux fois de suite
    Private Sub AddFileToPlaylist(ByVal Schedule As cSchedule, ByRef playlist As WMPLib.IWMPPlaylist)
        Randomize()
        Dim sFiles As String()
        Dim sFiles2 As String()
        Dim lnPos As Integer

        If Schedule.SearchPath = "" Then
            sFiles = System.IO.Directory.GetFiles(mSettings.WAVsFolder, "*.mp3", IO.SearchOption.AllDirectories)
            sFiles2 = System.IO.Directory.GetFiles(mSettings.WAVsFolder, "*.wav", IO.SearchOption.AllDirectories)
        Else
            sFiles = System.IO.Directory.GetFiles(Schedule.SearchPath, "*.mp3", IO.SearchOption.AllDirectories)
            sFiles2 = System.IO.Directory.GetFiles(Schedule.SearchPath, "*.wav", IO.SearchOption.AllDirectories)
        End If

        For i As Integer = 0 To UBound(sFiles2)
            ReDim Preserve sFiles(UBound(sFiles) + 1)
            sFiles(UBound(sFiles)) = sFiles2(i)
        Next

        Do
            lnPos = CInt(Int((UBound(sFiles) - (LBound(sFiles) + 1) + 1) * Rnd() + (LBound(sFiles) + 1)))
        Loop Until sFiles(lnPos) <> Schedule.Sound2

        If lnPos > 0 Then
            'WMPPlay(sFiles(lnPos))
            AddFileToPlaylist(sFiles(lnPos), playlist)
        End If
    End Sub

    Private Sub lstSchedule_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstSchedule.DoubleClick
        ModifySchedule()
    End Sub

    Private Sub ModifySchedule()
        Dim ofrmSched As frmSchedule = New frmSchedule(mSettings)
        ofrmSched.Time = lstSchedule.SelectedItems(0).Text

        If ofrmSched.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Dim lnSchedIdx As Integer = mSettings.GetScheduleIndex(lstSchedule.SelectedItems(0).Text)
            Dim oSched As cSchedule

            If lnSchedIdx > 0 Then
                oSched = mSettings.Schedule.Item(lnSchedIdx)
                mSettings.Schedule.Remove(lnSchedIdx)
            End If

            oSched = New cSchedule

            oSched.Enabled = ofrmSched.Active
            oSched.Time = ofrmSched.Time

            If ofrmSched.Sound1 Is Nothing Then
                oSched.Sound1 = ""
            ElseIf ofrmSched.Sound1.FileName = "Random" Then
                oSched.Sound1 = "Random"
            Else
                oSched.Sound1 = ofrmSched.Sound1.FileName
            End If

            If ofrmSched.Sound2 Is Nothing Then
                oSched.Sound2 = ""
            ElseIf ofrmSched.Sound2.FileName = "Random" Then
                oSched.Sound2 = "Random"
            Else
                oSched.Sound2 = ofrmSched.Sound2.FileName
            End If

            oSched.SearchPath = ofrmSched.SearchPath

            mSettings.Schedule.Add(oSched)
            RemoveScheduleFromListView(lstSchedule.SelectedItems(0))
            ShowScheduleInListView(oSched)
            oSched = Nothing
        End If

            ofrmSched = Nothing
            SaveSettings()
            LoadSettings()
    End Sub

    Private Sub mPlayStopTimer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles mPlayStopTimer.Elapsed
        mPlayStopTimer.Stop()

        If mPlayFileIndex = 0 Then
            oWMP.Ctlcontrols.next()
            mPlayFileIndex = 1
            mPlayStopTimer.Start()
        Else
            mPlayFileIndex = 0
            oWMP.Ctlcontrols.stop()
        End If
    End Sub

    Private Sub updnNbSecondsToPlay_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles updnNbSecondsToPlay.Validated
        mPlayStopTimer.Interval = updnNbSecondsToPlay.Value * 1000
        mSettings.StopPlayAfterXSeconds = updnNbSecondsToPlay.Value
        SaveSettings()
    End Sub

    'Private Sub oWMP_MediaChange(ByVal sender As Object, ByVal e As AxWMPLib._WMPOCXEvents_MediaChangeEvent) Handles oWMP.MediaChange
    '    Static lcCurrentMedia As String

    '    If oWMP.currentMedia.name <> lcCurrentMedia Then
    '        'mPlayFileIndex = 1
    '        'lcCurrentMedia = oWMP.currentMedia.name
    '        'mPlayStopTimer.Start()
    '    End If


    '    'If lnMediachange > 0 Then
    '    '    mPlayFileIndex = 1
    '    '    lnMediachange = 0
    '    'Else
    '    '    lnMediachange = 1
    '    'End If
    'End Sub
End Class
