Public Class cFileItem
    Private mFileName As String
    Private mFilePath As String

    Public Property FileName() As String
        Get
            Return mFileName
        End Get
        Set(ByVal value As String)
            mFilePath = value
            mFileName = System.IO.Path.GetFileNameWithoutExtension(value)
        End Set
    End Property

    Public ReadOnly Property FilePath() As String
        Get
            Return mFilePath
        End Get
    End Property

    Public Overrides Function Tostring() As String
        Return mFileName
    End Function

End Class
