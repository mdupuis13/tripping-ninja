﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;

namespace StoredProc
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["StoredProcTestConnectionString"].ToString());

            StoredProc cmdDBName = new StoredProc(conn, "getDBName", new Dictionary<string, object>());

            StoredProc cmdGetPersonnes = new StoredProc(conn, "getNombreDePersonnes", new Dictionary<string, object>());

            var lesParams = new Dictionary<string, object>();
            lesParams.Add("IdPersonne", 11);
            StoredProc cmdGetUnePersonneParId = new StoredProc(conn, "obtenirUnePersonneParID", lesParams);


            lesParams.Clear();
            lesParams.Add("prenom", "Jon");
            lesParams.Add("nom", "Postel");
            StoredProc cmdAddPersonne = new StoredProc(conn, "ajouterUnePersonne", lesParams);

            Console.WriteLine(string.Format("Vous êtes connectés à la BD: {0}", cmdDBName.ExecuteScalar()));
            Console.WriteLine(string.Format("Il y a {0} personne(s) dans la BD.", cmdGetPersonnes.ExecuteScalar()));

            Console.WriteLine("Ajout de Jon Postel en cours...");
            cmdAddPersonne.ExecuteNonQuery();

            Console.WriteLine(string.Format("Il y a {0} personne(s) dans la BD.", cmdGetPersonnes.ExecuteScalar()));

            object[] values = cmdGetUnePersonneParId.ExecuteReader();
            Console.WriteLine(string.Format("{0} est la première personne dans la BD.", string.Join(" ", values.Skip(1).Take(2).ToArray())));

            Console.Write("\nTaper une touche pour quitter...");
            Console.ReadKey(true);
        }


    }
}
