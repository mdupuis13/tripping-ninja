﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoredProc
{

    public class StoredProc
    {
        SqlCommand cmd;

        public StoredProc(SqlConnection conn, string nomSP, Dictionary<string, object> paramList)
        {
            // safeguards against an empty dictionary
            if (nomSP.Length == 0)
            {
                throw new ArgumentNullException("Argument 'nomSP' cannot be an empty string.");
            }

            cmd = conn.CreateCommand();
            cmd.CommandText = nomSP;
            cmd.CommandType = CommandType.StoredProcedure;

            if (paramList.Count > 0)
            {
                cmd.Parameters.AddRange(getParameters(paramList).ToArray());
            }
        }

        public int ExecuteNonQuery()
        {
            if (cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();

            int result = cmd.ExecuteNonQuery();
            
            if (cmd.Connection.State == ConnectionState.Open)
                cmd.Connection.Close();

            return result;
        }

        public object ExecuteScalar()
        {
            if (cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();

            object result = cmd.ExecuteScalar();

            if (cmd.Connection.State == ConnectionState.Open)
                cmd.Connection.Close();

            return result == null ? 0 : result;
        }

        public object[] ExecuteReader()
        {
            if (cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();

            SqlDataReader result = cmd.ExecuteReader();
            object[] resultData = new object[3];

            if (result.HasRows)
            {
                result.Read();
                result.GetValues(resultData);
            }
            //cmd.Connection = null;
            if (cmd.Connection.State == ConnectionState.Open)
                cmd.Connection.Close();

            return resultData;
            
        }

        public SqlParameterCollection getAssignedParams()
        {
            return cmd.Parameters;
        }
        private List<SqlParameter> getParameters(Dictionary<string, object> paramList)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();

            foreach (KeyValuePair<string, object> param in paramList)
            {
                sqlParams.Add(new SqlParameter(String.Concat("@", param.Key), param.Value));
            }

            return sqlParams;
        }

    }
}
