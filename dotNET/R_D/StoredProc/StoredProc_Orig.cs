﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPatttern
{
    class StoredProc_Orig
    {
        public StoredProc_Orig()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddRange(getParameter("getdbname", new ArrayList()));
        }

        static private SqlParameter[] getParameter(string NomSP, ArrayList ParamValue)
        {
            SqlParameter[] sqlparam = null;

            switch (NomSP)
            {
                case "dcrsp_getIdTraitementDiffere":
                case "dcrsp_getStatutTraitementDiffere":
                    sqlparam = new SqlParameter[]
                    {
                        new SqlParameter("@JOB",ParamValue[0])
                    };
                    break;

                case "dcrsp_GetMessageTraitementDiffere":
                    sqlparam = new SqlParameter[]
                    {
                        new SqlParameter("@JOB",ParamValue[0]),
                        new SqlParameter("@idTraitement",ParamValue[1])
                    };
                    break;

                case "dcrsp_setStatutTraitementDiffere":
                    sqlparam = new SqlParameter[]
                    {
                        new SqlParameter("@JOB",ParamValue[0]),
                        new SqlParameter("@statut",Convert.ToInt32(ParamValue[1]).ToString())
                    };
                    break;

                default:
                    break;
            }

            return sqlparam;
        }
    }
}
