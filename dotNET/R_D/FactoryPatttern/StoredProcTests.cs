﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;
using System.Data.SqlClient;
using System.Collections;

namespace FactoryPatttern
{
    public class StoredProcTests
    {
        [Fact]
        public void GivenASPNameAndParams_WhenICreateAStoredProcObject_ItShouldContainAllParameters()
        {
            SqlConnection conn = new SqlConnection();
            Dictionary<string, object> theparams = new Dictionary<string, object>();
            theparams.Add("param1", 4);
            theparams.Add("param2", "ma valeur");

            StoredProc sp = new StoredProc(conn, "getdbname", theparams);

            sp.getAssignedParams().Count.Should().Be(2);
            sp.getAssignedParams()[0].ParameterName.Should().Be("@param1");
            sp.getAssignedParams()[0].Value.Should().Be(4);
            sp.getAssignedParams()[1].ParameterName.Should().Be("@param2");
            sp.getAssignedParams()[1].Value.Should().Be("ma valeur");
        }

        [Fact]
        public void GivenASPNameAndAnEmptyParamsDict_WhenICreateAStoredProcObject_ItShouldRaiseArgumentNullException()
        {
            Dictionary<string, object> theparams = new Dictionary<string, object>();

            Action action = () => new StoredProc(new SqlConnection(), "getdbname", theparams);
            action.ShouldThrow<ArgumentNullException>("paramList cannot be null or empty");
        }

        [Fact]
        public void GivenAnEmptySPName_WhenICreateAStoredProcObject_ItShouldRaiseArgumentNullException()
        {
            Dictionary<string, object> theparams = new Dictionary<string, object>();

            Action action = () => new StoredProc(new SqlConnection(), "", theparams);
            action.ShouldThrow<ArgumentNullException>("a stored procedure name must be provided");
        }
    }
}