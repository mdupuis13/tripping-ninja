﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPatttern
{

    public class StoredProc
    {
        SqlCommand cmd;

        public StoredProc(SqlConnection conn, string nomSP, Dictionary<string, object> paramList)
        {
            // safeguards against an empty dictionary
            if (nomSP.Length == 0)
            {
                throw new ArgumentNullException("Argument 'nomSP' cannot be an empty string.");
            }

            if (paramList.Equals(null) || paramList.Count == 0)
            {
                throw new ArgumentNullException("Argument 'paramList' must contain at least one element.");
            }

            cmd = conn.CreateCommand();
            cmd.CommandText = nomSP;
            cmd.Parameters.AddRange(getParameters(paramList).ToArray());
        }

        public int ExecuteNonQuery()
        {
            return cmd.ExecuteNonQuery();
        }

        public object ExecuteScalar()
        {
            return cmd.ExecuteScalar();
        }

        public SqlDataReader ExecuteReader()
        {
            return cmd.ExecuteReader();
        }

        public SqlParameterCollection getAssignedParams()
        {
            return cmd.Parameters;
        }
        private List<SqlParameter> getParameters(Dictionary<string, object> paramList)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();

            foreach (KeyValuePair<string, object> param in paramList)
            {
                sqlParams.Add(new SqlParameter(String.Concat("@", param.Key), param.Value));
            }

            return sqlParams;
        }

    }
}
