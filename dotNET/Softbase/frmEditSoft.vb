Public Class frmEditSoft
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Dim conn As OleDb.OleDbConnection = New OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=Softbase.sfb;Mode=Share Deny None;Extended Properties="""";Jet OLEDB:System database="""";Jet OLEDB:Registry Path="""";Jet OLEDB:Engine Type=5;Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Global Partial Bulk Ops=2;Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:Create System Database=False;Jet OLEDB:Encrypt Database=False;Jet OLEDB:Don't Copy Locale on Compact=False;Jet OLEDB:Compact Without Replica Repair=False;Jet OLEDB:SFP=False")

        mdlMain.FillCategory(conn, cboCategory)
        mdlMain.FillCompany(conn, cboCompany)
        mdlMain.FillLanguage(conn, cboLanguage)
        mdlMain.FillLocation(conn, cboLocation)

        conn.Dispose()
        conn = Nothing

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tabGeneral As System.Windows.Forms.TabPage
    Friend WithEvents TabFiles As System.Windows.Forms.TabPage
    Friend WithEvents cmdSearchPath As System.Windows.Forms.Button
    Friend WithEvents txtDateOfEntry As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboCategory As System.Windows.Forms.ComboBox
    Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
    Friend WithEvents cboLocation As System.Windows.Forms.ComboBox
    Friend WithEvents cboLanguage As System.Windows.Forms.ComboBox
    Friend WithEvents txtNbMedia As System.Windows.Forms.TextBox
    Friend WithEvents txtSoftName As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblLanguage As System.Windows.Forms.Label
    Friend WithEvents lblCategory As System.Windows.Forms.Label
    Friend WithEvents lblLocation As System.Windows.Forms.Label
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNote As System.Windows.Forms.TextBox
    Friend WithEvents txtSerialNumber As System.Windows.Forms.TextBox
    Friend WithEvents txtPath As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmdViewFile1 As System.Windows.Forms.Button
    Friend WithEvents cmdInsertFile1 As System.Windows.Forms.Button
    Friend WithEvents cmdViewFile2 As System.Windows.Forms.Button
    Friend WithEvents cmdInsertFile2 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.txtSoftName = New System.Windows.Forms.TextBox
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.tabGeneral = New System.Windows.Forms.TabPage
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.lblLanguage = New System.Windows.Forms.Label
        Me.lblCategory = New System.Windows.Forms.Label
        Me.lblLocation = New System.Windows.Forms.Label
        Me.lblCompany = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtDateOfEntry = New System.Windows.Forms.TextBox
        Me.cmdSearchPath = New System.Windows.Forms.Button
        Me.cboCategory = New System.Windows.Forms.ComboBox
        Me.cboCompany = New System.Windows.Forms.ComboBox
        Me.cboLocation = New System.Windows.Forms.ComboBox
        Me.cboLanguage = New System.Windows.Forms.ComboBox
        Me.txtNote = New System.Windows.Forms.TextBox
        Me.txtSerialNumber = New System.Windows.Forms.TextBox
        Me.txtPath = New System.Windows.Forms.TextBox
        Me.txtNbMedia = New System.Windows.Forms.TextBox
        Me.TabFiles = New System.Windows.Forms.TabPage
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.cmdViewFile1 = New System.Windows.Forms.Button
        Me.cmdInsertFile1 = New System.Windows.Forms.Button
        Me.cmdViewFile2 = New System.Windows.Forms.Button
        Me.cmdInsertFile2 = New System.Windows.Forms.Button
        Me.TabControl1.SuspendLayout()
        Me.tabGeneral.SuspendLayout()
        Me.TabFiles.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtSoftName
        '
        Me.txtSoftName.BackColor = System.Drawing.SystemColors.Window
        Me.txtSoftName.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSoftName.Location = New System.Drawing.Point(8, 8)
        Me.txtSoftName.Name = "txtSoftName"
        Me.txtSoftName.Size = New System.Drawing.Size(552, 29)
        Me.txtSoftName.TabIndex = 0
        Me.txtSoftName.Text = ""
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.tabGeneral)
        Me.TabControl1.Controls.Add(Me.TabFiles)
        Me.TabControl1.Location = New System.Drawing.Point(8, 56)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(552, 400)
        Me.TabControl1.TabIndex = 1
        '
        'tabGeneral
        '
        Me.tabGeneral.Controls.Add(Me.Label2)
        Me.tabGeneral.Controls.Add(Me.Label8)
        Me.tabGeneral.Controls.Add(Me.Label7)
        Me.tabGeneral.Controls.Add(Me.Label6)
        Me.tabGeneral.Controls.Add(Me.lblLanguage)
        Me.tabGeneral.Controls.Add(Me.lblCategory)
        Me.tabGeneral.Controls.Add(Me.lblLocation)
        Me.tabGeneral.Controls.Add(Me.lblCompany)
        Me.tabGeneral.Controls.Add(Me.Label1)
        Me.tabGeneral.Controls.Add(Me.txtDateOfEntry)
        Me.tabGeneral.Controls.Add(Me.cmdSearchPath)
        Me.tabGeneral.Controls.Add(Me.cboCategory)
        Me.tabGeneral.Controls.Add(Me.cboCompany)
        Me.tabGeneral.Controls.Add(Me.cboLocation)
        Me.tabGeneral.Controls.Add(Me.cboLanguage)
        Me.tabGeneral.Controls.Add(Me.txtNote)
        Me.tabGeneral.Controls.Add(Me.txtSerialNumber)
        Me.tabGeneral.Controls.Add(Me.txtPath)
        Me.tabGeneral.Controls.Add(Me.txtNbMedia)
        Me.tabGeneral.Location = New System.Drawing.Point(4, 22)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Size = New System.Drawing.Size(544, 374)
        Me.tabGeneral.TabIndex = 0
        Me.tabGeneral.Text = "General"
        Me.tabGeneral.ToolTipText = "General information about the software."
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(392, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 23)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Nb media :"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(8, 168)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(80, 23)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Note :"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(8, 144)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(80, 23)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Serial Number:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(8, 120)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 23)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Path :"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblLanguage
        '
        Me.lblLanguage.Location = New System.Drawing.Point(16, 64)
        Me.lblLanguage.Name = "lblLanguage"
        Me.lblLanguage.Size = New System.Drawing.Size(72, 23)
        Me.lblLanguage.TabIndex = 14
        Me.lblLanguage.Text = "Language :"
        Me.lblLanguage.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCategory
        '
        Me.lblCategory.Location = New System.Drawing.Point(304, 32)
        Me.lblCategory.Name = "lblCategory"
        Me.lblCategory.Size = New System.Drawing.Size(64, 23)
        Me.lblCategory.TabIndex = 13
        Me.lblCategory.Text = "Category :"
        Me.lblCategory.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblLocation
        '
        Me.lblLocation.Location = New System.Drawing.Point(16, 40)
        Me.lblLocation.Name = "lblLocation"
        Me.lblLocation.Size = New System.Drawing.Size(72, 23)
        Me.lblLocation.TabIndex = 12
        Me.lblLocation.Text = "Location :"
        Me.lblLocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCompany
        '
        Me.lblCompany.Location = New System.Drawing.Point(16, 16)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(72, 24)
        Me.lblCompany.TabIndex = 11
        Me.lblCompany.Text = "Company :"
        Me.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(320, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 24)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Date entered :"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDateOfEntry
        '
        Me.txtDateOfEntry.BackColor = System.Drawing.SystemColors.Control
        Me.txtDateOfEntry.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtDateOfEntry.Enabled = False
        Me.txtDateOfEntry.Location = New System.Drawing.Point(408, 8)
        Me.txtDateOfEntry.Name = "txtDateOfEntry"
        Me.txtDateOfEntry.Size = New System.Drawing.Size(96, 13)
        Me.txtDateOfEntry.TabIndex = 9
        Me.txtDateOfEntry.Text = ""
        '
        'cmdSearchPath
        '
        Me.cmdSearchPath.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdSearchPath.Location = New System.Drawing.Point(504, 120)
        Me.cmdSearchPath.Name = "cmdSearchPath"
        Me.cmdSearchPath.Size = New System.Drawing.Size(24, 24)
        Me.cmdSearchPath.TabIndex = 8
        Me.cmdSearchPath.Text = "..."
        '
        'cboCategory
        '
        Me.cboCategory.Location = New System.Drawing.Point(376, 32)
        Me.cboCategory.Name = "cboCategory"
        Me.cboCategory.Size = New System.Drawing.Size(128, 21)
        Me.cboCategory.TabIndex = 7
        '
        'cboCompany
        '
        Me.cboCompany.Location = New System.Drawing.Point(96, 16)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(144, 21)
        Me.cboCompany.TabIndex = 6
        '
        'cboLocation
        '
        Me.cboLocation.Location = New System.Drawing.Point(96, 40)
        Me.cboLocation.Name = "cboLocation"
        Me.cboLocation.Size = New System.Drawing.Size(144, 21)
        Me.cboLocation.TabIndex = 5
        '
        'cboLanguage
        '
        Me.cboLanguage.Location = New System.Drawing.Point(96, 64)
        Me.cboLanguage.Name = "cboLanguage"
        Me.cboLanguage.Size = New System.Drawing.Size(144, 21)
        Me.cboLanguage.TabIndex = 4
        '
        'txtNote
        '
        Me.txtNote.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNote.Location = New System.Drawing.Point(96, 168)
        Me.txtNote.Multiline = True
        Me.txtNote.Name = "txtNote"
        Me.txtNote.Size = New System.Drawing.Size(408, 192)
        Me.txtNote.TabIndex = 3
        Me.txtNote.Text = ""
        '
        'txtSerialNumber
        '
        Me.txtSerialNumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSerialNumber.Location = New System.Drawing.Point(96, 144)
        Me.txtSerialNumber.Name = "txtSerialNumber"
        Me.txtSerialNumber.Size = New System.Drawing.Size(408, 20)
        Me.txtSerialNumber.TabIndex = 2
        Me.txtSerialNumber.Text = ""
        '
        'txtPath
        '
        Me.txtPath.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPath.Location = New System.Drawing.Point(96, 120)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(408, 20)
        Me.txtPath.TabIndex = 1
        Me.txtPath.Text = ""
        '
        'txtNbMedia
        '
        Me.txtNbMedia.Location = New System.Drawing.Point(464, 56)
        Me.txtNbMedia.Name = "txtNbMedia"
        Me.txtNbMedia.Size = New System.Drawing.Size(40, 20)
        Me.txtNbMedia.TabIndex = 0
        Me.txtNbMedia.Text = ""
        '
        'TabFiles
        '
        Me.TabFiles.Controls.Add(Me.cmdInsertFile2)
        Me.TabFiles.Controls.Add(Me.cmdViewFile2)
        Me.TabFiles.Controls.Add(Me.cmdInsertFile1)
        Me.TabFiles.Controls.Add(Me.cmdViewFile1)
        Me.TabFiles.Controls.Add(Me.Label4)
        Me.TabFiles.Controls.Add(Me.Label3)
        Me.TabFiles.Location = New System.Drawing.Point(4, 22)
        Me.TabFiles.Name = "TabFiles"
        Me.TabFiles.Size = New System.Drawing.Size(544, 374)
        Me.TabFiles.TabIndex = 1
        Me.TabFiles.Text = "Files"
        Me.TabFiles.ToolTipText = "Import files into the database. Once imported, the faile on disk can be deleted."
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(24, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "File 2 :"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(24, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "File 1 :"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdViewFile1
        '
        Me.cmdViewFile1.Location = New System.Drawing.Point(128, 40)
        Me.cmdViewFile1.Name = "cmdViewFile1"
        Me.cmdViewFile1.Size = New System.Drawing.Size(48, 23)
        Me.cmdViewFile1.TabIndex = 2
        Me.cmdViewFile1.Text = "View"
        '
        'cmdInsertFile1
        '
        Me.cmdInsertFile1.Location = New System.Drawing.Point(184, 40)
        Me.cmdInsertFile1.Name = "cmdInsertFile1"
        Me.cmdInsertFile1.Size = New System.Drawing.Size(48, 23)
        Me.cmdInsertFile1.TabIndex = 3
        Me.cmdInsertFile1.Text = "Insert"
        '
        'cmdViewFile2
        '
        Me.cmdViewFile2.Location = New System.Drawing.Point(128, 72)
        Me.cmdViewFile2.Name = "cmdViewFile2"
        Me.cmdViewFile2.Size = New System.Drawing.Size(48, 23)
        Me.cmdViewFile2.TabIndex = 4
        Me.cmdViewFile2.Text = "View"
        '
        'cmdInsertFile2
        '
        Me.cmdInsertFile2.Location = New System.Drawing.Point(184, 72)
        Me.cmdInsertFile2.Name = "cmdInsertFile2"
        Me.cmdInsertFile2.Size = New System.Drawing.Size(48, 23)
        Me.cmdInsertFile2.TabIndex = 5
        Me.cmdInsertFile2.Text = "Insert"
        '
        'frmEditSoft
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(568, 462)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.txtSoftName)
        Me.Name = "frmEditSoft"
        Me.Text = "frmEditSoft"
        Me.TabControl1.ResumeLayout(False)
        Me.tabGeneral.ResumeLayout(False)
        Me.TabFiles.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private m_SoftID As Integer = -1
    Private m_SoftData As DataSet
    Private m_InfoFile As Object

    Public WriteOnly Property SoftID() As Integer
        Set(ByVal Value As Integer)
            m_SoftID = Value

            LoadSoftInfo()
        End Set
    End Property


    Private Sub LoadSoftInfo()
        If m_SoftID > 0 Then
            'Load existing
            Dim conn As OleDb.OleDbConnection = New OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=Softbase.sfb;Mode=Share Deny None;Extended Properties="""";Jet OLEDB:System database="""";Jet OLEDB:Registry Path="""";Jet OLEDB:Engine Type=5;Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Global Partial Bulk Ops=2;Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:Create System Database=False;Jet OLEDB:Encrypt Database=False;Jet OLEDB:Don't Copy Locale on Compact=False;Jet OLEDB:Compact Without Replica Repair=False;Jet OLEDB:SFP=False")

            FillForm(conn)

            conn.Dispose()
            conn = Nothing
        End If
    End Sub

    Private Sub txtSoftName_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSoftName.Enter
        Me.txtSoftName.BackColor = Color.White
    End Sub

    Private Sub txtSoftName_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSoftName.Leave
        Me.txtSoftName.BackColor = SystemColors.Control
    End Sub

    Private Sub cmdSearchPath_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearchPath.Click
        OpenFileDialog1.InitialDirectory = Application.ExecutablePath
            OpenFileDialog1.ShowDialog()

            If OpenFileDialog1.FileName() <> "" Then
                txtPath.Text = Microsoft.VisualBasic.Left(OpenFileDialog1.FileName, InStrRev(OpenFileDialog1.FileName, "\"))
            End If

    End Sub

    Private Sub FillForm(ByVal conn As OleDb.OleDbConnection)
        Dim strSQL As String
        strSQL = "SELECT  tblSoftware.SoftID,  " & ControlChars.CrLf & _
                 "        tblSoftware.SoftCompID,  " & ControlChars.CrLf & _
                 "        tblCompany.CompName,  " & ControlChars.CrLf & _
                 "        tblSoftware.SoftCatID,  " & ControlChars.CrLf & _
                 "        tblCategory.CatName,  " & ControlChars.CrLf & _
                 "        tblSoftware.SoftTitle,  " & ControlChars.CrLf & _
                 "        tblSoftware.SoftLocation,  " & ControlChars.CrLf & _
                 "        tblLocation.LocName,  " & ControlChars.CrLf & _
                 "        tblSoftware.SoftPath,  " & ControlChars.CrLf & _
                 "        tblSoftware.SoftSerial,  " & ControlChars.CrLf & _
                 "        tblSoftware.SoftNote,  " & ControlChars.CrLf & _
                 "        tblSoftware.SoftInfoFile,  " & ControlChars.CrLf & _
                 "        tblSoftware.SoftCrackZip,  " & ControlChars.CrLf & _
                 "        tblSoftware.SoftNbTotalMedia,  " & ControlChars.CrLf & _
                 "        tblSoftware.SoftEntryDate,  " & ControlChars.CrLf & _
                 "        tblSoftware.SoftLanguage, " & ControlChars.CrLf & _
                 "        tblLanguage.LangDesc  " & ControlChars.CrLf & _
                 "FROM tblLocation  " & ControlChars.CrLf & _
                 "    INNER JOIN (tblCategory  " & ControlChars.CrLf & _
                 "        RIGHT JOIN ((tblCompany  " & ControlChars.CrLf & _
                 "            RIGHT JOIN tblSoftware  " & ControlChars.CrLf & _
                 "            ON tblCompany.CompID=tblSoftware.SoftCompID)  " & ControlChars.CrLf & _
                 "                LEFT JOIN tblLanguage  " & ControlChars.CrLf & _
                 "                ON tblSoftware.SoftLanguage=tblLanguage.LangID)  " & ControlChars.CrLf & _
                 "            ON tblCategory.CatID=tblSoftware.SoftCatID)  " & ControlChars.CrLf & _
                 "        ON tblLocation.LocID=tblSoftware.SoftLocation  " & ControlChars.CrLf & _
                 "WHERE tblSoftware.SoftID = " & m_SoftID

        Dim dta As New OleDb.OleDbDataAdapter(strSQL, conn)


        Try
            conn.Open()

            m_SoftData = New DataSet
            m_SoftData = New DataSet
            m_SoftData.Tables.Add("SoftData")
            m_SoftData.Tables("SoftData").TableName = "SoftData"
            dta.Fill(m_SoftData, "SoftData")

            If m_SoftData.Tables(0).Rows.Count = 1 Then
                Dim dtaRow As DataRow = m_SoftData.Tables(0).Rows(0)
                Me.Text = dtaRow.Item("SoftTitle")
                txtSoftName.Text = Me.Text

                If Not dtaRow.Item("SoftEntryDate") Is DBNull.Value Then
                    txtDateOfEntry.Text = Format(dtaRow.Item("SoftEntryDate"), "d MMMM yyyy")
                End If

                If Not dtaRow.Item("CompName") Is DBNull.Value Then
                    cboCompany.SelectedIndex = cboCompany.FindStringExact(dtaRow.Item("CompName"))
                End If

                If Not dtaRow.Item("CatName") Is DBNull.Value Then
                    cboCategory.SelectedIndex = cboCategory.FindStringExact(dtaRow.Item("CatName"))
                End If

                If Not dtaRow.Item("LangDesc") Is DBNull.Value Then
                    cboLanguage.SelectedIndex = cboLanguage.FindStringExact(dtaRow.Item("LangDesc"))
                End If

                If Not dtaRow.Item("LocName") Is DBNull.Value Then
                    cboLocation.SelectedIndex = cboLocation.FindStringExact(dtaRow.Item("LocName"))
                End If

                If Not dtaRow.Item("SoftNbTotalMedia") Is DBNull.Value Then
                    txtNbMedia.Text = dtaRow.Item("SoftNbTotalMedia")
                End If

                If Not dtaRow.Item("SoftPath") Is DBNull.Value Then
                    txtPath.Text = dtaRow.Item("SoftPath")
                End If

                If Not dtaRow.Item("SoftSerial") Is DBNull.Value Then
                    txtSerialNumber.Text = dtaRow.Item("SoftSerial")
                End If

                If Not dtaRow.Item("SoftNote") Is DBNull.Value Then
                    txtNote.Text = dtaRow.Item("SoftNote")
                End If

                If Not dtaRow.Item("SoftInfoFile") Is DBNull.Value Then
                    m_InfoFile = dtaRow.Item("SoftInfoFile")
                End If

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            dta.Dispose()
            dta = Nothing

            conn.Close()
        End Try
    End Sub

    Private Sub cboCompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCompany.SelectedIndexChanged
        Dim cbo As ComboBox = sender
        If cbo.SelectedIndex > 0 Then
            'MsgBox(cbo.SelectedIndex & "     " & CType(cbo.SelectedItem, cItem).Value)
        End If
    End Sub

    Private Sub txtPath_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPath.DoubleClick
        Try
            Shell("explorer.exe """ & CType(sender, TextBox).Text & """", AppWinStyle.NormalFocus, True, 60)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub cmdViewFile1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdViewFile1.Click

    End Sub
End Class

