Public Class cItem

    Private m_Desc As Object
    Private m_Value As Object

    Public Property Description() As Object
        Get
            Return m_Desc
        End Get
        Set(ByVal Value As Object)
            m_Desc = Value
        End Set
    End Property

    Public Property Value() As Object
        Get
            Return m_Value
        End Get
        Set(ByVal Value As Object)
            m_Value = Value
        End Set
    End Property

    Public Sub New(ByVal Description As Object, ByVal Value As Object)
        m_Desc = Description
        m_Value = Value
    End Sub

    Public Overrides Function ToString() As String
        Return CType(Description, String)
    End Function

End Class
