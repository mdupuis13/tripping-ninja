Public Class frmMain
    Inherits System.Windows.Forms.Form

    Private m_SoftList As DataSet
    Private m_FormLoad As Boolean
    Private m_NbClick As Integer

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If

            m_SoftList.Dispose()
            m_SoftList = Nothing
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents grdMain As System.Windows.Forms.DataGrid
    Friend WithEvents cboFilterLanguage As System.Windows.Forms.ComboBox
    Friend WithEvents cboFilterCompany As System.Windows.Forms.ComboBox
    Friend WithEvents cboFilterLocation As System.Windows.Forms.ComboBox
    Friend WithEvents fraFilter As System.Windows.Forms.GroupBox
    Friend WithEvents fraFilterDetach As System.Windows.Forms.GroupBox
    Friend WithEvents cboFilterCategory As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents SoftList As System.Windows.Forms.DataGridTableStyle
    Friend WithEvents SoftID As System.Windows.Forms.DataGridTextBoxColumn
    Friend Shadows WithEvents CompanyName As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents SoftTitle As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents LangAbbr As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents LocationName As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents LocationRemovable As System.Windows.Forms.DataGridBoolColumn
    Friend WithEvents CategoryName As System.Windows.Forms.DataGridTextBoxColumn
    Friend WithEvents txtSearchTitle As System.Windows.Forms.TextBox
    Friend WithEvents optFilterRemovableAll As System.Windows.Forms.RadioButton
    Friend WithEvents optFilterRemovableNo As System.Windows.Forms.RadioButton
    Friend WithEvents optFilterRemovableYes As System.Windows.Forms.RadioButton
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents lblNbSoft As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMain))
        Me.grdMain = New System.Windows.Forms.DataGrid
        Me.SoftList = New System.Windows.Forms.DataGridTableStyle
        Me.SoftID = New System.Windows.Forms.DataGridTextBoxColumn
        Me.CompanyName = New System.Windows.Forms.DataGridTextBoxColumn
        Me.SoftTitle = New System.Windows.Forms.DataGridTextBoxColumn
        Me.LangAbbr = New System.Windows.Forms.DataGridTextBoxColumn
        Me.LocationName = New System.Windows.Forms.DataGridTextBoxColumn
        Me.LocationRemovable = New System.Windows.Forms.DataGridBoolColumn
        Me.CategoryName = New System.Windows.Forms.DataGridTextBoxColumn
        Me.fraFilter = New System.Windows.Forms.GroupBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.fraFilterDetach = New System.Windows.Forms.GroupBox
        Me.optFilterRemovableAll = New System.Windows.Forms.RadioButton
        Me.optFilterRemovableNo = New System.Windows.Forms.RadioButton
        Me.optFilterRemovableYes = New System.Windows.Forms.RadioButton
        Me.cboFilterLanguage = New System.Windows.Forms.ComboBox
        Me.cboFilterCategory = New System.Windows.Forms.ComboBox
        Me.cboFilterCompany = New System.Windows.Forms.ComboBox
        Me.cboFilterLocation = New System.Windows.Forms.ComboBox
        Me.txtSearchTitle = New System.Windows.Forms.TextBox
        Me.StatusBar1 = New System.Windows.Forms.StatusBar
        Me.lblNbSoft = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        CType(Me.grdMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.fraFilter.SuspendLayout()
        Me.fraFilterDetach.SuspendLayout()
        Me.SuspendLayout()
        '
        'grdMain
        '
        Me.grdMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdMain.BackgroundColor = System.Drawing.SystemColors.Control
        Me.grdMain.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdMain.CaptionForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.grdMain.CaptionVisible = False
        Me.grdMain.DataMember = ""
        Me.grdMain.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.grdMain.Location = New System.Drawing.Point(0, 144)
        Me.grdMain.Name = "grdMain"
        Me.grdMain.Size = New System.Drawing.Size(664, 256)
        Me.grdMain.TabIndex = 0
        Me.grdMain.TableStyles.AddRange(New System.Windows.Forms.DataGridTableStyle() {Me.SoftList})
        '
        'SoftList
        '
        Me.SoftList.AlternatingBackColor = System.Drawing.SystemColors.Info
        Me.SoftList.DataGrid = Me.grdMain
        Me.SoftList.GridColumnStyles.AddRange(New System.Windows.Forms.DataGridColumnStyle() {Me.SoftID, Me.CompanyName, Me.SoftTitle, Me.LangAbbr, Me.LocationName, Me.LocationRemovable, Me.CategoryName})
        Me.SoftList.GridLineStyle = System.Windows.Forms.DataGridLineStyle.None
        Me.SoftList.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.SoftList.MappingName = "SoftList"
        Me.SoftList.ReadOnly = True
        Me.SoftList.RowHeaderWidth = 10
        '
        'SoftID
        '
        Me.SoftID.Format = ""
        Me.SoftID.FormatInfo = Nothing
        Me.SoftID.MappingName = "SoftID"
        Me.SoftID.NullText = ""
        Me.SoftID.Width = 0
        '
        'CompanyName
        '
        Me.CompanyName.Format = ""
        Me.CompanyName.FormatInfo = Nothing
        Me.CompanyName.HeaderText = "Company"
        Me.CompanyName.MappingName = "CompName"
        Me.CompanyName.NullText = ""
        Me.CompanyName.Width = 75
        '
        'SoftTitle
        '
        Me.SoftTitle.Format = ""
        Me.SoftTitle.FormatInfo = Nothing
        Me.SoftTitle.HeaderText = "Title"
        Me.SoftTitle.MappingName = "SoftTitle"
        Me.SoftTitle.NullText = ""
        Me.SoftTitle.Width = 175
        '
        'LangAbbr
        '
        Me.LangAbbr.Format = ""
        Me.LangAbbr.FormatInfo = Nothing
        Me.LangAbbr.HeaderText = "Lang"
        Me.LangAbbr.MappingName = "LangAbbr"
        Me.LangAbbr.NullText = ""
        Me.LangAbbr.Width = 40
        '
        'LocationName
        '
        Me.LocationName.Format = ""
        Me.LocationName.FormatInfo = Nothing
        Me.LocationName.HeaderText = "Location"
        Me.LocationName.MappingName = "LocName"
        Me.LocationName.Width = 75
        '
        'LocationRemovable
        '
        Me.LocationRemovable.AllowNull = False
        Me.LocationRemovable.FalseValue = False
        Me.LocationRemovable.HeaderText = "Removable ?"
        Me.LocationRemovable.MappingName = "LocRemovable"
        Me.LocationRemovable.NullText = ""
        Me.LocationRemovable.NullValue = CType(resources.GetObject("LocationRemovable.NullValue"), Object)
        Me.LocationRemovable.TrueValue = True
        Me.LocationRemovable.Width = 75
        '
        'CategoryName
        '
        Me.CategoryName.Format = ""
        Me.CategoryName.FormatInfo = Nothing
        Me.CategoryName.HeaderText = "Category"
        Me.CategoryName.MappingName = "CatName"
        Me.CategoryName.NullText = ""
        Me.CategoryName.Width = 125
        '
        'fraFilter
        '
        Me.fraFilter.Controls.Add(Me.Label4)
        Me.fraFilter.Controls.Add(Me.Label3)
        Me.fraFilter.Controls.Add(Me.Label2)
        Me.fraFilter.Controls.Add(Me.Label1)
        Me.fraFilter.Controls.Add(Me.fraFilterDetach)
        Me.fraFilter.Controls.Add(Me.cboFilterLanguage)
        Me.fraFilter.Controls.Add(Me.cboFilterCategory)
        Me.fraFilter.Controls.Add(Me.cboFilterCompany)
        Me.fraFilter.Controls.Add(Me.cboFilterLocation)
        Me.fraFilter.Location = New System.Drawing.Point(8, 8)
        Me.fraFilter.Name = "fraFilter"
        Me.fraFilter.Size = New System.Drawing.Size(344, 120)
        Me.fraFilter.TabIndex = 1
        Me.fraFilter.TabStop = False
        Me.fraFilter.Text = "Filter"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(8, 88)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 23)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Language"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 23)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Category"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(8, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 23)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Company"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 23)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Location"
        '
        'fraFilterDetach
        '
        Me.fraFilterDetach.Controls.Add(Me.optFilterRemovableAll)
        Me.fraFilterDetach.Controls.Add(Me.optFilterRemovableNo)
        Me.fraFilterDetach.Controls.Add(Me.optFilterRemovableYes)
        Me.fraFilterDetach.Location = New System.Drawing.Point(240, 16)
        Me.fraFilterDetach.Name = "fraFilterDetach"
        Me.fraFilterDetach.Size = New System.Drawing.Size(88, 72)
        Me.fraFilterDetach.TabIndex = 4
        Me.fraFilterDetach.TabStop = False
        Me.fraFilterDetach.Text = "Removable ?"
        '
        'optFilterRemovableAll
        '
        Me.optFilterRemovableAll.Checked = True
        Me.optFilterRemovableAll.Location = New System.Drawing.Point(24, 48)
        Me.optFilterRemovableAll.Name = "optFilterRemovableAll"
        Me.optFilterRemovableAll.Size = New System.Drawing.Size(48, 16)
        Me.optFilterRemovableAll.TabIndex = 2
        Me.optFilterRemovableAll.TabStop = True
        Me.optFilterRemovableAll.Text = "All"
        '
        'optFilterRemovableNo
        '
        Me.optFilterRemovableNo.Location = New System.Drawing.Point(24, 32)
        Me.optFilterRemovableNo.Name = "optFilterRemovableNo"
        Me.optFilterRemovableNo.Size = New System.Drawing.Size(48, 16)
        Me.optFilterRemovableNo.TabIndex = 1
        Me.optFilterRemovableNo.Text = "No"
        '
        'optFilterRemovableYes
        '
        Me.optFilterRemovableYes.Location = New System.Drawing.Point(24, 16)
        Me.optFilterRemovableYes.Name = "optFilterRemovableYes"
        Me.optFilterRemovableYes.Size = New System.Drawing.Size(48, 16)
        Me.optFilterRemovableYes.TabIndex = 0
        Me.optFilterRemovableYes.Text = "Yes"
        '
        'cboFilterLanguage
        '
        Me.cboFilterLanguage.Location = New System.Drawing.Point(80, 88)
        Me.cboFilterLanguage.Name = "cboFilterLanguage"
        Me.cboFilterLanguage.Size = New System.Drawing.Size(121, 21)
        Me.cboFilterLanguage.TabIndex = 3
        '
        'cboFilterCategory
        '
        Me.cboFilterCategory.Location = New System.Drawing.Point(80, 64)
        Me.cboFilterCategory.Name = "cboFilterCategory"
        Me.cboFilterCategory.Size = New System.Drawing.Size(121, 21)
        Me.cboFilterCategory.TabIndex = 2
        '
        'cboFilterCompany
        '
        Me.cboFilterCompany.Location = New System.Drawing.Point(80, 40)
        Me.cboFilterCompany.Name = "cboFilterCompany"
        Me.cboFilterCompany.Size = New System.Drawing.Size(121, 21)
        Me.cboFilterCompany.TabIndex = 1
        '
        'cboFilterLocation
        '
        Me.cboFilterLocation.Location = New System.Drawing.Point(80, 16)
        Me.cboFilterLocation.Name = "cboFilterLocation"
        Me.cboFilterLocation.Size = New System.Drawing.Size(121, 21)
        Me.cboFilterLocation.TabIndex = 0
        '
        'txtSearchTitle
        '
        Me.txtSearchTitle.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchTitle.Location = New System.Drawing.Point(456, 408)
        Me.txtSearchTitle.Name = "txtSearchTitle"
        Me.txtSearchTitle.Size = New System.Drawing.Size(200, 20)
        Me.txtSearchTitle.TabIndex = 2
        Me.txtSearchTitle.Text = ""
        '
        'StatusBar1
        '
        Me.StatusBar1.Location = New System.Drawing.Point(0, 440)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Size = New System.Drawing.Size(664, 22)
        Me.StatusBar1.TabIndex = 4
        '
        'lblNbSoft
        '
        Me.lblNbSoft.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblNbSoft.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNbSoft.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblNbSoft.Location = New System.Drawing.Point(8, 408)
        Me.lblNbSoft.Name = "lblNbSoft"
        Me.lblNbSoft.Size = New System.Drawing.Size(216, 16)
        Me.lblNbSoft.TabIndex = 5
        Me.lblNbSoft.Text = "Label5"
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.Location = New System.Drawing.Point(384, 408)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 16)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Search:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(664, 462)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblNbSoft)
        Me.Controls.Add(Me.StatusBar1)
        Me.Controls.Add(Me.txtSearchTitle)
        Me.Controls.Add(Me.fraFilter)
        Me.Controls.Add(Me.grdMain)
        Me.MinimumSize = New System.Drawing.Size(625, 400)
        Me.Name = "frmMain"
        Me.Text = "Softbase"
        CType(Me.grdMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.fraFilter.ResumeLayout(False)
        Me.fraFilterDetach.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadForm()
    End Sub

    Private Sub LoadForm()
        m_FormLoad = True
        Dim conn As OleDb.OleDbConnection = New OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=Softbase.sfb;Mode=Share Deny None;Extended Properties="""";Jet OLEDB:System database="""";Jet OLEDB:Registry Path="""";Jet OLEDB:Engine Type=5;Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Global Partial Bulk Ops=2;Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:Create System Database=False;Jet OLEDB:Encrypt Database=False;Jet OLEDB:Don't Copy Locale on Compact=False;Jet OLEDB:Compact Without Replica Repair=False;Jet OLEDB:SFP=False")

        BindGrid(conn)

        mdlMain.FillCategory(conn, cboFilterCategory)
        mdlMain.FillCompany(conn, cboFilterCompany)
        mdlMain.FillLanguage(conn, cboFilterLanguage)
        mdlMain.FillLocation(conn, cboFilterLocation)

        conn.Dispose()
        conn = Nothing

        m_FormLoad = False

        FilterForm()
    End Sub

    Private Sub BindGrid(ByVal Conn As OleDb.OleDbConnection)
        Dim dta As New OleDb.OleDbDataAdapter("SELECT  tblSoftware.SoftID,  " & ControlChars.CrLf & _
                                              "        tblSoftware.[SoftTitle],  " & ControlChars.CrLf & _
                                              "        tblCompany.CompID,  " & ControlChars.CrLf & _
                                              "        tblCompany.CompName,  " & ControlChars.CrLf & _
                                              "        tblLanguage.LangID, " & ControlChars.CrLf & _
                                              "        tblLanguage.[LangAbbr], " & ControlChars.CrLf & _
                                              "        tblLocation.LocID,  " & ControlChars.CrLf & _
                                              "        tblLocation.LocName,  " & ControlChars.CrLf & _
                                              "        tblLocation.LocRemovable,  " & ControlChars.CrLf & _
                                              "        tblCategory.CatID,  " & ControlChars.CrLf & _
                                              "        tblCategory.CatName  " & ControlChars.CrLf & _
                                              "FROM tblLocation  " & ControlChars.CrLf & _
                                              "    INNER JOIN (tblCategory  " & ControlChars.CrLf & _
                                              "        RIGHT JOIN ((tblCompany  " & ControlChars.CrLf & _
                                              "            RIGHT JOIN tblSoftware  " & ControlChars.CrLf & _
                                              "            ON tblCompany.CompID=tblSoftware.SoftCompID)  " & ControlChars.CrLf & _
                                              "                LEFT JOIN tblLanguage  " & ControlChars.CrLf & _
                                              "                ON tblSoftware.SoftLanguage=tblLanguage.LangID)  " & ControlChars.CrLf & _
                                              "            ON tblCategory.CatID=tblSoftware.SoftCatID)  " & ControlChars.CrLf & _
                                              "        ON tblLocation.LocID=tblSoftware.SoftLocation  " & ControlChars.CrLf & _
                                              "ORDER BY tblCompany.CompName, [SoftTitle];", Conn)

        Try
            Conn.Open()

            m_SoftList = New DataSet
            m_SoftList = New DataSet
            m_SoftList.Tables.Add("SoftList")
            m_SoftList.Tables("SoftList").TableName = "SoftList"
            dta.Fill(m_SoftList, "SoftList")

            grdMain.DataSource = m_SoftList.Tables(0).DefaultView
            grdMain.ReadOnly = True
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            dta.Dispose()
            dta = Nothing

            Conn.Close()
        End Try

    End Sub


    Private Sub grdMain_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdMain.DoubleClick
        m_NbClick = 2
    End Sub

    Private Sub cboFilterCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFilterCategory.SelectedIndexChanged
        FilterForm()
    End Sub


    Private Sub cboFilterCategory_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboFilterCategory.Validating
        FilterForm()
    End Sub

    Private Sub FilterForm()
        If Not m_FormLoad And Not m_SoftList Is Nothing Then
            Dim strFilter As String

            If cboFilterCategory.Text <> "" Then
                If strFilter <> "" Then
                    strFilter &= " and "
                End If

                strFilter &= "CatID = " & CType(cboFilterCategory.SelectedItem, cItem).Value
            End If

            If cboFilterCompany.Text <> "" Then
                If strFilter <> "" Then
                    strFilter &= " and "
                End If

                strFilter &= "CompID = " & CType(cboFilterCompany.SelectedItem, cItem).Value
            End If

            If cboFilterLanguage.Text <> "" Then
                If strFilter <> "" Then
                    strFilter &= " and "
                End If

                strFilter &= "LangID = " & CType(cboFilterLanguage.SelectedItem, cItem).Value
            End If

            If cboFilterLocation.Text <> "" Then
                If strFilter <> "" Then
                    strFilter &= " and "
                End If

                strFilter &= "LocID = " & CType(cboFilterLocation.SelectedItem, cItem).Value
            End If

            If optFilterRemovableYes.Checked Then
                If strFilter <> "" Then
                    strFilter &= " and "
                End If

                strFilter &= "LocRemovable = 1"
            ElseIf optFilterRemovableNo.Checked Then
                If strFilter <> "" Then
                    strFilter &= " and "
                End If

                strFilter &= "LocRemovable = 0"
            End If

            If txtSearchTitle.Text <> "" Then
                If strFilter <> "" Then
                    strFilter &= " and "
                End If

                strFilter &= "SoftTitle like '*" & txtSearchTitle.Text & "*'"
            End If

            Dim dv As New DataView
            dv = New DataView(m_SoftList.Tables(0))
            dv.RowFilter = strFilter
            grdMain.DataSource = dv

            lblNbSoft.Text = "Total of " & dv.Count & " software."
        End If
    End Sub

    Private Sub optFilterRemovableAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optFilterRemovableAll.CheckedChanged
        If sender.checked Then
            FilterForm()
        End If
    End Sub

    Private Sub optFilterRemovableNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optFilterRemovableNo.CheckedChanged
        If sender.checked Then
            FilterForm()
        End If
    End Sub

    Private Sub optFilterRemovableYes_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optFilterRemovableYes.CheckedChanged
        If sender.checked Then
            FilterForm()
        End If
    End Sub

    Private Sub cboFilterCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFilterCompany.SelectedIndexChanged
        FilterForm()
    End Sub

    Private Sub cboFilterCompany_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboFilterCompany.Validating
        FilterForm()
    End Sub

    Private Sub cboFilterLanguage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFilterLanguage.SelectedIndexChanged
        FilterForm()
    End Sub

    Private Sub cboFilterLanguage_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboFilterLanguage.Validating
        FilterForm()
    End Sub

    Private Sub cboFilterLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFilterLocation.SelectedIndexChanged
        FilterForm()
    End Sub

    Private Sub cboFilterLocation_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cboFilterLocation.Validating
        FilterForm()
    End Sub

    Private Sub grdMain_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles grdMain.MouseUp
        If grdMain.HitTest(e.X, e.Y).Type = DataGrid.HitTestType.RowHeader And m_NbClick = 2 Then
            Dim frmEdit As New frmEditSoft

            frmEdit.SoftID = grdMain.Item(grdMain.CurrentRowIndex, 0)
            frmEdit.Show()

            frmEdit = Nothing

            m_NbClick = 0
        End If
    End Sub

    Private Sub txtSearchTitle_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchTitle.Validated
        FilterForm()
    End Sub
End Class
