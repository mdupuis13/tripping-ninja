Module mdlMain
    Public Sub FillCategory(ByVal Conn As OleDb.OleDbConnection, ByRef cbo As ComboBox)
        Dim dtcmd As New OleDb.OleDbCommand("SELECT tblCategory.CatName as Name, " & ControlChars.CrLf & _
                                            "       tblCategory.CatID as ID " & ControlChars.CrLf & _
                                            "FROM tblCategory " & ControlChars.CrLf & _
                                            "ORDER BY CatName", Conn)

        Try
            Dim dtr As OleDb.OleDbDataReader

            Conn.Open()
            dtr = dtcmd.ExecuteReader

            Dim itm As cItem

            Do While dtr.Read
                itm = New cItem(dtr.Item("Name"), dtr.Item("ID"))

                cbo.Items.Add(itm)
            Loop

            dtr = Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            Conn.Close()
        End Try

        cbo.SelectedIndex = -1
    End Sub

    Public Sub FillCompany(ByVal Conn As OleDb.OleDbConnection, ByRef cbo As ComboBox)
        Dim dtcmd As New OleDb.OleDbCommand("SELECT tblCompany.CompName as Name, " & ControlChars.CrLf & _
                                            "       tblCompany.CompID as ID " & ControlChars.CrLf & _
                                            "FROM tblCompany " & ControlChars.CrLf & _
                                            "ORDER BY CompName", Conn)

        Try
            Dim dtr As OleDb.OleDbDataReader

            Conn.Open()
            dtr = dtcmd.ExecuteReader

            Dim itm As cItem

            Do While dtr.Read
                itm = New cItem(dtr.Item("Name"), dtr.Item("ID"))

                cbo.Items.Add(itm)
            Loop

            cbo.DisplayMember = "Description"
            cbo.ValueMember = "Value"

            dtr = Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            Conn.Close()
        End Try

        cbo.SelectedIndex = -1
    End Sub

    Public Sub FillLanguage(ByVal Conn As OleDb.OleDbConnection, ByRef cbo As ComboBox)
        Dim dtcmd As New OleDb.OleDbCommand("SELECT tblLanguage.LangDesc as Name, " & ControlChars.CrLf & _
                                            "       tblLanguage.LangID as ID " & ControlChars.CrLf & _
                                            "FROM tblLanguage " & ControlChars.CrLf & _
                                            "ORDER BY LangAbbr", Conn)

        Try
            Dim dtr As OleDb.OleDbDataReader

            Conn.Open()
            dtr = dtcmd.ExecuteReader

            Dim itm As cItem

            Do While dtr.Read
                itm = New cItem(dtr.Item("Name"), dtr.Item("ID"))

                cbo.Items.Add(itm)
            Loop

            dtr = Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            Conn.Close()
        End Try

        cbo.SelectedIndex = -1
    End Sub

    Public Sub FillLocation(ByVal Conn As OleDb.OleDbConnection, ByRef cbo As ComboBox)
        Dim dtcmd As New OleDb.OleDbCommand("SELECT tblLocation.LocName as Name, " & ControlChars.CrLf & _
                                            "       tblLocation.LocID as ID " & ControlChars.CrLf & _
                                            "FROM tblLocation " & ControlChars.CrLf & _
                                            "ORDER BY LocName", Conn)

        Try
            Dim dtr As OleDb.OleDbDataReader

            Conn.Open()
            dtr = dtcmd.ExecuteReader

            Dim itm As cItem

            Do While dtr.Read
                itm = New cItem(dtr.Item("Name"), dtr.Item("ID"))

                cbo.Items.Add(itm)
                cbo.DisplayMember = "Description"
                cbo.ValueMember = "Value"
            Loop

            dtr = Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            Conn.Close()
        End Try

        cbo.SelectedIndex = -1
    End Sub

End Module
