Public Class API
    Private Declare Function SHBrowseForFolder Lib "shell32" (ByVal lpbi As BrowseInfo) As Long
    Private Declare Function SHGetPathFromIDList Lib "shell32" (ByVal pidList As Long, ByVal lpBuffer As String) As Long
    Private Declare Sub CoTaskMemFree Lib "ole32.dll" (ByVal hMem As Long)
    Private Declare Function lstrcat Lib "kernel32" Alias "lstrcatA" (ByVal lpString1 As String, ByVal lpString2 As String) As Long

    Public Structure BrowseInfo
        Public hWndOwner As Integer
        Public pIDLRoot As Integer
        Public pszDisplayName As Integer
        Public lpszTitle As Integer
        Public ulFlags As Integer
        Public lpfnCallback As Integer
        Public lParam As Integer
        Public iImage As Integer
    End Structure

    Public Const BIF_RETURNONLYFSDIRS As Integer = 1
    Public Const MAX_PATH As Integer = 260

    Public Function BrowseFolder(ByVal InitPath As String, _
                                 ByVal Title As String, _
                                 ByVal Flags As Integer, _
                                 ByVal hWndOwner As Integer) As String

    End Function

End Class
