Imports System
Imports System.IO
Imports LogicMD.Utils

Public Class FileGenerator
    Private m_logPLL As Clog

    Private Const MAX_COLS_PER_ROW As Byte = 30
    Private m_blnGetGIF As Boolean
    Private m_blnGetJPG As Boolean
    Private m_blnGetPNG As Boolean

    Private m_intTotalFileCount As Integer
    Private m_intMaxFiles As Integer
    Private m_intRootPathLen As Integer

    Private m_strBaseFilename As String
    Private m_strmFile As StreamWriter


    ' this event is raised whenever a file is read from the OS
    Public Event FileRead(ByVal CurrentCount As Integer, ByVal TotalCount As Integer)


    ' This sub is teh root of the HTML file generation.
    ' It processes
    Public Sub GenerateFiles(ByVal RootPath As String, ByVal BaseFileName As String, _
                             ByVal GetGIF As Boolean, ByVal GetJPG As Boolean, _
                             ByVal GetPNG As Boolean, ByVal MaxFiles As Integer, _
                             ByVal Recurse As Boolean)

        If RootPath <> "" Then
            m_blnGetGIF = GetGIF
            m_blnGetJPG = GetJPG
            m_blnGetPNG = GetPNG

            m_strBaseFilename = BaseFileName
            m_intTotalFileCount = GetFileCount(RootPath, Recurse)
            m_intRootPathLen = Len(RootPath)

            m_intMaxFiles = MaxFiles

            'RaiseEvent FileRead(0, m_intTotalFileCount)

            If m_intTotalFileCount > 0 Then
                m_logPLL = New Clog()

                ' here GetFileName returns de folder name, 
                ' for some reason when the path is not terminated by a "\",
                ' the Framework thinks the last word is a file.. so I leveraged this
                ' feature to get only the folder name without path information
                If m_logPLL.Open(RootPath & "\" & System.IO.Path.GetFileName(RootPath) & ".pll", False) Then
                    ProcessFolder(RootPath, Recurse)
                End If
            End If

            m_logPLL.Close()
            RaiseEvent FileRead(0, 1)
        End If
    End Sub

    Private Function GetFileCount(ByVal RootPath As String, ByVal recurse As Boolean) As Integer
        Dim fso As Scripting.FileSystemObject
        Dim file As Scripting.File
        Dim fldr As Scripting.Folder
        Dim subFldr As Scripting.Folder

        Dim intFileCount As Integer = 0

        fso = New Scripting.FileSystemObject()
        fldr = fso.GetFolder(RootPath)

        If fldr.Files.Count > 0 Then
            For Each file In fldr.Files
                Application.DoEvents()

                Select Case LCase(Right(file.Name, 3))
                    Case "gif"
                        If m_blnGetGIF Then
                            intFileCount += 1
                        End If
                    Case "jpg"
                        If m_blnGetJPG Then
                            intFileCount += 1
                        End If
                    Case "png"
                        If m_blnGetPNG Then
                            intFileCount += 1
                        End If
                End Select
            Next
        End If

        If fldr.SubFolders.Count > 0 And recurse Then
            For Each subFldr In fldr.SubFolders
                intFileCount += GetFileCount(subFldr.Path, recurse)
            Next
        End If

        fso = Nothing
        fldr = Nothing
        subFldr = Nothing

        Return intFileCount
    End Function

    Private Sub ProcessFolder(ByVal RootPath As String, ByVal Recurse As Boolean)

        Dim fso As Scripting.FileSystemObject
        Dim file As Scripting.File
        Dim fldr As Scripting.Folder
        Dim subFldr As Scripting.Folder

        Dim intCurrentFileNb As Integer = 1
        Dim intFileCount As Integer = 0
        Dim dblNbFilesNeeded As Double = 0

        Dim strBaseName As String = ""
        Dim strFileName As String = ""

        fso = New Scripting.FileSystemObject()
        fldr = fso.GetFolder(RootPath)

        ' generate the file for the current folder
        If m_strBaseFilename = "" Then
            strBaseName = fldr.Name
        Else
            strBaseName = m_strBaseFilename
        End If



        If fldr.Files.Count > 0 Then
            For Each file In fldr.Files
                Application.DoEvents()
                Select Case LCase(Right(file.Name, 3))
                    Case "gif"
                        If m_blnGetGIF Then
                            intFileCount += 1
                        End If
                    Case "jpg"
                        If m_blnGetJPG Then
                            intFileCount += 1
                        End If
                    Case "png"
                        If m_blnGetPNG Then
                            intFileCount += 1
                        End If
                End Select
            Next

            ' total number of files
            ' round to highest integer
            dblNbFilesNeeded = System.Math.Round(intFileCount / m_intMaxFiles)
            If dblNbFilesNeeded * m_intMaxFiles < intFileCount Then
                dblNbFilesNeeded = CType(dblNbFilesNeeded, Integer) + 1
            End If

            For intCurrentFileNb = 1 To CType(dblNbFilesNeeded, Integer)
                RaiseEvent FileRead(intCurrentFileNb, CType(dblNbFilesNeeded, Integer))
                Application.DoEvents()

                Try
                    strFileName = fldr.Path & "\" & strBaseName & Format(intCurrentFileNb, "000") & ".htm"

                    m_strmFile = New StreamWriter(strFileName)
                    m_logPLL.WriteLn("""." & Mid(strFileName, m_intRootPathLen + 1) & """, #" & Now & "#")

                    m_strmFile.WriteLine(CreateHeader(strBaseName, fldr.Path, intCurrentFileNb, intFileCount))

                    ' if more than one file, then put a table of links
                    If dblNbFilesNeeded > 1 Then
                        m_strmFile.WriteLine(CreateFileListTable(strBaseName, fldr.Path, intCurrentFileNb, CInt(dblNbFilesNeeded)))
                    End If

                    m_strmFile.WriteLine(CreateBody(fldr, intCurrentFileNb))

                    ' if more than one file, then put a table of links
                    If dblNbFilesNeeded > 1 Then
                        m_strmFile.WriteLine(CreateFileListTable(strBaseName, fldr.Path, intCurrentFileNb, CInt(dblNbFilesNeeded)))
                    End If

                    'm_strmFile.WriteLine(CreateFooter(strBaseName, fldr.Path, intCurrentFileNb, intFileCount))

                    m_strmFile.Flush()
                    m_strmFile.Close()
                Catch IOe As IOException
                    MsgBox("IOException: " & ControlChars.CrLf & IOe.Message, MsgBoxStyle.OKOnly, IOe.Source)
                Catch e As Exception
                    MsgBox(e.Message, MsgBoxStyle.OKOnly, e.Source)

                End Try
            Next
        End If


        If fldr.SubFolders.Count > 0 And Recurse Then
            For Each subFldr In fldr.SubFolders
                ProcessFolder(subFldr.Path, Recurse)
            Next
        End If

        fso = Nothing
        fldr = Nothing
        subFldr = Nothing

    End Sub

    Private Function CreateHeader(ByVal BaseName As String, _
                                  ByVal CurrentFolderPath As String, _
                                  ByVal CurrentFileNb As Integer, _
                                  ByVal TotalFiles As Integer) As String
        Return "<html>" & ControlChars.CrLf & _
               "<head><TITLE>" & BaseName & Format(CurrentFileNb, "000") & ".htm</TITLE></HEAD> " & ControlChars.CrLf & _
               "<BODY> " & ControlChars.CrLf & _
               "<CENTER><img src=""http://www.logic-md.com/html/Images/Logicmd_logo_140.gif"" border=0 height=140><br>" & ControlChars.CrLf & _
               "<H1><U>Picture List</U></H1></CENTER> " & ControlChars.CrLf & _
               "<B>Directory:&nbsp;" & CurrentFolderPath & "</B>" & ControlChars.CrLf


    End Function

    'Private Function CreateFooter(ByVal BaseName As String, _
    '                              ByVal CurrentFolderPath As String, _
    '                              ByVal CurrentFileNb As Integer, _
    '                              ByVal TotalFiles As Integer) As String

    'End Function

    Private Function CreateFileListTable(ByVal BaseName As String, _
                                         ByVal CurrentFolderPath As String, _
                                         ByVal CurrentFileNb As Integer, _
                                         ByVal TotalFiles As Integer) As String

        Dim strTable As String

        Dim dblRows As Double
        Dim intRow, intCol As Integer

        strTable = "<HR> " & ControlChars.CrLf & _
                   "<CENTER><TABLE border=0 cellpadding=0 cellspacing=1>" & ControlChars.CrLf

        dblRows = TotalFiles / MAX_COLS_PER_ROW
        If CInt(dblRows) * MAX_COLS_PER_ROW < TotalFiles Then
            dblRows += 1
        End If

        For intRow = 1 To CType(dblRows, Integer)
            Application.DoEvents()

            strTable += Space(4) & "<TR>" & ControlChars.CrLf

            For intCol = 1 To TotalFiles
                strTable += Space(8) & "<TD align=middle valign=center>"
                If intCol = CurrentFileNb Then
                    strTable += "<b>" & intCol & "</b>"
                Else
                    strTable += "<a href='" & BaseName & Format(intCol, "000") & ".htm'>" & intCol & "</a>"
                End If

                strTable += "</TD>" & ControlChars.CrLf
            Next

            strTable += Space(4) & "</TR>" & ControlChars.CrLf
        Next

        strTable += "</TABLE></CENTER>" & ControlChars.CrLf
        Return strTable
    End Function

    Private Function CreateBody(ByVal folder As Scripting.Folder, _
                                ByVal CurrentFileNb As Integer) As String
        Dim file As Scripting.File

        Dim intStartFile As Integer
        Dim intFile As Integer
        Dim strBody As String = ""

        intStartFile = (CurrentFileNb * m_intMaxFiles) - m_intMaxFiles + 1
        intFile = 1

        For Each file In folder.Files
            Application.DoEvents()

            Select Case LCase(Right(file.Name, 3))
                Case "gif"
                    If m_blnGetGIF Then
                        If intFile >= intStartFile And intFile < m_intMaxFiles + intStartFile Then
                            strBody += "<BR><TABLE WIDTH=100% COLS=2><TR><TH COLSPAN=2 WIDTH=100%><IMG SRC='" & file.Name & "' WIDTH=100%></TH></TR>"
                            strBody += "<TR><TD>" & file.Name & "</TD><TD ALIGN=RIGHT>Size: " & Format(CType(file.Size, Long), "D") & " bytes</TD></TR></TABLE>"
                            strBody += ControlChars.CrLf

                        ElseIf intFile >= m_intMaxFiles + intStartFile Then
                            Exit For
                        End If
                    End If

                    intFile += 1

                Case "jpg"
                    If intFile >= intStartFile And intFile < m_intMaxFiles + intStartFile Then
                        If m_blnGetJPG Then
                            strBody += "<BR><TABLE WIDTH=100% COLS=2><TR><TH COLSPAN=2 WIDTH=100%><IMG SRC='" & file.Name & "' WIDTH=100%></TH></TR>"
                            strBody += "<TR><TD>" & file.Name & "</TD><TD ALIGN=RIGHT>Size: " & Format(CType(file.Size, Long), "D") & " bytes</TD></TR></TABLE>"
                            strBody += ControlChars.CrLf

                        ElseIf intFile >= m_intMaxFiles + intStartFile Then
                            Exit For
                        End If
                    End If

                    intFile += 1

                Case "png"
                    If m_blnGetPNG Then
                        If intFile >= intStartFile And intFile < m_intMaxFiles + intStartFile Then
                            strBody += "<BR><TABLE WIDTH=100% COLS=2><TR><TH COLSPAN=2 WIDTH=100%><IMG SRC='" & file.Name & "' WIDTH=100%></TH></TR>"
                            strBody += "<TR><TD>" & file.Name & "</TD><TD ALIGN=RIGHT>Size: " & Format(CType(file.Size, Long), "D") & " bytes</TD></TR></TABLE>"
                            strBody += ControlChars.CrLf

                        ElseIf intFile >= m_intMaxFiles + intStartFile Then
                            Exit For
                        End If
                    End If

                    intFile += 1

            End Select

        Next

        Return strBody & ControlChars.CrLf
    End Function
End Class

