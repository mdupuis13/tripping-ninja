Imports System.Diagnostics

Public Class frmMain
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdGO As System.Windows.Forms.Button
    Friend WithEvents pgbFileRead As System.Windows.Forms.ProgressBar
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tabGeneral As System.Windows.Forms.TabPage
    Friend WithEvents tabFilename As System.Windows.Forms.TabPage
    Friend WithEvents tabFileTypes As System.Windows.Forms.TabPage
    Friend WithEvents lblNbPictures As System.Windows.Forms.Label
    Friend WithEvents UdnNbPictures As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtCustomFilename As System.Windows.Forms.TextBox
    Friend WithEvents optFolderFilename As System.Windows.Forms.RadioButton
    Friend WithEvents optPiclistFilename As System.Windows.Forms.RadioButton
    Friend WithEvents optCustomFilename As System.Windows.Forms.RadioButton
    Friend WithEvents chkFileTypePNG As System.Windows.Forms.CheckBox
    Friend WithEvents chkFileTypeJPEG As System.Windows.Forms.CheckBox
    Friend WithEvents chkFileTypeGIF As System.Windows.Forms.CheckBox
    Friend WithEvents bffDialog As developingskills.com.DsBrowseForFolderDialog
    Friend WithEvents chkRecurse As System.Windows.Forms.CheckBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmMain))
        Me.cmdGO = New System.Windows.Forms.Button()
        Me.pgbFileRead = New System.Windows.Forms.ProgressBar()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tabGeneral = New System.Windows.Forms.TabPage()
        Me.lblNbPictures = New System.Windows.Forms.Label()
        Me.UdnNbPictures = New System.Windows.Forms.NumericUpDown()
        Me.tabFilename = New System.Windows.Forms.TabPage()
        Me.txtCustomFilename = New System.Windows.Forms.TextBox()
        Me.optFolderFilename = New System.Windows.Forms.RadioButton()
        Me.optPiclistFilename = New System.Windows.Forms.RadioButton()
        Me.optCustomFilename = New System.Windows.Forms.RadioButton()
        Me.tabFileTypes = New System.Windows.Forms.TabPage()
        Me.chkFileTypePNG = New System.Windows.Forms.CheckBox()
        Me.chkFileTypeJPEG = New System.Windows.Forms.CheckBox()
        Me.chkFileTypeGIF = New System.Windows.Forms.CheckBox()
        Me.bffDialog = New developingskills.com.DsBrowseForFolderDialog(Me.components)
        Me.chkRecurse = New System.Windows.Forms.CheckBox()
        Me.TabControl1.SuspendLayout()
        Me.tabGeneral.SuspendLayout()
        CType(Me.UdnNbPictures, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabFilename.SuspendLayout()
        Me.tabFileTypes.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdGO
        '
        Me.cmdGO.Location = New System.Drawing.Point(80, 176)
        Me.cmdGO.Name = "cmdGO"
        Me.cmdGO.Size = New System.Drawing.Size(68, 28)
        Me.cmdGO.TabIndex = 5
        Me.cmdGO.Text = "GO !"
        '
        'pgbFileRead
        '
        Me.pgbFileRead.Cursor = System.Windows.Forms.Cursors.Default
        Me.pgbFileRead.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pgbFileRead.Location = New System.Drawing.Point(0, 216)
        Me.pgbFileRead.Name = "pgbFileRead"
        Me.pgbFileRead.Size = New System.Drawing.Size(214, 8)
        Me.pgbFileRead.Step = 50
        Me.pgbFileRead.TabIndex = 6
        Me.pgbFileRead.Value = 45
        '
        'TabControl1
        '
        Me.TabControl1.Controls.AddRange(New System.Windows.Forms.Control() {Me.tabGeneral, Me.tabFilename, Me.tabFileTypes})
        Me.TabControl1.Location = New System.Drawing.Point(4, 4)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(208, 168)
        Me.TabControl1.TabIndex = 10
        '
        'tabGeneral
        '
        Me.tabGeneral.Controls.AddRange(New System.Windows.Forms.Control() {Me.chkRecurse, Me.lblNbPictures, Me.UdnNbPictures})
        Me.tabGeneral.Location = New System.Drawing.Point(4, 22)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Size = New System.Drawing.Size(200, 142)
        Me.tabGeneral.TabIndex = 0
        Me.tabGeneral.Text = "General"
        '
        'lblNbPictures
        '
        Me.lblNbPictures.Location = New System.Drawing.Point(16, 68)
        Me.lblNbPictures.Name = "lblNbPictures"
        Me.lblNbPictures.Size = New System.Drawing.Size(124, 16)
        Me.lblNbPictures.TabIndex = 5
        Me.lblNbPictures.Text = "Number of pictures / file "
        '
        'UdnNbPictures
        '
        Me.UdnNbPictures.Location = New System.Drawing.Point(140, 64)
        Me.UdnNbPictures.Maximum = New Decimal(New Integer() {500, 0, 0, 0})
        Me.UdnNbPictures.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.UdnNbPictures.Name = "UdnNbPictures"
        Me.UdnNbPictures.Size = New System.Drawing.Size(44, 20)
        Me.UdnNbPictures.TabIndex = 4
        Me.UdnNbPictures.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.UdnNbPictures.ThousandsSeparator = True
        Me.UdnNbPictures.Value = New Decimal(New Integer() {25, 0, 0, 0})
        '
        'tabFilename
        '
        Me.tabFilename.Controls.AddRange(New System.Windows.Forms.Control() {Me.txtCustomFilename, Me.optFolderFilename, Me.optPiclistFilename, Me.optCustomFilename})
        Me.tabFilename.Location = New System.Drawing.Point(4, 22)
        Me.tabFilename.Name = "tabFilename"
        Me.tabFilename.Size = New System.Drawing.Size(200, 142)
        Me.tabFilename.TabIndex = 1
        Me.tabFilename.Text = "Filename"
        '
        'txtCustomFilename
        '
        Me.txtCustomFilename.Location = New System.Drawing.Point(32, 96)
        Me.txtCustomFilename.MaxLength = 25
        Me.txtCustomFilename.Name = "txtCustomFilename"
        Me.txtCustomFilename.Size = New System.Drawing.Size(144, 20)
        Me.txtCustomFilename.TabIndex = 19
        Me.txtCustomFilename.Text = ""
        '
        'optFolderFilename
        '
        Me.optFolderFilename.Checked = True
        Me.optFolderFilename.Location = New System.Drawing.Point(32, 48)
        Me.optFolderFilename.Name = "optFolderFilename"
        Me.optFolderFilename.Size = New System.Drawing.Size(144, 20)
        Me.optFolderFilename.TabIndex = 18
        Me.optFolderFilename.TabStop = True
        Me.optFolderFilename.Text = "Same as folder"
        '
        'optPiclistFilename
        '
        Me.optPiclistFilename.Location = New System.Drawing.Point(32, 24)
        Me.optPiclistFilename.Name = "optPiclistFilename"
        Me.optPiclistFilename.Size = New System.Drawing.Size(144, 24)
        Me.optPiclistFilename.TabIndex = 17
        Me.optPiclistFilename.Text = "Piclist"
        '
        'optCustomFilename
        '
        Me.optCustomFilename.Location = New System.Drawing.Point(32, 68)
        Me.optCustomFilename.Name = "optCustomFilename"
        Me.optCustomFilename.Size = New System.Drawing.Size(144, 24)
        Me.optCustomFilename.TabIndex = 16
        Me.optCustomFilename.Text = "Custom filename"
        '
        'tabFileTypes
        '
        Me.tabFileTypes.Controls.AddRange(New System.Windows.Forms.Control() {Me.chkFileTypePNG, Me.chkFileTypeJPEG, Me.chkFileTypeGIF})
        Me.tabFileTypes.Location = New System.Drawing.Point(4, 22)
        Me.tabFileTypes.Name = "tabFileTypes"
        Me.tabFileTypes.Size = New System.Drawing.Size(200, 142)
        Me.tabFileTypes.TabIndex = 2
        Me.tabFileTypes.Text = "File Types"
        '
        'chkFileTypePNG
        '
        Me.chkFileTypePNG.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkFileTypePNG.Checked = True
        Me.chkFileTypePNG.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFileTypePNG.Location = New System.Drawing.Point(52, 80)
        Me.chkFileTypePNG.Name = "chkFileTypePNG"
        Me.chkFileTypePNG.Size = New System.Drawing.Size(88, 20)
        Me.chkFileTypePNG.TabIndex = 5
        Me.chkFileTypePNG.Text = "&PNG"
        '
        'chkFileTypeJPEG
        '
        Me.chkFileTypeJPEG.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkFileTypeJPEG.Checked = True
        Me.chkFileTypeJPEG.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFileTypeJPEG.Location = New System.Drawing.Point(52, 60)
        Me.chkFileTypeJPEG.Name = "chkFileTypeJPEG"
        Me.chkFileTypeJPEG.Size = New System.Drawing.Size(88, 20)
        Me.chkFileTypeJPEG.TabIndex = 4
        Me.chkFileTypeJPEG.Text = "&JPEG"
        '
        'chkFileTypeGIF
        '
        Me.chkFileTypeGIF.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkFileTypeGIF.Checked = True
        Me.chkFileTypeGIF.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFileTypeGIF.Location = New System.Drawing.Point(52, 40)
        Me.chkFileTypeGIF.Name = "chkFileTypeGIF"
        Me.chkFileTypeGIF.Size = New System.Drawing.Size(88, 20)
        Me.chkFileTypeGIF.TabIndex = 3
        Me.chkFileTypeGIF.Text = "&GIF"
        '
        'bffDialog
        '
        Me.bffDialog.Caption = Nothing
        Me.bffDialog.Title = Nothing
        '
        'chkRecurse
        '
        Me.chkRecurse.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkRecurse.Checked = True
        Me.chkRecurse.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkRecurse.Location = New System.Drawing.Point(16, 36)
        Me.chkRecurse.Name = "chkRecurse"
        Me.chkRecurse.Size = New System.Drawing.Size(164, 16)
        Me.chkRecurse.TabIndex = 6
        Me.chkRecurse.Text = "Recurse folders"
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(214, 224)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.TabControl1, Me.pgbFileRead, Me.cmdGO})
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Piclist"
        Me.TabControl1.ResumeLayout(False)
        Me.tabGeneral.ResumeLayout(False)
        CType(Me.UdnNbPictures, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabFilename.ResumeLayout(False)
        Me.tabFileTypes.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Private Const AppCaption As String = "Piclist "

    Private WithEvents m_FilGen As New FileGenerator()

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly.Location)
            Me.Text = AppCaption '& .FileMajorPart & "." & .FileMinorPart & " build " & .FileBuildPart
        End With

        initControlValues()
    End Sub

    Private Sub m_FilGen_FileRead(ByVal CurrentCount As Integer, ByVal TotalCount As Integer) Handles m_FilGen.FileRead
        If pgbFileRead.Maximum <> TotalCount Then
            pgbFileRead.Maximum = TotalCount
        End If

        pgbFileRead.Value = CurrentCount
        pgbFileRead.Refresh()
        Application.DoEvents()
    End Sub

    Private Sub initControlValues()
        UdnNbPictures.Value = 25
        optFolderFilename.Checked = True

        chkFileTypeGIF.CheckState = CheckState.Checked
        chkFileTypeJPEG.CheckState = CheckState.Checked
        chkFileTypePNG.CheckState = CheckState.Checked

        pgbFileRead.Value = 0
    End Sub

    Private Sub cmdGO_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGO.Click
        StartProcess()
    End Sub

    Private Sub StartProcess()
        Dim strFilename As String
        Dim bContinue As Boolean = True
        Dim strRootFolder As String = ""


        If optCustomFilename.Checked = True Then
            If txtCustomFilename.Text <> "" Then
                strFilename = txtCustomFilename.Text
            Else
                MsgBox("Veuillez spécifier un nom de fichier avec l'option Custom.", MsgBoxStyle.Information And MsgBoxStyle.OKOnly, "Pas de nom de fichier")
                bContinue = False
            End If

        ElseIf optFolderFilename.Checked = True Then
            strFilename = ""
        ElseIf optPiclistFilename.Checked = True Then
            strFilename = "Piclist"
        End If

        If bContinue Then
            cmdGO.Enabled = False
            strRootFolder = GetRootFolder()

            Cursor.Current = Cursors.WaitCursor
            Try
                m_FilGen.GenerateFiles(strRootFolder, _
                                       strFilename, _
                                       chkFileTypeGIF.Checked = True, _
                                       chkFileTypeJPEG.Checked = True, _
                                       chkFileTypePNG.Checked = True, _
                                       CType(UdnNbPictures.Value, Integer), _
                                       chkRecurse.Checked = True)
            Catch ex As Exception
                MsgBox(ex.Message, , ex.Source)
            Finally
                Cursor.Current = Cursors.Default
                cmdGO.Enabled = True
            End Try
        End If
    End Sub

    Private Function GetRootFolder() As String
        Dim strRoot As String

        'Set the title and caption of the browse for folder dialog
        bffDialog.Title = "Choisir un répertoire"
        bffDialog.Caption = "Choisir le répertoire initial"

        'Launch the browse for folder dialog
        Dim eRet As DialogResult = bffDialog.ShowDialog()
        If eRet = DialogResult.OK Then
            'Write the selected folder to the label
            strRoot = bffDialog.SelectedItem
        Else
            strRoot = ""
        End If

        Return strRoot
    End Function

    Private Sub optPiclistFilename_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optPiclistFilename.CheckedChanged
        txtCustomFilename.Text = ""
        txtCustomFilename.Visible = False
    End Sub

    Private Sub optFolderFilename_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optFolderFilename.CheckedChanged
        txtCustomFilename.Text = ""
        txtCustomFilename.Visible = False
    End Sub

    Private Sub optCustomFilename_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optCustomFilename.CheckedChanged
        txtCustomFilename.Text = ""
        txtCustomFilename.Visible = True
    End Sub
End Class


