Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Piclist by Logic-MD")> 
<Assembly: AssemblyDescription("HTML picture listing program")> 
<Assembly: AssemblyCompany("Logic-MD")> 
<Assembly: AssemblyProduct("PicList")> 
<Assembly: AssemblyCopyright("Logic-MD 2003")> 
<Assembly: AssemblyTrademark("Logic-MD 2003")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("C21688CA-1749-4259-8960-9746E231A4C5")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("3.1.0.0")> 
