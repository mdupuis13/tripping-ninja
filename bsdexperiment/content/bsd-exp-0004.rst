:title: Change of mind
:date: 2013-12-25
:slug: change_mind
:tags: FreeBSD, FreeNAS
:category: OS
:author: Martin Dupuis
:summary: Having heard a BSDNow! podcast, I began to think about my goal, the time available to me, to my familly and the burden of managing it all.

Time is a limited resource. Until now, I only managed to install FreeBSD and create a mirrored pool with my server. Not much for the pas six weeks.

Familly life, house renovation projects and just plain tiredness got the better of my willpower I think. Last week, I was listening to `Episode 15: Kickin' NAS <http://www.bsdnow.tv/episodes/2013_12_11-kickin_nas>`_ from the `BSDNow <http://www.bsdnow.tv>`_ podcast, and thoughts started to form into my head. 

    Why should I do all this by myself ?

    FreeNAS_ answers all my needs in a single package !

    Plus, do you remember that you already setup a FreeNAS_ server for a client ?


So, after thinking things through for a short, very short, time, I decided that it was time to download FreeNAS_ 9.2 and create a test VM.




.. Link definitions
.. _freenas: http://www.freenas.org

