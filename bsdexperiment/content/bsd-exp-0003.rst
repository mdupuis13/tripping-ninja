:title: FreeBSD 9.2 install fest (continued)...
:date: 2013-11-25
:slug: FreeBSD_install_fest_continued
:tags: OS, installation, FreeBSD 9.2
:category: OS
:author: Martin Dupuis
:summary: The installation continues, this time, I get a login prompt.

Week of nov. 17th
=================
In the course of last week, I tried again to insall FreeBSD and finally managed to properly understand what the handbook was telling me. 

As I said in my `previous post <{filename}/bsd-exp-0002.rst>`_, I managed to install FreeBSD using the default partitionning scheme, so FreeBSD runs on my hardware. Now, I just needed to figure out how to partition the way I want it to. I re-read, s-l-o-w-l-y, the `doc <https://www.freebsd.org/doc/handbook/bsdinstall-partitioning.html#bsdinstall-part-manual-splitfs>`_ and finally realized that FreeBSD needs some partitions (slices) to be in a particular order. Here's what I used finally:

.. code-block:: Text Only

    * 512Kb     freebsd-boot
    * 3G        /
    * 4G        /var
    * rest(60G) /usr

I simplified my partition scheme and it worked beautifully. I logged in and changed the OpenSSH daemon config to allow password login and root logins. "Oh sh*t ! you should not do that ! why oh why ?" Is what you are probable thinking. Well, this is a local server, behind a NAT with only FTP exposed. You should also know that my ISP only allows FTP inbound, nothing else, if I try to pass HTTP through port 21 it would be blocked, so they even block at the protocol level I think. This gives me a pretty good indication that it'd be hard to get to this machine directly.

Nov. 25th
=================
My inner voice told me to at least try to configure key-based authentication on the SSH daemon. I created local keys on my laptop using:

.. code-block:: bash

    $ ssh-keygen -t rsa -b 4096

Using scp, I copied the keys to my server on the root account (remember, I still have login access to root).
I logged in and created a user with the same username than my laptop. Moved, and renamed, the keyfile where it should go, and presto, key-based authentication works auto-magically.

.. code-block:: bash

    $ scp ~/.rsa/id_rsa.pub <myuser>@<myserverip>:/root
    $ adduser
    <answer all questions>
    $ su
    <enter root pass>
    $ mv /root/id_rsa.pub /home/<username>/.rsa/authaurized_keys
    $ exit

Stay tuned...
Next post will probably be about setting up a ZFS mirror on two 2Tb drives.
