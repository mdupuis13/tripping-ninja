:title: Test post
:date: 2013-04-07 11:00
:tags: meta
:summary: Test post for pelican setup

This is a test post for my `pelican <http://getpelican.com>`_ installation.

Content is coming soon...

Example Python syntax highlighting

.. code-block:: python

    # func1 prints "Hello world" on the standard output and returns 0 if no error occurs
    def func1():
        try:
            print 'Hello world !'
            return 0
        catch ex as Exception:
            return 1
        
