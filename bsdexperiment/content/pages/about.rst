:title: About
:date: 2013-04-07 19:31
:summary: Page explaining the purpose of this web site.

This site is a log of my foray into the use of the BSDs as a desktop operating system.

It's made with pelican_, a static web site generator build in python_. This is the first time I use pelican, so this is also a kind of experiment into using this kind of tool.

*The BSD Experiment* is written by myself `Martin Dupuis <http://martindupuis.info>`_ as a way to share knowledge and as documentation about my setup.


.. _pelican: http://blog.getpelican.com/
.. _python: http://python.org
