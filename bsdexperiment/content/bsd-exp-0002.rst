:title: FreeBSD 9.2 install fest !
:date: 2013-11-18
:slug: FreeBSD_install_fest
:tags: OS, installation, FreeBSD 9.2
:category: OS
:author: Martin Dupuis
:summary: How I tried and failed at installing FreeBSD 9.2 on my new server.

This past week-end I tried to install FreeBSD on my new server. Here's the summary of how it went:

Strike 1 (friday evening)
=========================
I deleted all existing partitions on the drive on the 73GB WD Raptor. Then came time to create the partitions as I want them to be.

.. code-block:: Text Only

    * 1G  /var
    * 1G  /var/log
    * 35G /
    * 10G /usr/home
    * 2G  /tmp
    * 8G  swap

After reboot, I have a message similar to :code:`"kernel not found at /boot/kernel"` at boot. So I decide to open the `FreeBSD Handbook`_
and read the partitioning section. Turns out FreeBSD requires a boot partition at the start of the disk to put the boot loader.

Strike 2
========
So I restart the installation, decide to go with GPT and add this :code:`boot` partition. I thus deleted the whole disk, change partition scheme to GPT recreated the parittions and... got almost the same message. Progress, but the the kind of progress I want.

Home run !
============
"Enough of this !": I said to myself. I am now doubting this drive is still good. So I start over and use *guided* partitioning as the most basic test. This time the server reboots into the friendly FreeBSD boot chooser.

Now I just need to figure out why I can't boot with the partition setup I want.


.. _freebsd handbook: https://www.freebsd.org/doc/handbook/
