:title: First post !
:date: 2013-11-12
:slug: first_post
:tags: Debian, LaCie, FreeBSD, history
:category: Meta
:author: Martin Dupuis
:summary: Historic and background information about the purpose of this blog. This post explains some reasons for this site, as well as my current hardware setup.

What is this site about ?
=========================

This is the ongoing story of my journey into the setup and configuration of my new file server. This is my first time using FreeBSD_ on a production server and I will try to keep a log of what I do, and sometimes, what I decide not to do, and why. I hope this server stays with me for a while.

I am a software programmer but I also like the technical aspect of computing. I always sought a technical solution to my needs and sometimes made decisions based on little more than thin air. My first file server was running windows2000, then I discovered Linux and switched promptly, but not without difficulty, damn Samba !

After a few years, I met somebody who raved about its  `LaCie 2big NAS <http://www.lacie.com/cafr/products/product.htm?id=10584>`_ box. I decided to buy one with two 1Tb drives configured as RAID 1. A little bit more than a year later, I changed the drives for two 2Tb units, Western Digital Greens'. This choice came to bit me behind my back later.

My Current setup
----------------

So now I have a 2Tb XFS [1]_ running on this NAS box for a while. Every day, Four times a day, my backup server mount the NAS through NFS and rsyncs all the data. 

Talking about it, I will describe it now. My backup server is housed into... an old LaCie 1U network storage box ! It's a hand me down from a client, the BIOS was fried and not salvagable. I replaced the whole thing with a micro-ATX motherboard with an Atom N270 soldered on, 2Gb of Ram and a 1Tb HDD which was replaced by a 2Tb drive at the same time I upgraded the LaCie 2big NAS. It currently runs `Debian 6`_ and a custom rsync script. I know it's not a "real" backup as I don't have previous versions, and it can rsync corruption into my backup, but it's a trade-off I was ready to make at the time.

The Problem
-----------

Let me tell you only one thing: NEVER use WD Green drives if they will be running for any extended period of time. I didn't know this, so I lost both drives in a short period of time. Having not accessed it for a while, I did not notice until it was too late. So now, I'm left with no NAS and a five month old backup. Luckyly, I have not lost anything  . that I am aware of at the moment.

So, with the two hard drives bad, the LaCie 2big is dead as the OS was on those drives. Time to replace this proprietary box with something I can fix myself using commodity components. This time, I want to do it the right way. I already have a basic 4U chassis, so I did not want to buy a complete computer (from iXSystems_ for example). I bought some server components (see below)

My Goal
-------
Here's a list of my need and wants.

Wants:

*  simple to manage
*  reliable
*  stable
*  standard hardware components
*  periodic email status
*  SMS or Twitter + email failure notification

Needs:

*  ZFS `RAID1 <http://en.wikipedia.org/wiki/Standard_RAID_levels#RAID_1>` (mirror or RAIDZx)
*  24/7 grade hard disk drives
*  NFS
*  Samba
*  DLNA
*  service privilege separation (chroot or jails)
*  to be able to install software by myself

I took some time to reflect on those points and decided to go with a custom server build with off-the-shelf components running FreeBSD_ and using a ZFS pool to contain the data.


New hardware
============

* Custom server into basic 4U rackmount chassis  

.. image:: /images/4U_rackmount_chassis.JPG
    :alt: Server chassis image

* Intel `S1200V3RPS <http://ark.intel.com/products/71385/>` motherboard  
* Intel Xeon `E3-1250 <http://ark.intel.com/products/75461/Intel-Xeon-Processor-E3-1225-v3-8M-Cache-3_20-GHz>`
* 8Gb Kingston ECC RAM  
* 1x WD Raptor (10k rpm) 73Gb (OS disk)  
* 2x HDD `WD Red <http://www.wd.com/en/products/products.aspx?id=810>` 2Tb (data disks)

Why use FreeBSD_ ?
---------------------
I program on *the other OS* to pay my bills, I use Linux on my personal PCs (1 desktop, 1 laptop, 1 netbook) and I dream about a FreeBSD_ server the rest of the time !  So, now is the time to make that dream a reality.

Rewind many many years back when I was in college. I had a networking class and we were using RedHat Linux, circa 1998. This is when I discover Linux. It's underground, it's hard, it's completely hand's on. After a few months of trial and error, I give up and go back to *the other OS*.
A few years ago, I rediscovered Linux thanks to my father-in-law at the time, Ubuntu 8.04 was the latest and greatest. I ran a dual-boot machine for a few months, when I was confident that everything i need would work fine, I switched completely to Linux.

I'm a geek, so I like to learn about new things, better ways to do stuff, I like to find underground stuff to try out. So, in my quest for a good, stable, well writen OS [2]_, I found out about the BSDs. I tried OpenBSD, but I find it a bit too *hardcore*, so FreeBSD_ is my second choice, but the right choice.

There you have it, a long recall of my history with Linux and BSDs. If you've read down to here, you either have nothing better to do, or are really insterested to learn more about my experiment.

Stay tuned for more...


.. [1] LaCie uses Linux with XFS as their file system. It's a simple and proven setup. As a bonus, if you mirror the drives, you can unplug one from the NAS, plug it into an internal SATA port and read it right away with any Linux distribution that has XFS drivers. Pretty neat !

.. [2] Let's be frank, Linux is great, but each and every distro has it's own agenda and file system organization it want's to use. Some applications install stuff at one place, others at another place... I sometime feel there's little to no cohesion in the file system architecture


.. Link definitions
.. _freebsd: http://www.freebsd.org
.. _Debian 6:  http://www.debian.org/releases/squeeze/
.. _iXSystems: http://www.ixsystems.com
