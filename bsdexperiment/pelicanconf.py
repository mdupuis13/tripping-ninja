#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

DELETE_OUTPUT_DIRECTORY = True

AUTHOR = u'Martin Dupuis'
SITENAME = u'The BSD Experiment'
#SITEURL = 'http://bsd.martindupuis.info'
SITEURL = 'http://localhost:8000'
SUMMARY_MAX_LENGTH = 50
OUTPUT_SOURCES = False
OUTPUT_SOURCES_EXTENSION = '.text'
AUTHOR_SAVE_AS = False
AUTHORS_SAVE_AS = False

STATIC_PATHS = ['images', ]

ARTICLE_URL = 'posts/{date:%Y}/{date:%b}/{date:%d}-{slug}'
ARTICLE_SAVE_AS = 'posts/{date:%Y}/{date:%b}/{date:%d}-{slug}/index.html'

TIMEZONE = 'America/Montreal'

DEFAULT_LANG = u'en'
THEME = 'crowsfoot'

# global metadata to all the contents
DEFAULT_METADATA = (('keywords', 'BSD, use bsd for desktop, bsd experiment, freebsd, openbsd, netbsd'),)
GOOGLE_ANALYTICS = 'UA-11622348-3'

# Feed generation is usually not desired when developing
FEED_DOMAIN = None
#FEED_ALL_ATOM = 'feeds/all.atom.xml'
#CATEGORY_FEED_ATOM = None
#TRANSLATION_FEED_ATOM = None

# Blogroll
LINKS =  (("Martin Dupuis' blog", "http://martindupuis.info/"),
          ('Pelican', 'http://getpelican.com/'),
          ('Python.org', 'http://python.org/'),
          #          ('You can modify those links in your config file', '#'),
          )

# Social widget
SOCIAL = (('google+', 'https://plus.google.com/103468859076510752794'),
          ('github', 'https://github.com/mdupuis13'), 
          #('RSS', '/feeds/all.atom.xml'),
         )
#TWITTER_USERNAME = "mdupuis13"

DEFAULT_PAGINATION = 10


# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

#crowsfoot theme specifics
PROFILE_IMAGE_URL = '/images/daemon-phk.png'
GITHUB_ADDRESS = 'https://github.com/mdupuis13'
#TWITTER_ADDRESS = 'https://twitter.com/mdupuis13'
FEED_RSS = 'feeds/rss.xml'
LICENSE_URL = 'http://creativecommons.org/licenses/by-sa/4.0/'
LICENSE_NAME = '<img alt="Creative Commons Licence" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/4.0/88x31.png" />'
SHOW_ARTICLE_AUTHOR = False
