#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# This file is only used if you use `make publish` or
# explicitly specify it as your config file.

import os
import sys
sys.path.append(os.curdir)
from pelicanconf import *

SITEURL = 'http://bsd.martindupuis.info'
RELATIVE_URLS = False

FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/%s.atom.xml'

DELETE_OUTPUT_DIRECTORY = True

# Following items are often useful when publishing

# global metadata to all the contents
DEFAULT_METADATA = (('keywords', 'BSD, use bsd for desktop, bsd experiment, freebsd, openbsd, netbsd'),)
GOOGLE_ANALYTICS = 'UA-11622348-3'
